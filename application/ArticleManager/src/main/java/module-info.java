module cr.ac.tec.p2bd.articlemanager {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires org.jetbrains.annotations;
    requires de.mkammerer.argon2;
    requires org.apache.commons.io;
    requires de.mkammerer.argon2.nolibs;


    exports cr.ac.tec.p2bd.articlemanager.UserInterface;
    opens cr.ac.tec.p2bd.articlemanager.UserInterface to javafx.fxml;
    exports cr.ac.tec.p2bd.articlemanager;
    opens cr.ac.tec.p2bd.articlemanager to javafx.fxml;
}