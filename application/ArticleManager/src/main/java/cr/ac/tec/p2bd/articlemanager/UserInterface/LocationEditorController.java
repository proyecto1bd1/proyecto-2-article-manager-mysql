/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CantonController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CountryController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DistrictController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProvinceController;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.HashMap;

public class LocationEditorController {
    private final CountryController countryController = new CountryController();
    private final ProvinceController provinceController = new ProvinceController();
    private final CantonController cantonController = new CantonController();
    private final DistrictController districtController = new DistrictController();
    private HashMap<String, String> location;
    public ChoiceBox<String> cbLocationType;
    public TextField txtLocationName;
    public ChoiceBox<Choice> cbCountry;
    public ChoiceBox<Choice> cbProvince;
    public ChoiceBox<Choice> cbCanton;

    public void initialize() {
        String[] locationOptions = {"Country", "Province", "Canton", "District"};
        cbLocationType.getItems().addAll(locationOptions);
        cbLocationType.setOnAction(this::onLocationSelected);
        cbCountry.getItems().addAll(ChoiceBuilder.getCountryList());
        cbCountry.setOnAction(this::onCountrySelected);

        cbProvince.setOnAction(this::onProvinceSelected);

        cbCanton.setOnAction(this::onCantonSelected);

        cbCountry.setDisable(true);
        cbProvince.setDisable(true);
        cbCanton.setDisable(true);
    }

    public void onLocationSelected(ActionEvent event) {
        String locationSelected = cbLocationType.getValue();

        cbCountry.getItems().clear();
        cbProvince.getItems().clear();
        cbCanton.getItems().clear();

        switch (locationSelected) {
            case "Country" -> {
                cbCountry.setDisable(true);
                cbProvince.setDisable(true);
                cbCanton.setDisable(true);
            }
            case "Province" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(true);
                cbCanton.setDisable(true);
            }
            case "Canton" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(false);
                cbCanton.setDisable(true);
            }
            case "District" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(false);
                cbCanton.setDisable(false);
            }
        }

        cbCountry.getItems().addAll(ChoiceBuilder.getCountryList());
    }

    public void onCountrySelected(ActionEvent event) {
        Choice countrySelected = cbCountry.getValue();
        if (countrySelected != null) {
            cbProvince.getItems().clear();
            cbProvince.getItems().addAll(ChoiceBuilder.getProvinceList(countrySelected.id));
        }
    }

    public void onProvinceSelected(ActionEvent event) {
        Choice provinceSelected = cbProvince.getValue();
        if (provinceSelected != null) {
            cbCanton.getItems().clear();
            cbCanton.getItems().addAll(ChoiceBuilder.getCantonList(provinceSelected.id));
        }
    }

    public void onCantonSelected(ActionEvent event) {
        //TODO
    }

    public void onSave(ActionEvent event) {
        String locationName = txtLocationName.getText();

        try {
            switch (cbLocationType.getValue()) {
                case "Country" -> {
                    if (location != null) {
                        countryController.updateCountry(Integer.valueOf(location.get("countryId")), locationName);
                    } else {
                        countryController.createCountry(locationName);
                    }
                }
                case "Province" -> {
                    Integer selectedCountryId = cbCountry.getValue().id;

                    if (location != null) {
                        provinceController.updateProvince(Integer.valueOf(location.get("provinceId")), locationName, selectedCountryId);
                    } else {
                        provinceController.createProvince(locationName, selectedCountryId);
                    }

                }
                case "Canton" -> {
                    Integer selectedProvinceId = cbProvince.getValue().id;

                    if (location != null) {
                        cantonController.updateCanton(Integer.valueOf(location.get("cantonId")), locationName, selectedProvinceId);
                    } else {
                        cantonController.createCanton(locationName, selectedProvinceId);
                    }
                }
                case "District" -> {
                    Integer selectedCantonId = cbCanton.getValue().id;
                    if (location != null) {
                        districtController.updateDistrict(Integer.valueOf(location.get("districtId")), locationName, selectedCantonId);
                    } else {
                        districtController.createDistrict(locationName, selectedCantonId);
                    }
                }
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Location Saved");
            alert.setHeaderText("We successfully store the location");
            alert.setContentText("You will be redirected to the management page");
            alert.show();

            SessionManager.getInstance().getHomeController().onManageLocations(event);

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to save location");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void setEditMode(HashMap<String, String> location) {
        this.location = location;

        try {
            if (this.location != null) {
                ArrayList<HashMap<String, String>> countries = countryController.getCountryRows();
                ArrayList<HashMap<String, String>> provinces;
                if (this.location.get("countryId") != null && this.location.get("countryName") != null) {
                    cbLocationType.setValue("Country");
                    txtLocationName.setText(this.location.get("countryName"));
                } else if (this.location.get("provinceId") != null && this.location.get("provinceName") != null) {
                    cbLocationType.setValue("Province");
                    txtLocationName.setText(this.location.get("provinceName"));

                    HashMap<String, String> selectedCountry = null;
                    for (HashMap<String, String> country : countries) {
                        if (this.location.get("countryId").equals(country.get("countryId"))) {
                            selectedCountry = country;
                        }
                    }

                    if (selectedCountry != null) {
                        cbCountry.setValue(
                                new Choice(
                                        Integer.valueOf(selectedCountry.get("countryId")),
                                        selectedCountry.get("countryName")
                                )
                        );
                    }

                } else if (this.location.get("cantonId") != null && this.location.get("cantonName") != null) {
                    cbLocationType.setValue("Canton");
                    txtLocationName.setText(this.location.get("cantonName"));

                    HashMap<String, String> selectedProvince = provinceController.getProvinceRow(Integer.valueOf(this.location.get("provinceId")));
                    provinces = provinceController.getProvinceRows(Integer.valueOf(selectedProvince.get("countryId")));
                    cbProvince.getItems().clear();
                    ArrayList<Choice> provincesChoices = new ArrayList<>();

                    for (HashMap<String, String> province : provinces) {
                        provincesChoices.add(
                                new Choice(
                                        Integer.valueOf(province.get("provinceId")),
                                        (province.get("provinceName")
                                        )
                                )
                        );
                    }

                    cbProvince.getItems().addAll(provincesChoices);

                    HashMap<String, String> selectedCountry = null;

                    for (HashMap<String, String> country : countries) {
                        if (selectedProvince.get("countryId").equals(country.get("countryId"))) {
                            selectedCountry = country;
                        }
                    }

                    if (selectedCountry != null) {
                        cbCountry.setValue(
                                new Choice(
                                        Integer.valueOf(selectedCountry.get("countryId")),
                                        selectedCountry.get("countryName")
                                )
                        );
                    }

                    cbProvince.setValue(
                            new Choice(
                                    Integer.valueOf(selectedProvince.get("provinceId")),
                                    selectedProvince.get("provinceName")
                            )
                    );
                } else if (this.location.get("districtId") != null && this.location.get("districtName") != null) {
                    cbLocationType.setValue("District");
                    txtLocationName.setText(this.location.get("districtName"));

                    HashMap<String, String> selectedCanton = cantonController.getCantonRow(Integer.valueOf(this.location.get("cantonId")));
                    ArrayList<HashMap<String, String>> cantons = cantonController.getCantonRows(Integer.valueOf(selectedCanton.get("provinceId")));
                    ArrayList<Choice> cantonChoices = new ArrayList<>();

                    cbCanton.getItems().clear();

                    for (HashMap<String, String> canton : cantons) {
                        cantonChoices.add(
                                new Choice(
                                        Integer.valueOf(canton.get("cantonId")),
                                        canton.get("cantonName")
                                )
                        );
                    }

                    cbCanton.getItems().addAll(cantonChoices);

                    HashMap<String, String> selectedProvince = provinceController.getProvinceRow(Integer.valueOf(selectedCanton.get("provinceId")));
                    provinces = provinceController.getProvinceRows(Integer.valueOf(selectedProvince.get("countryId")));
                    cbProvince.getItems().clear();
                    ArrayList<Choice> provincesChoices = new ArrayList<>();

                    for (HashMap<String, String> province : provinces) {
                        provincesChoices.add(
                                new Choice(
                                        Integer.valueOf(province.get("provinceId")),
                                        (province.get("provinceName")
                                        )
                                )
                        );
                    }

                    cbProvince.getItems().addAll(provincesChoices);

                    HashMap<String, String> selectedCountry = null;

                    for (HashMap<String, String> country : countries) {
                        if (selectedProvince.get("countryId").equals(country.get("countryId"))) {
                            selectedCountry = country;
                        }
                    }

                    if (selectedCountry != null) {
                        cbCountry.setValue(
                                new Choice(
                                        Integer.valueOf(selectedCountry.get("countryId")),
                                        selectedCountry.get("countryName")
                                )
                        );
                    }

                    cbProvince.setValue(
                            new Choice(
                                    Integer.valueOf(selectedProvince.get("provinceId")),
                                    selectedProvince.get("provinceName")
                            )
                    );

                    cbCanton.setValue(
                            new Choice(
                                    Integer.valueOf(selectedCanton.get("cantonId")),
                                    selectedCanton.get("cantonName")
                            )
                    );
                }
            }

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch location");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }
}
