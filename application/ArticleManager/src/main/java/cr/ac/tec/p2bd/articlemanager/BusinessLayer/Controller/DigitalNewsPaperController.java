/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class DigitalNewsPaperController {

    public ArrayList<HashMap<String, String>> getDigitalNewsPaperRows() throws Exception {

        ArrayList<HashMap<String, String>> digitalNewsPaperRows = new ArrayList<>();

        ArrayList<DigitalNewsPaper> digitalNewsPapers = DigitalNewsPaper.getAll();

        digitalNewsPapers.forEach((DigitalNewsPaper digitalNewsPaper) -> {
            digitalNewsPaperRows.add(digitalNewsPaper.toMap());
        });

        return digitalNewsPaperRows;
    }

    public ArrayList<String> getDigitalNewsPaperColumns() throws Exception {
        Set<String> columns = DigitalNewsPaper.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getDigitalNewsPaperRow(Integer digitalNewsPaperId) throws Exception {
        DigitalNewsPaper digitalNewsPaper = DigitalNewsPaper.find(digitalNewsPaperId);

        return digitalNewsPaper.toMap();
    }

    public Integer createDigitalNewsPaper(String digitalNewsPaperName, Integer committeeId, Integer universityId) throws Exception {
        DigitalNewsPaper digitalNewsPaper = new DigitalNewsPaper(digitalNewsPaperName, committeeId, universityId);

        digitalNewsPaper.save();

        return digitalNewsPaper.getNewsPaperId();
    }

    public String updateDigitalNewsPaper(Integer digitalNewsPaperId, String digitalNewsPaperName, Integer committeeId, Integer universityId) throws Exception {
        DigitalNewsPaper digitalNewsPaper = DigitalNewsPaper.find(digitalNewsPaperId);

        digitalNewsPaper.setNewsPaperName(digitalNewsPaperName);
        digitalNewsPaper.setCommitteeId(committeeId);
        digitalNewsPaper.setUniversityId(universityId);

        digitalNewsPaper.save();

        return digitalNewsPaper.getNewsPaperName();
    }

    public ArrayList<DigitalNewsPaper> getAllNewspaper() throws Exception {
        return DigitalNewsPaper.getAll();
    }

    public boolean deleteDigitalNewsPaper(Integer digitalNewsPaperId) throws Exception {
        DigitalNewsPaper digitalNewsPaper = DigitalNewsPaper.find(digitalNewsPaperId);
        digitalNewsPaper.delete();
        return digitalNewsPaper.getNewsPaperId() == null;
    }

    public ArrayList<HashMap<String, String>> getNewspaperMembers(Integer selectedNewspaper) throws Exception{
        ArrayList<PersonXNewspaper> members;
        ArrayList<HashMap<String, String>> desireMembers = new ArrayList<>();

        members = PersonXNewspaper.getAll();

        for (PersonXNewspaper member : members) {
            DigitalNewsPaper newspaper = DigitalNewsPaper.find(member.getNewsPaperId());

            if (Objects.equals(newspaper.getNewsPaperId(), selectedNewspaper)) {
                desireMembers.add(Person.find(member.getPersonId()).toMap());
            }
        }
        return desireMembers;
    }
}
