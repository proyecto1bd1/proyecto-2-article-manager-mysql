/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class PersonXCommittee extends AbstractModel {
    private Integer PersonId;
    private Integer CommitteeId;

    public PersonXCommittee() {
    }

    public PersonXCommittee(Integer PersonId, Integer CommitteeId) {
        this.PersonId = PersonId;
        this.CommitteeId = CommitteeId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setCommitteeId(Integer CommitteeId) {
        this.CommitteeId = CommitteeId;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getCommitteeId() {
        return CommitteeId;
    }

    @Override
    public String toString() {
        return String.format(
                "PersonXCommittee{ PersonId: %d CommitteeId: %d }",
                PersonId,
                CommitteeId
        );
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            CallableStatement sql;
            assert connection != null;
            sql = connection.prepareCall("{call createpersonXcommittee(?, ?)}");
            sql.setInt(1, this.PersonId);
            sql.setInt(2, this.CommitteeId);
            sql.executeQuery();

        } catch (SQLException e) {
            throw new Exception("Error: Unable to save relationship between person and committee", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.CommitteeId == null & this.PersonId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletepersonXcommittee(?, ?)}");
            sql.setInt(1, this.PersonId);
            sql.setInt(2, this.CommitteeId);
            sql.executeQuery();
            this.CommitteeId = null;
            this.PersonId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete relationship between person and committee", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> personCommitteeMap = new HashMap<>();

        personCommitteeMap.put("personId", PersonId.toString());
        personCommitteeMap.put("committeeId", CommitteeId.toString());

        return personCommitteeMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("personId");
        columns.add("committeeId");

        return columns;
    }

    public static PersonXCommittee find(Integer personId, Integer committeeId) throws Exception {
        PersonXCommittee personXcommittee = null;
        ArrayList<PersonXCommittee> personXCommittees;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpersonXcommittee(?, ?)}");
            sql.setInt(1, personId);
            sql.setInt(2, committeeId);

            ResultSet result = sql.executeQuery();
            personXCommittees = buildList(result);


            if (!personXCommittees.isEmpty()) {
                personXcommittee = personXCommittees.get(0);
            }

            return personXcommittee;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between person and committee", e);
        }
    }

    public static @NotNull ArrayList<PersonXCommittee> getAll() throws Exception {
        ArrayList<PersonXCommittee> personXCommittees;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpersonXcommittee(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            personXCommittees = buildList(result);

            return personXCommittees;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between person and committee", e);
        }
    }

    private static @NotNull ArrayList<PersonXCommittee> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<PersonXCommittee> personXCommittees = new ArrayList<>();

        while (result.next()) {
            PersonXCommittee personXcommittee = new PersonXCommittee(
                    result.getInt("PersonId"),
                    result.getInt("CommitteeId")
            );
            personXCommittees.add(personXcommittee);
        }

        return personXCommittees;
    }

    public static @NotNull ArrayList<PersonXCommittee> getAll(Integer personId) throws Exception {
        ArrayList<PersonXCommittee> personXCommittees;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCommitteesOfPerson(?)}");
            sql.setInt(1, personId);

            ResultSet result = sql.executeQuery();

            personXCommittees = buildList(result);

            return personXCommittees;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between person and committee", e);
        }
    }
}
