/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProductController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PurchaseController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PurchaseXProductController;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.HashMap;

public class StoreController {
    private final ProductController productController = new ProductController();
    private final PurchaseController purchaseController = new PurchaseController();
    private final PurchaseXProductController purchaseXProductController = new PurchaseXProductController();
    public ChoiceBox<Choice> cbProducts;
    public AnchorPane anchorPane;

    public void initialize() {
        cbProducts.getItems().addAll(ChoiceBuilder.getProductList());
    }

    public void onPurchase(ActionEvent event) {
        try {
            if (cbProducts.getValue() != null) {
                HashMap<String, String> product = productController.getProductRow(cbProducts.getValue().id);

                int pointsNeeded = Integer.parseInt(product.get("PointsPrice"));
                int inStock = Integer.parseInt(product.get("NumberInStock"));

                if (SessionManager.getInstance().getPoints() >= pointsNeeded) {
                    if (inStock > 0) {
                        Integer purchaseId = purchaseController.createPurchase(
                                Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId"))
                        );

                        purchaseXProductController.createPurchaseXProduct(purchaseId, cbProducts.getValue().id);

                        productController.updateProduct(
                                cbProducts.getValue().id,
                                product.get("Name"),
                                pointsNeeded,
                                inStock - 1,
                                Integer.valueOf(product.get("StoreId"))
                        );

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Purchase successful");
                        alert.setHeaderText("the item you selected was bought");
                        alert.setContentText("We will be reaching you later for the delivery");
                        alert.showAndWait();

                        Stage stage = (Stage) anchorPane.getScene().getWindow();
                        stage.close();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Out of stock");
                        alert.setHeaderText("The product you selected is not available");
                        alert.setContentText("Please pick up a different item");
                        alert.show();
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Not enough points");
                    alert.setHeaderText("You don't have enough points");
                    alert.setContentText("Please pick up a cheaper item");
                    alert.show();
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("No item selected");
                alert.setHeaderText("Please select a product");
                alert.setContentText("We can't make a purchase if you don't select a product");
                alert.show();
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to make purchase");
            alert.setHeaderText("We failed at making the purchase");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }
}
