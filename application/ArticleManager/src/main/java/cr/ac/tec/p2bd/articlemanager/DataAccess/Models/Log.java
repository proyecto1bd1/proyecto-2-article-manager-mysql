/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Log extends AbstractModel {
    private Integer LogId;
    private String FieldName;
    private String TableName;
    private String CurrentValue;
    private String PreviousValue;
    private Integer ArticleId;

    public Log() {
    }

    private Log(Integer LogId, String FieldName, String TableName, String CurrentValue, String PreviousValue, Integer ArticleId) {
        this.LogId = LogId;
        this.FieldName = FieldName;
        this.TableName = TableName;
        this.CurrentValue = CurrentValue;
        this.PreviousValue = PreviousValue;
        this.ArticleId = ArticleId;
    }

    public Log(String FieldName, String TableName, String CurrentValue, String PreviousValue, Integer ArticleId) {
        this.FieldName = FieldName;
        this.TableName = TableName;
        this.CurrentValue = CurrentValue;
        this.PreviousValue = PreviousValue;
        this.ArticleId = ArticleId;
    }

    public void setLogId(Integer LogId) {
        this.LogId = LogId;
    }

    public void setFieldName(String FieldName) {
        this.FieldName = FieldName;
    }

    public void setTableName(String TableName) {
        this.TableName = TableName;
    }

    public void setCurrentValue(String CurrentValue) {
        this.CurrentValue = CurrentValue;
    }

    public void setPreviousValue(String PreviousValue) {
        this.PreviousValue = PreviousValue;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public Integer getLogId() {
        return LogId;
    }

    public String getFieldName() {
        return FieldName;
    }

    public String getTableName() {
        return TableName;
    }

    public String getCurrentValue() {
        return CurrentValue;
    }

    public String getPreviousValue() {
        return PreviousValue;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    @Override
    public String toString() {
        return String.format(
                "Log{ LogId: %d FieldName: %s TableName: %s CurrentValue: %s PreviousValue: %s ArticleId: %d }",
                LogId,
                FieldName,
                TableName,
                CurrentValue,
                PreviousValue,
                ArticleId
        );
    }


    @Override
    public void delete() throws Exception {
        throw new Exception("Log entries cannot be deleted this way");
    }


    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> logMap = new HashMap<>();

        logMap.put("LogId", LogId.toString());
        logMap.put("FieldName", FieldName);
        logMap.put("TableName", TableName);
        logMap.put("CurrentValue", CurrentValue);
        logMap.put("PreviousValue", PreviousValue);

        return logMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("LogId");
        columns.add("FieldName");
        columns.add("TableName");
        columns.add("CurrentValue");
        columns.add("PreviousValue");
        columns.add("ArticleId");
        return columns;
    }

    public static Log find(Integer id) throws Exception {
        Log log = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getLog(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                log = new Log(
                        result.getInt("LogId"),
                        result.getString("FieldName"),
                        result.getString("TableName"),
                        result.getString("CurrentValue"),
                        result.getString("PreviousValue"),
                        result.getInt("ArticleId")
                );
            }
            return log;
        } catch (SQLException e) {
            throw new Exception("Unable to get log", e);
        }
    }

    public static ArrayList<Log> getAll() throws Exception {
        ArrayList<Log> logs = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getlog(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Log log = new Log(
                        result.getInt("LogId"),
                        result.getString("FieldName"),
                        result.getString("TableName"),
                        result.getString("CurrentValue"),
                        result.getString("PreviousValue"),
                        result.getInt("ArticleId")
                );

                logs.add(log);
            }
            return logs;
        } catch (SQLException e) {
            throw new Exception("Unable to get logs", e);
        }
    }

    @Override
    public void save() throws Exception {
        throw new Exception("Logs cannot be written");
    }
}
