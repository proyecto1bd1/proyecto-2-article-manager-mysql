/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class Canton extends AbstractModel {

    private Integer cantonId;
    private String cantonName;
    private Integer provinceId;

    public Canton() {
    }

    private Canton(Integer cantonId, String cantonName, Integer provinceId) {
        this.cantonId = cantonId;
        this.cantonName = cantonName;
        this.provinceId = provinceId;
    }

    public Canton(String cantonName, Integer provinceId) {
        this.cantonName = cantonName;
        this.provinceId = provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCantonId() {
        return cantonId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setCantonName(String cantonName) {
        this.cantonName = cantonName;
    }

    public String getCantonName() {
        return cantonName;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.cantonId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateCanton(?, ?, ?)}");
                sql.setInt(1, this.cantonId);
                sql.setString(2, cantonName);
                sql.setInt(3, provinceId);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createCanton(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, cantonName);
                sql.setInt(3, provinceId);
                sql.execute();
                this.cantonId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving canton", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.cantonId == null)
            return;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteCanton(?)}");
            sql.setInt(1, cantonId);
            sql.executeQuery();
            this.cantonId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting canton", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Canton{ Id: %d Name: %s ProvinceId: %d }",
                cantonId,
                cantonName,
                provinceId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> provinceMap = new HashMap<>();

        provinceMap.put("cantonId", cantonId.toString());
        provinceMap.put("cantonName", cantonName);
        provinceMap.put("provinceId", provinceId.toString());

        return provinceMap;
    }

    private static @NotNull ArrayList<Canton> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Canton> cantons = new ArrayList<>();

        while (result.next()) {
            Canton canton = new Canton(
                    result.getInt("CantonId"),
                    result.getString("CantonName"),
                    result.getInt("ProvinceId")
            );
            cantons.add(canton);
        }

        return cantons;
    }

    public static Canton find(Integer id) throws Exception {
        Canton canton = null;
        ArrayList<Canton> cantons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCantons(?, ?)}");
            sql.setInt(1, id);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            cantons = Canton.buildList(result);

            if (!cantons.isEmpty()) {
                canton = cantons.get(0);
            }

            return canton;
        } catch (SQLException e) {
            throw new Exception("Error fetching cantons", e);
        }
    }

    public static @NotNull ArrayList<Canton> getAll() throws Exception {
        ArrayList<Canton> cantons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCantons(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            cantons = Canton.buildList(result);

            return cantons;
        } catch (SQLException e) {
            throw new Exception("Error fetching cantons", e);
        }
    }

    public static @NotNull ArrayList<Canton> getAll(Integer provinceId) throws Exception {
        ArrayList<Canton> cantons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{ call getCantons(?, ?) }");
            sql.setNull(1, Types.BIGINT);
            sql.setInt(2, provinceId);

            ResultSet result = sql.executeQuery();

            cantons = Canton.buildList(result);

            return cantons;
        } catch (SQLException e) {
            throw new Exception("Error fetching cantons", e);
        }
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("cantonId");
        columns.add("cantonName");
        columns.add("provinceId");

        return columns;
    }
}
