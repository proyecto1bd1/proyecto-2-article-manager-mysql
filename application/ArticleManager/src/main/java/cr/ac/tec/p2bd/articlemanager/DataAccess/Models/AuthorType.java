/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class AuthorType extends AbstractModel {
    private Integer AuthorTypeId;
    private String Description;

    public AuthorType() {
    }

    private AuthorType(Integer AuthorTypeId, String Description) {
        this.AuthorTypeId = AuthorTypeId;
        this.Description = Description;
    }

    public AuthorType(String Description) {
        this.Description = Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getAuthorTypeId() {
        return AuthorTypeId;
    }

    public String getDescription() {
        return Description;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (AuthorTypeId != null) {
                CallableStatement sql = connection.prepareCall("{call updateAuthortype(?, ?)}");
                sql.setInt(1, AuthorTypeId);
                sql.setString(2, Description);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createAuthortype(?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, Description);
                sql.execute();

                AuthorTypeId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Errors saving author type", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (AuthorTypeId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteAuthortype(?)}");
            sql.setInt(1, AuthorTypeId);
            sql.executeQuery();

            AuthorTypeId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting author type", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "AuthorType{ Id: %d Description: %s }",
                AuthorTypeId,
                Description
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> authorTypeMap = new HashMap<>();

        authorTypeMap.put("AuthorTypeId", AuthorTypeId.toString());
        authorTypeMap.put("Description", Description);

        return authorTypeMap;
    }

    private static @NotNull ArrayList<AuthorType> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<AuthorType> authorTypes = new ArrayList<>();

        while (result.next()) {
            AuthorType authorType = new AuthorType(
                    result.getInt("AuthorTypeId"),
                    result.getString("Description")
            );

            authorTypes.add(authorType);
        }

        return authorTypes;
    }

    public static AuthorType find(Integer id) throws Exception {
        AuthorType authorType = null;
        ArrayList<AuthorType> authorTypes;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{ call getAuthortype(?) }");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            authorTypes = buildList(result);

            if (!authorTypes.isEmpty()) {
                authorType = authorTypes.get(0);
            }

            return authorType;
        } catch (SQLException e) {
            throw new Exception("Error fetching author types", e);
        }
    }

    public static @NotNull ArrayList<AuthorType> getAll() throws Exception {
        ArrayList<AuthorType> authorTypes;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{ call getAuthortype(?) }");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            authorTypes = buildList(result);

            return authorTypes;
        } catch (SQLException e) {
            throw new Exception("Error fetching author types", e);
        }
    }

    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("authorTypeId");
        columns.add("Description");

        return columns;
    }
}
