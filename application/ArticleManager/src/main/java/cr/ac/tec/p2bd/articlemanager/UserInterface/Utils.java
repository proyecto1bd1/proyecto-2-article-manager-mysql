/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CantonController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CountryController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DistrictController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProvinceController;
import javafx.scene.control.Alert;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

public class Utils {

    private static final CountryController countryController = new CountryController();
    private static final ProvinceController provinceController = new ProvinceController();
    private static final CantonController cantonController = new CantonController();
    private static final DistrictController districtController = new DistrictController();

    public static @Nullable HashMap<String, HashMap<String, String>> getDistrictFullLocation(Integer districtId) {
        try {
            HashMap<String, String> district = districtController.getDistrictRow(districtId);

            HashMap<String, String> canton = cantonController.getCantonRow(Integer.valueOf(district.get("cantonId")));

            HashMap<String, String> province = provinceController.getProvinceRow(Integer.valueOf(canton.get("provinceId")));

            HashMap<String, String> country = countryController.getCountryRow(Integer.valueOf(province.get("countryId")));

            HashMap<String, HashMap<String, String>> fullLocation = new HashMap<>();

            fullLocation.put("country", country);
            fullLocation.put("province", province);
            fullLocation.put("canton", canton);
            fullLocation.put("district", district);

            return fullLocation;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch location");
            alert.setContentText(e.getMessage());
            alert.show();
            return null;
        }
    }

    public static HashMap<String, HashMap<String, String>> getCantonFullLocation(Integer cantonId) {
        try {
            HashMap<String, String> canton = cantonController.getCantonRow(cantonId);

            HashMap<String, String> province = provinceController.getProvinceRow(Integer.valueOf(canton.get("provinceId")));

            HashMap<String, String> country = countryController.getCountryRow(Integer.valueOf(province.get("countryId")));

            HashMap<String, HashMap<String, String>> fullLocation = new HashMap<>();

            fullLocation.put("country", country);
            fullLocation.put("province", province);
            fullLocation.put("canton", canton);

            return fullLocation;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch location");
            alert.setContentText(e.getMessage());
            alert.show();
            return null;
        }
    }

    public static HashMap<String, HashMap<String, String>> getProvinceFullLocation(Integer provinceId) {
        try {
            HashMap<String, String> province = provinceController.getProvinceRow(provinceId);

            HashMap<String, String> country = countryController.getCountryRow(Integer.valueOf(province.get("countryId")));

            HashMap<String, HashMap<String, String>> fullLocation = new HashMap<>();

            fullLocation.put("country", country);
            fullLocation.put("province", province);
            
            return fullLocation;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch location");
            alert.setContentText(e.getMessage());
            alert.show();
            return null;
        }
    }
}
