/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Email extends AbstractModel {

    private Integer emailId;
    private String emailAddress;
    private Integer personId;

    public Email() {
    }

    public Email(Integer emailId, String emailAddress, Integer personId) {
        this.emailId = emailId;
        this.emailAddress = emailAddress;
        this.personId = personId;
    }

    public Email(String emailAddress, Integer personId) {
        this.emailAddress = emailAddress;
        this.personId = personId;
    }

    public void setEmailId(Integer emailId) {
        this.emailId = emailId;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getEmailId() {
        return emailId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public Integer getPersonId() {
        return personId;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (emailId != null) {
                CallableStatement sql = connection.prepareCall("{call updateEmail(?, ?, ?)}");
                sql.setInt(1, emailId);
                sql.setString(2, emailAddress);
                sql.setInt(3, personId);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createEmail(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, emailAddress);
                sql.setInt(3, personId);
                sql.execute();

                emailId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving email", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (emailId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteEmail(?)}");
            sql.setInt(1, emailId);
            sql.executeQuery();

            emailId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting email", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Email{ emailId: %d emailAddress= %s personId: %d}",
                emailId,
                emailAddress,
                personId
        );
    }

    private static @NotNull ArrayList<Email> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Email> emails = new ArrayList<>();

        while (result.next()) {
            Email email = new Email(
                    result.getInt("EmailId"),
                    result.getString("EmailAddress"),
                    result.getInt("PersonId")
            );
            emails.add(email);
        }

        return emails;
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> emailMap = new HashMap<>();
        emailMap.put("EmailId", emailId.toString());
        emailMap.put("EmailAddress", emailAddress);
        emailMap.put("PersonId", personId.toString());

        return emailMap;
    }

    public static Email find(Integer id) throws Exception {
        Email email = null;
        ArrayList<Email> emails;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getEmail(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            emails = buildList(result);

            if (!emails.isEmpty()) {
                email = emails.get(0);
            }

            return email;
        } catch (SQLException e) {
            throw new Exception("Error fetching email", e);
        }
    }

    public static @NotNull ArrayList<Email> getAll() throws Exception {
        ArrayList<Email> emails;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getEmail(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            emails = buildList(result);

            return emails;
        } catch (SQLException e) {
            throw new Exception("Error fetching emails", e);
        }
    }

    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("EmailId");
        columns.add("EmailAddress");
        columns.add("PersonId");

        return columns;
    }
}
