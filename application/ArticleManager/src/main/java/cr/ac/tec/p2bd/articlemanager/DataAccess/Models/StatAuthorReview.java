/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class StatAuthorReview {

    private String authorName;
    private Integer rating;

    public StatAuthorReview(){};

    public StatAuthorReview(String authorName, Integer rating) {
        this.authorName = authorName;
        this.rating = rating;
    }

    public void setauthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setrating(Integer rating) {
        this.rating = rating;
    }

    public String getauthorName() {
        return authorName;
    }

    public Integer getrating() {
        return rating;
    }

    public static ArrayList<StatAuthorReview> getAll() throws Exception {
        ArrayList<StatAuthorReview> rows = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call statAvgReviewXauthor}");
            ResultSet result = sql.executeQuery();

            while (result.next()) {
                StatAuthorReview stat = new StatAuthorReview(
                        result.getString("AuthorName"),
                        result.getInt("AverageReview")
                );
                rows.add(stat);
            }
            return rows;
        } catch (SQLException e) {
            throw new Exception("Unable to get statistic", e);
        }
    }

    public HashMap<String, String> toMap() {

        HashMap<String, String> statMap = new HashMap<>();

        statMap.put("Author", authorName);
        statMap.put("Average Review", rating.toString());

        return statMap;
    }
}
