/*
 * The MIT License
 *
 * Copyright 2022 Daniel G.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Author;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Daniel G
 */
public class AuthorController {
    /**
     * Gets a list of all authors
     *
     * @return All the entries in authors as rows
     */
    public ArrayList<HashMap<String, String>> getAuthorRows() throws Exception {

        ArrayList<HashMap<String, String>> authorRows = new ArrayList<>();

        ArrayList<Author> authors = Author.getAll();

        authors.forEach((Author author) -> {
            authorRows.add(author.toMap());
        });

        return authorRows;
    }

    /**
     * Get the columns of the author model
     *
     * @return A list of columns
     */
    public ArrayList<String> getAuthorColumns() throws Exception {
        Set<String> columns = Author.getColumns();

        return new ArrayList<>(columns);
    }

    /**
     * Get a author as a row
     *
     * @param authorId The author to locate
     * @return The row representing the author
     */
    public HashMap<String, String> getAuthorRow(Integer authorId) throws Exception {
        Author author = Author.find(authorId);

        return author != null ? author.toMap() : null;
    }

    /**
     * Stores a new author
     *
     * @param authorId     Id of the author (PersonId)
     * @param authorTypeId The name of the author
     * @return The id of the created element
     */
    public Integer createAuthor(Integer authorId, Integer authorTypeId) throws Exception {
        Author author = new Author(authorId, authorTypeId);

        author.save();

        return author.getAuthorId();
    }

    /**
     * Updates the value of a author
     *
     * @param authorId     The id of the author to update
     * @param authorTypeId The new name of the author
     * @return The new name of the author
     */
    public Integer updateAuthor(Integer authorId, Integer authorTypeId) throws Exception {
        Author author = Author.find(authorId);

        author.setAuthorTypeId(authorTypeId);
        author.save();

        return author.getAuthorTypeId();

    }

    /**
     * Deletes an author from the system
     *
     * @param authorId The id of the author to delete
     * @return True if author was deleted, false otherwise
     * Could fail if author has references attach
     */
    public boolean deleteAuthor(Integer authorId) throws Exception {
        Author author = Author.find(authorId);

        author.delete();

        return author.getAuthorId() == null;
    }

    public Integer authorPoints(Integer authorId) throws Exception {
        return Author.getAuthorPoints(authorId);
    }

    public Integer authorAvailablePoints(Integer authorId) throws Exception {
        return Author.getAuthorAvailablePoints(authorId);
    }
}
