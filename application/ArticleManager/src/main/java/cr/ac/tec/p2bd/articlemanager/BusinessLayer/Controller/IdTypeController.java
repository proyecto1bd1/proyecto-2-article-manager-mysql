/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.IdType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class IdTypeController {

    /**
     * Gets a list of all countries
     *
     * @return All the entries in countries as rows
     
     */
    public ArrayList<HashMap<String, String>> getIdTypeRows() throws Exception {

        ArrayList<HashMap<String, String>> idTypeRows = new ArrayList<>();

        ArrayList<IdType> idTypes = IdType.getAll();

        idTypes.forEach((IdType idType) -> {
            idTypeRows.add(idType.toMap());
        });

        return idTypeRows;
    }

    /**
     * Get the columns of the idType model
     *
     * @return A list of columns
     
     */
    public ArrayList<String> getIdTypeColumns() throws Exception {
        Set<String> columns = IdType.getColumns();

        return new ArrayList<>(columns);
    }

    /**
     * Get a idType as a row
     *
     * @param idTypeId The idType to locate
     * @return The row representing the idType
     
     */
    public HashMap<String, String> getIdTypeRow(Integer idTypeId) throws Exception {
        IdType idType = IdType.find(idTypeId);

        return idType.toMap();
    }

    public Integer createIdType(String idTypeName, String mask) throws Exception {
        IdType idType = new IdType(idTypeName, mask);

        idType.save();

        return idType.getTypeId();
    }

    public String updateIdType(Integer idTypeId, String idTypeName) throws Exception {
        IdType idType = IdType.find(idTypeId);

        idType.setDescription(idTypeName);
        idType.save();

        return idType.getDescription();
    }

    /**
     * Deletes a idType from the system
     *
     * @param idTypeId The id of the typeId
     * @return True if idType was deleted, false otherwise
      Could fail if idType has references attach
     */
    public boolean deleteIdType(Integer idTypeId) throws Exception {
        IdType idType = IdType.find(idTypeId);

        idType.delete();

        return idType.getTypeId() == null;
    }
}
