/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Person;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

/**
 * @author Diego Herrera
 */
public class PersonController {
    private final Argon2 passwordMaker;

    public PersonController() {
        passwordMaker = Argon2Factory.create();
    }

    private String encryptPassword(@NotNull String password) {
        return passwordMaker.hash(10, 65536, 1, password.toCharArray());
    }

    public HashMap<String, String> getPersonRow(Integer personId) throws Exception {
        Person person = Person.find(personId);

        return person.toMap();
    }

    public Integer createPerson(String IdCard, String Name, String Firstlastname, String Secondlastname, String Birthdate, File Photo, String LocalAddress, String Username, String Password, Integer Districtid, Integer Genderid, Integer typeid) throws Exception {
        String encryptedPassword = encryptPassword(Password);

        Date personBirthdate = Date.valueOf(Birthdate);

        byte[] photoConverted = Photo != null ? FileUtils.readFileToByteArray(Photo) : null;

        Person person = new Person(IdCard, Name, Firstlastname, Secondlastname, personBirthdate, photoConverted, LocalAddress, Username, encryptedPassword, Districtid, Genderid, typeid);

        person.save();

        return person.getPersonId();
    }

    public boolean deletePerson(Integer personId) throws Exception {
        Person person = Person.find(personId);

        person.delete();

        return person.getPersonId() == null;
    }

    public HashMap<String, String> login(String username, String password) throws Exception {
        Person person = Person.find(username);

        if (person == null) {
            throw new Exception("Username or password incorrect");
        }

        if (verifyPassword(person.getPassword(), password)) {
            throw new Exception("Username or password incorrect");
        }

        return person.toMap();
    }

    private boolean verifyPassword(String hash, String password) {
        return !passwordMaker.verify(hash, password.toCharArray());
    }

    public void updatePerson(Integer personId, String IdCard, String Name, String Firstlastname, String Secondlastname, String Birthdate, File Photo, String LocalAddress, String OldPassword, String NewPassword, Integer Districtid, Integer Genderid, Integer typeid) throws Exception {
        Person person = Person.find(personId);

        if (person != null) {
            Date personBirthdate = Date.valueOf(Birthdate);

            byte[] photoConverted = Photo != null ? FileUtils.readFileToByteArray(Photo) : null;

            if (verifyPassword(person.getPassword(), OldPassword)) {
                throw new Exception("Wrong password");
            }

            if (NewPassword.length() > 0) {
                person.setPassword(encryptPassword(NewPassword));
            }

            person.setIdCard(IdCard);
            person.setName(Name);
            person.setFirstlastname(Firstlastname);
            person.setSecondlastname(Secondlastname);
            person.setBirthdate(personBirthdate);
            person.setPhoto(photoConverted);
            person.setLocalAddress(LocalAddress);
            person.setDistrictid(Districtid);
            person.setGenderid(Genderid);
            person.setTypeid(typeid);
            person.save();
        }
    }

    public void updatePersonAdmin(Integer personId, String IdCard, String Name, String Firstlastname, String Secondlastname, String Birthdate, File Photo, String LocalAddress, String NewPassword, Integer Districtid, Integer Genderid, Integer typeid) throws Exception {
        Person person = Person.find(personId);

        if (person != null) {
            Date personBirthdate = Date.valueOf(Birthdate);

            byte[] photoConverted = Photo != null ? FileUtils.readFileToByteArray(Photo) : null;

            if (NewPassword.length() > 0) {
                person.setPassword(encryptPassword(NewPassword));
            }

            person.setIdCard(IdCard);
            person.setName(Name);
            person.setFirstlastname(Firstlastname);
            person.setSecondlastname(Secondlastname);
            person.setBirthdate(personBirthdate);
            person.setPhoto(photoConverted);
            person.setLocalAddress(LocalAddress);
            person.setDistrictid(Districtid);
            person.setGenderid(Genderid);
            person.setTypeid(typeid);
            person.save();
        }
    }

    public ArrayList<HashMap<String, String>> getPersonRows() throws Exception {

        ArrayList<HashMap<String, String>> personRows = new ArrayList<>();

        ArrayList<Person> people = Person.getAll();

        people.forEach((Person person) -> {
            personRows.add(person.toMap());
        });

        return personRows;
    }

    public ArrayList<HashMap<String, String>> getPersonNoCommittee() throws Exception {

        ArrayList<HashMap<String, String>> personRows = new ArrayList<>();

        ArrayList<Person> people = Person.getPersonNoCommittee();

        people.forEach((Person person) -> {
            personRows.add(person.toMap());
        });

        return personRows;
    }

    public ArrayList<HashMap<String, String>> getPersonNoNewspaper() throws Exception {

        ArrayList<HashMap<String, String>> personRows = new ArrayList<>();

        ArrayList<Person> people = Person.getPersonNoNewspaper();

        people.forEach((Person person) -> {
            personRows.add(person.toMap());
        });

        return personRows;
    }
}
