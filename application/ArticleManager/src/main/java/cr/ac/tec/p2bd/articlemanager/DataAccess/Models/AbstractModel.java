/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import java.util.HashMap;

/**
 * Implements the basic methods of a Model, since find and getAll are static,
 * they can't be overridden nor abstracted
 *
 * @author Diego Herrera
 */
abstract class AbstractModel {
    /**
     * Saves the model been created in the database
     *
      If saving fails, it will throw an error
     */
    abstract public void save() throws Exception;

    /**
     * Deletes the model from the database
     *
      If saving fails, it will throw an error
     */
    abstract public void delete() throws Exception;

    /**
     * String representation of the model for debug purpose
     *
     * @return The values of the model
     */
    @Override
    abstract public String toString();

    /**
     * Model values has a hashmap for easier usage in the front end
     *
     * @return The model represented as strings
     */
    abstract public HashMap<String, String> toMap();
}
