/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PxReviewsXArticle;

/**
 * @author Diego Herrera
 */
public class PxReviewsXArticleController {

    public PxReviewsXArticleController() {
    }


    public ArrayList<HashMap<String, String>> getPxReviewsXArticleRows() throws Exception {
        ArrayList<HashMap<String, String>> PxReviewsXArticleRows = new ArrayList<>();

        ArrayList<PxReviewsXArticle> reviews = PxReviewsXArticle.getAll();

        reviews.forEach((PxReviewsXArticle pxreviewsxarticle) -> {
            PxReviewsXArticleRows.add(pxreviewsxarticle.toMap());
        });

        return PxReviewsXArticleRows;

    }

    public ArrayList<String> getPxReviewsXArticleColumns() throws Exception {
        Set<String> pXReviewsXArticle = PxReviewsXArticle.getColumns();

        return new ArrayList<>(pXReviewsXArticle);
    }

    public HashMap<String, String> getPxReviewsXArticleRow(Integer personId, Integer articleId) throws Exception {
        PxReviewsXArticle pxreviewsxarticle = PxReviewsXArticle.find(personId, articleId);

        return pxreviewsxarticle != null ? pxreviewsxarticle.toMap() : null;
    }

    public Integer createPxReviewsXArticle(Integer personId, Integer articleId, Integer NStars) throws Exception {
        PxReviewsXArticle pxreviewsxarticle = new PxReviewsXArticle(personId, articleId, NStars);

        pxreviewsxarticle.save();

        return pxreviewsxarticle.getPersonId();
    }

    public Integer updatePxReviewsXArticle(Integer personId, Integer articleId, Integer NStars) throws Exception {
        PxReviewsXArticle pxreviewsxarticle = PxReviewsXArticle.find(personId, articleId);

        pxreviewsxarticle.setnStars(NStars);
        pxreviewsxarticle.save();

        return pxreviewsxarticle.getPersonId();
    }

    public boolean deletePxReviewsXArticle(Integer personId, Integer articleId) throws Exception {
        PxReviewsXArticle pxreviewsxarticle = PxReviewsXArticle.find(personId, articleId);

        pxreviewsxarticle.delete();

        return pxreviewsxarticle.getPersonId() == null;
    }
}
