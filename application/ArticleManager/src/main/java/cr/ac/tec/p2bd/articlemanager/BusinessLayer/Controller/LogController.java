/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


/**
 * @author Diego Herrera
 */
public class LogController {

    public LogController() {
    }

    public ArrayList<HashMap<String, String>> getLogRows() throws Exception {

        ArrayList<HashMap<String, String>> logRows = new ArrayList<>();

        ArrayList<Log> logs = Log.getAll();

        logs.forEach((Log log) -> {
            logRows.add(log.toMap());
        });

        return logRows;
    }

    public ArrayList<String> getLogColumns() throws Exception {
        Set<String> columns = Log.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getLogRow(Integer logId) throws Exception {
        Log log = Log.find(logId);

        return log.toMap();
    }

    public ArrayList<Log> getAllLogs() throws Exception {
        ArrayList<Log> logs = new ArrayList<>();
        logs = Log.getAll();
        return logs;
    }
}
