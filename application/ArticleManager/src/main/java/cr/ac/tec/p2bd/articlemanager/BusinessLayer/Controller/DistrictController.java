/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.District;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class DistrictController {
    public ArrayList<HashMap<String, String>> getDistrictRows(Integer cantonId) throws Exception {
        ArrayList<HashMap<String, String>> districtRows = new ArrayList<>();

        ArrayList<District> districts = District.getAll(cantonId);

        districts.forEach((District district) -> {
            districtRows.add(district.toMap());
        });

        return districtRows;
    }

    public ArrayList<HashMap<String, String>> getDistrictRows() throws Exception {
        ArrayList<HashMap<String, String>> districtRows = new ArrayList<>();

        ArrayList<District> districts = District.getAll();

        districts.forEach((District district) -> {
            districtRows.add(district.toMap());
        });

        return districtRows;
    }

    public HashMap<String, String> getDistrictRow(Integer districtId) throws Exception {
        District district = District.find(districtId);

        return district.toMap();
    }

    public Integer createDistrict(String districtName, Integer cantonId) throws Exception {
        District district = new District(districtName, cantonId);

        district.save();

        return district.getDistrictId();
    }

    public String updateDistrict(Integer districtId, String districtName, Integer cantonId) throws Exception {
        District district = District.find(districtId);

        district.setDistrictName(districtName);
        district.setCantonId(cantonId);
        district.save();

        return district.getDistrictName();
    }

    public boolean deleteDistrict(Integer districtId) throws Exception {
        District district = District.find(districtId);

        district.delete();

        return district.getDistrictId() == null;
    }

    public ArrayList<String> getDistrictColumns() {
        Set<String> columns = District.getColumns();

        return new ArrayList<>(columns);
    }
}
