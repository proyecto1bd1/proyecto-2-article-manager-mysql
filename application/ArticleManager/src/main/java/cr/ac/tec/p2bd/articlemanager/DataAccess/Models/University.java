/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * Represents a university within the system
 *
 * @author Daniel G
 */
public class University extends AbstractModel {
    private Integer UniversityId;
    private String UniversityName;
    private String LocalAddress;
    private Integer DistrictId;

    public University(Integer UniversityId, String UniversityName, String LocalAddress, Integer DistrictId) {
        this.UniversityId = UniversityId;
        this.UniversityName = UniversityName;
        this.LocalAddress = LocalAddress;
        this.DistrictId = DistrictId;
    }

    public University(String UniversityName, String LocalAddress, Integer DistrictId) {
        this.UniversityName = UniversityName;
        this.LocalAddress = LocalAddress;
        this.DistrictId = DistrictId;
    }

    public void setUniversityId(Integer UniversityId) {
        this.UniversityId = UniversityId;
    }

    public void setUniversityName(String UniversityName) {
        this.UniversityName = UniversityName;
    }

    public void setLocalAddress(String LocalAddress) {
        this.LocalAddress = LocalAddress;
    }

    public void setDistrictId(Integer DistrictId) {
        this.DistrictId = DistrictId;
    }

    public Integer getUniversityId() {
        return UniversityId;
    }

    public String getUniversityName() {
        return UniversityName;
    }

    public String getLocalAddress() {
        return LocalAddress;
    }

    public Integer getDistrictId() {
        return DistrictId;
    }


    @Override
    public String toString() {
        return String.format(
                "University{ UniversityId: %d UniversityName: %s LocalAddress: %s DistrictId: %d }",
                UniversityId,
                UniversityName,
                LocalAddress,
                DistrictId
        );
    }


    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.UniversityId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateuniversity(?, ?, ?, ?)}");
                sql.setInt(1, this.UniversityId);
                sql.setString(2, this.UniversityName);
                sql.setString(3, this.LocalAddress);
                sql.setInt(4, this.DistrictId);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createuniversity(?, ?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.UniversityName);
                sql.setString(3, this.LocalAddress);
                sql.setInt(4, this.DistrictId);
                sql.execute();
                this.UniversityId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error: Unable to save university", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.UniversityId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteuniversity(?)}");
            sql.setInt(1, this.UniversityId);
            sql.executeQuery();
            this.UniversityId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete university", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> universityMap = new HashMap<>();

        universityMap.put("universityId", UniversityId.toString());
        universityMap.put("universityName", UniversityName);
        universityMap.put("localAddress", LocalAddress);
        universityMap.put("districtId", DistrictId.toString());
        return universityMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("universityId");
        columns.add("universityName");
        columns.add("localAddress");
        columns.add("districtId");

        return columns;
    }

    public static University find(Integer id) throws Exception {
        University university = null;
        ArrayList<University> universities;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getuniversities(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            universities = buildList(result);

            if (!universities.isEmpty()) {
                university = universities.get(0);
            }

            return university;
        } catch (SQLException e) {
            throw new Exception("Unable to get university", e);
        }
    }

    private static @NotNull ArrayList<University> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<University> universities = new ArrayList<>();

        while (result.next()) {
            University university = new University(
                    result.getInt("UniversityId"),
                    result.getString("UniversityName"),
                    result.getString("LocalAddress"),
                    result.getInt("DistrictId")
            );

            universities.add(university);
        }

        return universities;
    }

    public static @NotNull ArrayList<University> getAll() throws Exception {
        ArrayList<University> universities = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getuniversities(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            universities = buildList(result);

            return universities;
        } catch (SQLException e) {
            throw new Exception("Unable to get universities: ", e);
        }
    }

    public static @NotNull ArrayList<University> getAllWithoutNewspaper() throws Exception {
        ArrayList<University> universities;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getUniNoNewspaper}");
            ResultSet result = sql.executeQuery();
            universities = buildList(result);

            return universities;
        } catch (SQLException e) {
            throw new Exception("Unable to get universities: ", e);
        }
    }
}
