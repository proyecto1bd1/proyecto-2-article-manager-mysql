/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class PersonXNewspaper extends AbstractModel {
    private Integer NewsPaperId;
    private Integer PersonId;

    public PersonXNewspaper() {
    }

    public PersonXNewspaper(Integer NewsPaperId, Integer PersonId) {
        this.NewsPaperId = NewsPaperId;
        this.PersonId = PersonId;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    @Override
    public String toString() {
        return String.format(
                "PersonXNewspaper{ NewsPaperId: %d PersonId: %d }",
                NewsPaperId,
                PersonId
        );
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            CallableStatement sql;
            assert connection != null;
            sql = connection.prepareCall("{call createpersonXnewspaper(?, ?)}");
            sql.setInt(1, this.NewsPaperId);
            sql.setInt(2, this.PersonId);
            sql.executeQuery();

        } catch (SQLException e) {
            throw new Exception("Error: Unable to save relationship between person and newspaper", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.NewsPaperId == null & this.PersonId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletepersonXnewspaper(?, ?)}");
            sql.setInt(1, this.NewsPaperId);
            sql.setInt(2, this.PersonId);
            sql.executeQuery();
            this.NewsPaperId = null;
            this.PersonId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete relationship between person and newspaper", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> personNewspaperMap = new HashMap<>();

        personNewspaperMap.put("newspaperId", NewsPaperId.toString());
        personNewspaperMap.put("personId", PersonId.toString());

        return personNewspaperMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("newspaperId");
        columns.add("personId");

        return columns;
    }

    public static PersonXNewspaper find(Integer newspaperId, Integer personId) throws Exception {
        PersonXNewspaper personXnewspaper = null;
        ArrayList<PersonXNewspaper> personNewspapers;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpersonXnewspaper(?, ?)}");
            sql.setInt(1, newspaperId);
            sql.setInt(2, personId);

            ResultSet result = sql.executeQuery();

            personNewspapers = buildList(result);

            if (!personNewspapers.isEmpty()) {
                personXnewspaper = personNewspapers.get(0);
            }

            return personXnewspaper;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between person and newspaper", e);
        }
    }

    public static @NotNull ArrayList<PersonXNewspaper> getAll() throws Exception {
        ArrayList<PersonXNewspaper> personNewspapers;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpersonXnewspaper(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            personNewspapers = buildList(result);

            return personNewspapers;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between person and newspaper", e);
        }
    }

    public static @NotNull ArrayList<PersonXNewspaper> getAll(Integer personId) throws Exception {
        ArrayList<PersonXNewspaper> personNewspapers;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getNewsPaperRows(?)}");
            sql.setInt(1, personId);

            ResultSet result = sql.executeQuery();

            personNewspapers = buildList(result);

            return personNewspapers;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between person and newspaper", e);
        }
    }

    private static @NotNull ArrayList<PersonXNewspaper> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<PersonXNewspaper> personNewspapers = new ArrayList<>();

        while (result.next()) {
            PersonXNewspaper personXnewspaper = new PersonXNewspaper(
                    result.getInt("newspaperId"),
                    result.getInt("personId")
            );
            personNewspapers.add(personXnewspaper);
        }

        return personNewspapers;
    }
}
