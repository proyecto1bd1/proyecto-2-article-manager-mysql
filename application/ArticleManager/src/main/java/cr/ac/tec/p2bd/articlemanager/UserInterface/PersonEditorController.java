/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.IdTypeController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.time.LocalDate;
import java.util.Base64;
import java.util.HashMap;
import java.util.Objects;

public class PersonEditorController {
    private final PersonController personController = new PersonController();
    private final IdTypeController idTypeController = new IdTypeController();
    public TextField txtUsername;
    private HashMap<String, String> personToEdit = null;
    private boolean updateProfileMode = false;
    private boolean editMode = false;

    private String idMask = "";

    private File profilePic = null;

    @FXML
    public TextField txtName;

    @FXML
    public TextField txtFirstLastName;

    @FXML
    public TextField txtSecondLastName;

    @FXML
    public TextField txtIdCard;

    @FXML
    public DatePicker dtBirthdate;

    @FXML
    public ChoiceBox<Choice> cbIdType;

    @FXML
    public ChoiceBox<Choice> cbGender;

    @FXML
    public ChoiceBox<Choice> cbCountry;

    @FXML
    public ChoiceBox<Choice> cbProvince;

    @FXML
    public ChoiceBox<Choice> cbCanton;

    @FXML
    public ChoiceBox<Choice> cbDistrict;

    @FXML
    public TextArea txtLocalAddress;

    @FXML
    public PasswordField pswNewPassword;

    @FXML
    public PasswordField pswConfirmPassword;

    @FXML
    public ImageView imgProfilePic;

    @FXML
    public Button btnManageTelephone;

    @FXML
    public Button btnManageEmail;


    @FXML
    protected void initialize() throws IOException {
        cbGender.getItems().addAll(ChoiceBuilder.getGenderList());
        cbIdType.getItems().addAll(ChoiceBuilder.getIdTypeList());

        cbCountry.getItems().addAll(ChoiceBuilder.getCountryList());

        cbCountry.setOnAction(this::onCountrySelected);
        cbProvince.setOnAction(this::onProvinceSelected);
        cbCanton.setOnAction(this::onCantonSelected);

        cbIdType.setOnAction(this::onIdTypeSelected);

        btnManageEmail.setVisible(false);
        btnManageTelephone.setVisible(false);
        pswNewPassword.setVisible(false);
    }

    public void setProfileUpdateMode() throws IOException {
        if (SessionManager.getInstance().isGuest()) {
            btnManageEmail.setVisible(false);
            btnManageTelephone.setVisible(false);
            pswNewPassword.setVisible(false);
            pswConfirmPassword.setPromptText("Password");
        } else {
            personToEdit = SessionManager.getInstance().getPersonData();
            updateProfileMode = true;
            editMode = false;
            txtUsername.setEditable(false);
            btnManageEmail.setVisible(true);
            btnManageTelephone.setVisible(true);
            pswNewPassword.setVisible(true);
        }

        fillFormData();
    }

    private void fillFormData() throws IOException {
        if (personToEdit != null) {
            HashMap<String, HashMap<String, String>> personFullLocation = Utils.getDistrictFullLocation(
                    Integer.valueOf(personToEdit.get("DistrictId"))
            );

            txtUsername.setText(personToEdit.get("Username"));

            final Choice[] selectedCountry = new Choice[1];

            assert personFullLocation != null;
            cbCountry.getItems().forEach(countryChoice -> {
                if (countryChoice.id.equals(Integer.valueOf(personFullLocation.get("country").get("countryId")))) {
                    selectedCountry[0] = countryChoice;
                }
            });

            cbCountry.setValue(selectedCountry[0]);

            final Choice[] selectedProvince = new Choice[1];

            cbProvince.getItems().forEach(provinceChoice -> {
                if (provinceChoice.id.equals(Integer.valueOf(personFullLocation.get("province").get("provinceId")))) {
                    selectedProvince[0] = provinceChoice;
                }
            });

            cbProvince.setValue(selectedProvince[0]);

            final Choice[] selectedCanton = new Choice[1];

            cbCanton.getItems().forEach(cantonChoice -> {
                if (cantonChoice.id.equals(Integer.valueOf(personFullLocation.get("canton").get("cantonId")))) {
                    selectedCanton[0] = cantonChoice;
                }
            });

            cbCanton.setValue(selectedCanton[0]);

            final Choice[] selectedDistrict = new Choice[1];

            cbDistrict.getItems().forEach(districtChoice -> {
                if (districtChoice.id.equals(Integer.valueOf(personFullLocation.get("district").get("districtId")))) {
                    selectedDistrict[0] = districtChoice;
                }
            });

            cbDistrict.setValue(selectedDistrict[0]);

            txtName.setText(personToEdit.get("Name"));
            txtFirstLastName.setText(personToEdit.get("FirstLastName"));
            txtSecondLastName.setText(personToEdit.get("SecondLastName"));

            LocalDate birthdate = LocalDate.parse(personToEdit.get("Birthdate"));

            dtBirthdate.setValue(birthdate);

            txtLocalAddress.setText(personToEdit.get("LocalAddress"));

            final Choice[] selectedGender = new Choice[1];

            cbGender.getItems().forEach(genderChoice -> {
                if (genderChoice.id.equals(Integer.valueOf(personToEdit.get("GenderId")))) {
                    selectedGender[0] = genderChoice;
                }
            });

            cbGender.setValue(selectedGender[0]);

            final Choice[] selectedIdType = new Choice[1];

            cbIdType.getItems().forEach(idTypeChoice -> {
                if (idTypeChoice.id.equals(Integer.valueOf(personToEdit.get("TypeId")))) {
                    selectedIdType[0] = idTypeChoice;
                }
            });

            cbIdType.setValue(selectedIdType[0]);

            txtIdCard.setText(personToEdit.get("IdCard"));

            formatId();

            if (!personToEdit.get("Photo").isEmpty()) {
                byte[] imageStream = Base64.getDecoder().decode(personToEdit.get("Photo"));
                InputStream imageData = new ByteArrayInputStream(imageStream);
                Image image = new Image(imageData);
                imgProfilePic.setImage(image);

                profilePic = File.createTempFile("profile", ".jpg");
                FileUtils.writeByteArrayToFile(profilePic, imageStream);
            }
        }
    }

    public void setEditMode(HashMap<String, String> person) throws IOException {
        personToEdit = person;
        updateProfileMode = false;
        editMode = true;
        txtUsername.setEditable(false);
        pswNewPassword.setVisible(true);
        pswConfirmPassword.setVisible(false);
        fillFormData();
    }

    public void onCountrySelected(ActionEvent event) {
        Choice countrySelected = cbCountry.getValue();
        cbProvince.getItems().clear();
        cbProvince.getItems().addAll(ChoiceBuilder.getProvinceList(countrySelected.id));
    }

    public void onProvinceSelected(ActionEvent event) {
        Choice provinceSelected = cbProvince.getValue();
        cbCanton.getItems().clear();
        cbCanton.getItems().addAll(ChoiceBuilder.getCantonList(provinceSelected.id));
    }

    public void onCantonSelected(ActionEvent event) {
        Choice cantonSelected = cbCanton.getValue();
        cbDistrict.getItems().clear();
        cbDistrict.getItems().addAll(ChoiceBuilder.getDistrictList(cantonSelected.id));
    }

    public void onIdTypeSelected(ActionEvent event) {
        Choice idTypeChoice = cbIdType.getValue();

        try {
            HashMap<String, String> idType = idTypeController.getIdTypeRow(idTypeChoice.id);

            idMask = idType.get("mask");

            formatId();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch ID Type");
            alert.setHeaderText("We were unable to fetch the ID Type");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void formatId() {
        String idText = txtIdCard.getText().replace("-", "");
        StringBuilder idFormat = new StringBuilder();

        int j = 0;
        for(int i = 0; i < idText.length(); i++) {
            if (i < idMask.length() && j < idMask.length()) {
                if (idMask.charAt(j) != 'X') {
                    if (idMask.charAt(j) != idText.charAt(i)) {
                        idFormat.append(idMask.charAt(j));
                        j++;
                    }
                }
                idFormat.append(idText.charAt(i));
                j++;
            }
        }

        txtIdCard.setText(idFormat.toString());
    }

    public void onIdInputChanged(KeyEvent event) {
        formatId();
        txtIdCard.end();
    }

    public void onImgClick(MouseEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Image");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg")
        );

        profilePic = fileChooser.showOpenDialog(imgProfilePic.getScene().getWindow());

        if (profilePic != null) {
            Image pic = new Image(profilePic.toString());
            imgProfilePic.setImage(pic);
        }
    }

    public void onClose(ActionEvent event) throws IOException {
        if (updateProfileMode) {
            SessionManager.getInstance().getHomeController().onLoadLatestNews(event);
        } else if (editMode) {
            SessionManager.getInstance().getHomeController().onManagePerson();
        } else {
            if (SessionManager.getInstance().isAdmin()) {
                SessionManager.getInstance().getHomeController().onManagePerson();
            } else {
                FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("login-view.fxml")));
                Stage stage = (Stage)(((Node)event.getSource()).getScene().getWindow());
                Scene scene = new Scene(loader.load(), 300, 400);
                stage.setScene(scene);
                stage.show();
            }
        }
    }

    public void onSave(ActionEvent event) {
        if (updateProfileMode) {
            try {
                SessionManager.getInstance().updateProfile(
                        Integer.valueOf(this.personToEdit.get("PersonId")),
                        txtIdCard.getText().replace("-", ""),
                        txtName.getText(),
                        txtFirstLastName.getText(),
                        txtSecondLastName.getText(),
                        dtBirthdate.getValue().toString(),
                        profilePic,
                        txtLocalAddress.getText(),
                        pswConfirmPassword.getText(),
                        pswNewPassword.getText(),
                        cbDistrict.getValue().id,
                        cbGender.getValue().id,
                        cbIdType.getValue().id
                );

                Alert confirmation = new Alert(Alert.AlertType.INFORMATION);
                confirmation.setTitle("Profile updated");
                confirmation.setHeaderText("Your profile was success fully updated");
                confirmation.setContentText("You will be sent to the home screen");

                if (confirmation.showAndWait().get() == ButtonType.OK) {
                    SessionManager.getInstance().getHomeController().onLoadLatestNews(event);
                }
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unable to update profile");
                alert.setHeaderText("We were unable to update your profile");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        } else if (editMode) {
            try {
                personController.updatePersonAdmin(
                        Integer.valueOf(this.personToEdit.get("PersonId")),
                        txtIdCard.getText().replace("-", ""),
                        txtName.getText(),
                        txtFirstLastName.getText(),
                        txtSecondLastName.getText(),
                        dtBirthdate.getValue().toString(),
                        profilePic,
                        txtLocalAddress.getText(),
                        pswNewPassword.getText(),
                        cbDistrict.getValue().id,
                        cbGender.getValue().id,
                        cbIdType.getValue().id
                );

                Alert confirmation = new Alert(Alert.AlertType.INFORMATION);
                confirmation.setTitle("User data updated");
                confirmation.setHeaderText("The user data has been updated");
                confirmation.setContentText("You will be sent to the user management screen");

                if (confirmation.showAndWait().get() == ButtonType.OK) {
                    SessionManager.getInstance().getHomeController().onManagePerson();
                }
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unable to update user");
                alert.setHeaderText("We fail at updating the user data");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        } else {
            try {
                personController.createPerson(
                        txtIdCard.getText().replace("-", ""),
                        txtName.getText(),
                        txtFirstLastName.getText(),
                        txtSecondLastName.getText(),
                        dtBirthdate.getValue().toString(),
                        profilePic,
                        txtLocalAddress.getText(),
                        txtUsername.getText(),
                        pswConfirmPassword.getText(),
                        cbDistrict.getValue().id,
                        cbGender.getValue().id,
                        cbIdType.getValue().id
                );

                if (SessionManager.getInstance().isAdmin()) {
                    Alert confirmation = new Alert(Alert.AlertType.INFORMATION);
                    confirmation.setTitle("Account created successfully");
                    confirmation.setHeaderText("A new account was created");
                    confirmation.setContentText("You will be sent to person management screen");

                    if (confirmation.showAndWait().get() == ButtonType.OK) {
                        SessionManager.getInstance().getHomeController().onManagePerson();
                    }
                } else {
                    Alert confirmation = new Alert(Alert.AlertType.INFORMATION);
                    confirmation.setTitle("Account created successfully");
                    confirmation.setHeaderText("You can now log into the system");
                    confirmation.setContentText("You will be sent to the login screen");

                    if (confirmation.showAndWait().get() == ButtonType.OK) {
                        FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("login-view.fxml")));
                        Stage stage = (Stage)(((Node)event.getSource()).getScene().getWindow());
                        Scene scene = new Scene(loader.load(), 300, 400);
                        stage.setScene(scene);
                        stage.show();
                    }
                }

            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unable to create user");
                alert.setHeaderText("We fail to create the user");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
    }
}
