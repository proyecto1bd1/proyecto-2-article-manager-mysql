/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class DigitalNewsPaper extends AbstractModel {
    private Integer NewsPaperId;
    private String NewsPaperName;
    private Integer CommitteeId;
    private Integer UniversityId;

    public DigitalNewsPaper() {

    }

    public DigitalNewsPaper(Integer NewsPaperId, String NewsPaperName, Integer CommitteeId, Integer UniversityId) {
        this.NewsPaperId = NewsPaperId;
        this.NewsPaperName = NewsPaperName;
        this.CommitteeId = CommitteeId;
        this.UniversityId = UniversityId;
    }

    public DigitalNewsPaper(String NewsPaperName, Integer CommitteeId, Integer UniversityId) {
        this.NewsPaperName = NewsPaperName;
        this.CommitteeId = CommitteeId;
        this.UniversityId = UniversityId;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setNewsPaperName(String NewsPaperName) {
        this.NewsPaperName = NewsPaperName;
    }

    public void setCommitteeId(Integer CommitteeId) {
        this.CommitteeId = CommitteeId;
    }

    public void setUniversityId(Integer UniversityId) {
        this.UniversityId = UniversityId;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public String getNewsPaperName() {
        return NewsPaperName;
    }

    public Integer getCommitteeId() {
        return CommitteeId;
    }

    public Integer getUniversityId() {
        return UniversityId;
    }


    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (NewsPaperId != null) {
                CallableStatement sql = connection.prepareCall("{call updateDigitalnewspaper(?, ?, ?, ?)}");
                sql.setInt(1, NewsPaperId);
                sql.setString(2, NewsPaperName);
                sql.setInt(3, CommitteeId);
                sql.setInt(4, UniversityId);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createDigitalnewspaper(?, ?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, NewsPaperName);
                sql.setInt(3, CommitteeId);
                sql.setInt(4, UniversityId);
                sql.execute();

                NewsPaperId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Unable to save news paper", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (NewsPaperId == null) return;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteDigitalnewspaper(?)}");
            sql.setInt(1, NewsPaperId);
            sql.executeQuery();

            NewsPaperId = null;
        } catch (SQLException e) {
            throw new Exception("Failed to delete news paper", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "DigitalNewsPaper{ Id: %d Name: %s Committee: %d }",
                NewsPaperId,
                NewsPaperName,
                CommitteeId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> digitalNewsPaperMap = new HashMap<>();

        digitalNewsPaperMap.put("NewsPaperId", NewsPaperId.toString());
        digitalNewsPaperMap.put("NewsPaperName", NewsPaperName);
        digitalNewsPaperMap.put("CommitteeId", CommitteeId.toString());
        digitalNewsPaperMap.put("UniversityId", UniversityId.toString());

        return digitalNewsPaperMap;
    }

    public static @NotNull ArrayList<DigitalNewsPaper> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<DigitalNewsPaper> newspapers = new ArrayList<>();

        while (result.next()) {
            DigitalNewsPaper newspaper = new DigitalNewsPaper(
                    result.getInt("NewsPaperId"),
                    result.getString("NewsPaperName"),
                    result.getInt("CommitteeId"),
                    result.getInt("UniversityId")
            );

            newspapers.add(newspaper);
        }

        return newspapers;
    }

    public static DigitalNewsPaper find(Integer id) throws Exception {
        DigitalNewsPaper newspaper = null;
        ArrayList<DigitalNewsPaper> newspapers;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getDigitalnewspaper(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            newspapers = buildList(result);

            if (!newspapers.isEmpty()) {
                newspaper = newspapers.get(0);
            }

            return newspaper;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch newspaper", e);
        }
    }

    public static @NotNull ArrayList<DigitalNewsPaper> getAll() throws Exception {
        ArrayList<DigitalNewsPaper> newspapers;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getDigitalnewspaper(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            newspapers = buildList(result);

            return newspapers;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch newspaper", e);
        }
    }

    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("NewsPaperId");
        columns.add("NewsPaperName");
        columns.add("CommitteeId");
        columns.add("UniversityId");

        return columns;
    }
}
