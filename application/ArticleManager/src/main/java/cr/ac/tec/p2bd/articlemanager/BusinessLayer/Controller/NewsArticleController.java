/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Committee;
import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.NewsArticle;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.sql.Date;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class NewsArticleController {

    public NewsArticleController() {
    }

    public ArrayList<HashMap<String, String>> getNewsArticleRows() throws Exception {

        ArrayList<HashMap<String, String>> newsArticleRows = new ArrayList<>();

        ArrayList<NewsArticle> newsArticles = NewsArticle.getAll();

        newsArticles.forEach((NewsArticle article) -> {
            newsArticleRows.add(article.toMap());
        });

        return newsArticleRows;
    }

    public ArrayList<HashMap<String, String>> getNewsLatestArticleRows(Integer pageNumber, Integer newsPaperId) throws Exception {

        ArrayList<HashMap<String, String>> newsArticleRows = new ArrayList<>();

        ArrayList<NewsArticle> newsArticles = NewsArticle.getAllLatest(pageNumber, newsPaperId);

        newsArticles.forEach((NewsArticle article) -> {
            newsArticleRows.add(article.toMap());
        });

        return newsArticleRows;
    }

    public ArrayList<String> getNewsArticleColumns() throws Exception {
        Set<String> columns = NewsArticle.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getNewsArticleRow(Integer articleId) throws Exception {
        NewsArticle article = NewsArticle.find(articleId);

        return article.toMap();
    }

    public Integer createNewsArticle(String title, String text, String publicationDate, File photo, Integer authorId, Integer newsPaperId, Integer committeeIdProv) throws Exception {
        Date publicisedIn = Date.valueOf(publicationDate);

        byte[] photoConverted = photo != null ? FileUtils.readFileToByteArray(photo) : null;

        NewsArticle article = new NewsArticle(title, text, publicisedIn, photoConverted, authorId, authorId, newsPaperId, committeeIdProv);

        article.save();

        return article.getArticleId();
    }

    public String updateNewsArticle(Integer articleId, String title, String text, String publicationDate, File photo, Integer authorId, Integer newsPaperId, Integer committeeIdProv) throws Exception {
        NewsArticle article = NewsArticle.find(articleId);

        Date publicisedIn = Date.valueOf(publicationDate);

        byte[] photoConverted = photo != null ? FileUtils.readFileToByteArray(photo) : null;

        article.setTitle(title);
        article.setContent(text);
        article.setPublicationDate(publicisedIn);
        article.setPhoto(photoConverted);
        article.setAuthorId(authorId);
        article.setArticleId(articleId);
        article.setNewsPaperId(newsPaperId);
        article.setCommitteeIdAprov(committeeIdProv);

        article.save();

        return article.getTitle();
    }

    public boolean deleteNewsArticle(Integer committeeId) throws Exception {
        Committee committee = Committee.find(committeeId);

        committee.delete();

        return committee.getCommitteeId() == null;
    }

    public Integer latestNewsPages(Integer newsPaperId) throws Exception {
        return NewsArticle.getLatestNewsPages(newsPaperId);
    }

    public void approveArticle(Integer articleId, Integer committeeId) throws Exception {
        NewsArticle article = NewsArticle.find(articleId);

        if (article != null) {
            article.setCommitteeIdAprov(committeeId);
            article.approveArticle();
        }
    }

    public ArrayList<HashMap<String, String>> getArticlesPending(Integer newspaperId, Integer pageNumber) throws Exception {

        ArrayList<HashMap<String, String>> newsArticleRows = new ArrayList<>();

        ArrayList<NewsArticle> newsArticles = NewsArticle.getAllPending(pageNumber, newspaperId);

        newsArticles.forEach((NewsArticle article) -> {
            newsArticleRows.add(article.toMap());
        });

        return newsArticleRows;
    }

    public Integer pendingNewsPages(Integer newsPaperId) throws Exception {
        return NewsArticle.getPendingNewsPages(newsPaperId);
    }
}
