/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class IdType extends AbstractModel {

    private Integer TypeId;
    private String Description;
    private String Mask;

    public IdType() {
    }

    private IdType(Integer TypeId, String Description, String Mask) {
        this.TypeId = TypeId;
        this.Description = Description;
        this.Mask = Mask;
    }

    public IdType(String Description, String Mask) {
        this.Description = Description;
        this.Mask = Mask;
    }

    public Integer getTypeId() {
        return TypeId;
    }

    public String getDescription() {
        return Description;
    }

    public String getMask() {
        return Mask;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setMask(String Mask) {
        this.Mask = Mask;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (TypeId != null) {
                CallableStatement sql = connection.prepareCall("{call updateIdtype(?, ?, ?)}");
                sql.setInt(1, TypeId);
                sql.setString(2, Description);
                sql.setString(3, Mask);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createIdtype(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, Description);
                sql.setString(3, Mask);
                sql.execute();

                TypeId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving idType", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (TypeId == null) return;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteIdtype(?)}");
            sql.setInt(1, TypeId);
            sql.executeQuery();

            TypeId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting idType", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "IdType{ Id: %d Name: %s Mask: %s }",
                TypeId,
                Description,
                Mask
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> idTypeMap = new HashMap<>();

        idTypeMap.put("typeId", TypeId.toString());
        idTypeMap.put("description", Description);
        idTypeMap.put("mask", Mask);

        return idTypeMap;
    }

    private static @NotNull ArrayList<IdType> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<IdType> idTypes = new ArrayList<>();

        while (result.next()) {
            IdType IdType = new IdType(
                    result.getInt("TypeId"),
                    result.getString("DESCRIPTION"),
                    result.getString("MASK")
            );

            idTypes.add(IdType);
        }

        return idTypes;
    }

    public static IdType find(Integer id) throws Exception {
        IdType idType = null;
        ArrayList<IdType> idTypes;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getIdtype(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();
           

            idTypes = buildList(result);

            if (!idTypes.isEmpty()) {
                idType = idTypes.get(0);
            }

            return idType;
        } catch (SQLException e) {
            throw new Exception("Error fetching typeId", e);
        }
    }

    public static @NotNull ArrayList<IdType> getAll() throws Exception {
        ArrayList<IdType> idTypes;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getIdtype(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            idTypes = buildList(result);
            return idTypes;
        } catch (SQLException e) {
            throw new Exception("Error fetching typeId", e);
        }
    }


    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("idTypeId");
        columns.add("Description");
        columns.add("Mask");

        return columns;
    }
}
