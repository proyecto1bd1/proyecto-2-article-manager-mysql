package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DigitalNewsPaperController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonXNewspaperController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class LoginController {

    private final PersonController personController = new PersonController();
    private final PersonXNewspaperController personXNewspaperController = new PersonXNewspaperController();
    private final DigitalNewsPaperController digitalNewsPaperController = new DigitalNewsPaperController();

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    public void submit(ActionEvent event) throws IOException {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if (username.isBlank() || password.isBlank()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Missing fields required");
            alert.setContentText("Please fill the username and password fields or enter as guest");
            return;
        }

        try {
            HashMap<String, String> personData = personController.login(username, password);
            SessionManager session = SessionManager.getInstance();
            session.setPersonData(personData);
            Integer personId = Integer.valueOf(session.getPersonData().get("PersonId"));

            ArrayList<HashMap<String, String>> newsPapers = personXNewspaperController.getPersonNewspaperRows(personId);

            if (newsPapers.size() > 1) {
                ArrayList<Integer> newsPapersIds = new ArrayList<>();

                for (var newsPaper : newsPapers) {
                    newsPapersIds.add(
                            Integer.valueOf(newsPaper.get("newspaperId"))
                    );
                }

                Stage modalDialog = new Stage();
                FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("select-newspaper-view.fxml"));
                Parent dialogRoot = dialogLoader.load();
                SelectNewspaperController selectNewspaperController = dialogLoader.getController();
                selectNewspaperController.setNewsPapersIds(newsPapersIds);
                Scene dialogScene = new Scene(dialogRoot,260, 120);
                modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
                modalDialog.setScene(dialogScene);
                modalDialog.setResizable(false);
                modalDialog.showAndWait();

                session.setSelectedNewsPaper(selectNewspaperController.getSelectedNewsPaperId());
            } else {
                session.setSelectedNewsPaper(Integer.valueOf(newsPapers.get(0).get("newspaperId")));
            }

            FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("home-view.fxml")));
            Stage stage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            Scene scene = new Scene(loader.load(), 600, 600);
            stage.setScene(scene);
            session.setHomeController(loader.getController());
            stage.show();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to log in");
            alert.setHeaderText("We where unable to log into the system");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onRegisterPerson(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("person-editor-view.fxml")));
        Stage stage = (Stage)(((Node)event.getSource()).getScene().getWindow());
        Scene scene = new Scene(loader.load(), 600, 400);
        stage.setScene(scene);
        stage.show();
    }

    public void onGuest(ActionEvent event) throws IOException {

        try {
            SessionManager session = SessionManager.getInstance();
            session.setGuest();

            ArrayList<HashMap<String, String>> newsPapers = digitalNewsPaperController.getDigitalNewsPaperRows();

            if (newsPapers.size() > 1) {
                ArrayList<Integer> newsPapersIds = new ArrayList<>();

                for (var newsPaper : newsPapers) {
                    newsPapersIds.add(
                            Integer.valueOf(newsPaper.get("NewsPaperId"))
                    );
                }

                Stage modalDialog = new Stage();
                FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("select-newspaper-view.fxml"));
                Parent dialogRoot = dialogLoader.load();
                SelectNewspaperController selectNewspaperController = dialogLoader.getController();
                selectNewspaperController.setNewsPapersIds(newsPapersIds);
                Scene dialogScene = new Scene(dialogRoot,260, 120);
                modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
                modalDialog.setScene(dialogScene);
                modalDialog.setResizable(false);
                modalDialog.showAndWait();

                session.setSelectedNewsPaper(selectNewspaperController.getSelectedNewsPaperId());

                FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("home-view.fxml")));
                Stage stage = (Stage)(((Node)event.getSource()).getScene().getWindow());
                Scene scene = new Scene(loader.load(), 600, 600);
                stage.setScene(scene);
                session.setHomeController(loader.getController());
                stage.show();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to enter as guest");
            alert.setHeaderText("We where unable to enter in the system");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }
}