/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.NewsArticleType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author
 */
public class NewsArticleTypeController {
    public ArrayList<HashMap<String, String>> getNewsArticleTypeRows() throws Exception {

        ArrayList<HashMap<String, String>> newsArticleTypeRows = new ArrayList<>();

        ArrayList<NewsArticleType> newsArticlesType = NewsArticleType.getAll();

        newsArticlesType.forEach((NewsArticleType newsArticleType) -> {
            newsArticleTypeRows.add(newsArticleType.toMap());
        });

        return newsArticleTypeRows;
    }

    public ArrayList<String> getNewsArticleTypeColumns() throws Exception {
        Set<String> columns = NewsArticleType.getColumns();

        return new ArrayList<>(columns);
    }


    public HashMap<String, String> getNewsArticleRow(Integer TypeId) throws Exception {
        NewsArticleType newArticleType = NewsArticleType.find(TypeId);

        return newArticleType != null ? newArticleType.toMap() : null;
    }

    public Integer createNewsArticle(String description) throws Exception {
        NewsArticleType newsArticleType = new NewsArticleType(description);

        newsArticleType.save();

        return newsArticleType.getTypeId();
    }

    public String updateNewsArticle(Integer TypeId, String description) throws Exception {
        NewsArticleType newsArticleType = NewsArticleType.find(TypeId);

        newsArticleType.setDescription(description);
        newsArticleType.save();

        return newsArticleType.getDescription();
    }

    public boolean deleteNewsArticle(Integer TypeId) throws Exception {
        NewsArticleType newsArticleType = NewsArticleType.find(TypeId);

        newsArticleType.delete();

        return newsArticleType.getTypeId() == null;
    }
}
