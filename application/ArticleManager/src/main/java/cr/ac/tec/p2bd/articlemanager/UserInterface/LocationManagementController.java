/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CantonController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CountryController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DistrictController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProvinceController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class LocationManagementController {

    private final CountryController countryController = new CountryController();
    private final ProvinceController provinceController = new ProvinceController();
    private final CantonController cantonController = new CantonController();
    private final DistrictController districtController = new DistrictController();

    public void onCreateLocation(ActionEvent event) throws IOException {
        SessionManager.getInstance().getHomeController().onCreateLocation();;
    }

    public void onEditLocation(ActionEvent event) throws IOException {
        HashMap<String, String> location = createLocationPicker(event);

        if (location != null) {
            SessionManager.getInstance().getHomeController().onEditLocation(location);
        }
    }

    public void onDeleteLocation(ActionEvent event) throws IOException {
        HashMap<String, String> location = createLocationPicker(event);

        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle("Are you sure?");
        confirmation.setHeaderText("The deletion of a location is irreversible");
        confirmation.setContentText("Are you sure you want to delete?");

        if (confirmation.showAndWait().get() == ButtonType.CANCEL) {
            return;
        }

        try {
            if (location != null) {
                if (location.get("countryId") != null && location.get("countryName") != null) {
                    countryController.deleteCountry(Integer.valueOf(location.get("countryId")));
                } else if (location.get("provinceId") != null && location.get("provinceName") != null) {
                    provinceController.deleteProvince(Integer.valueOf(location.get("provinceId")));
                } else if (location.get("cantonId") != null && location.get("cantonName") != null) {
                    cantonController.deleteCanton(Integer.valueOf(location.get("cantonId")));
                } else if (location.get("districtId") != null && location.get("districtName") != null) {
                    districtController.deleteDistrict(Integer.valueOf(location.get("districtId")));
                }
            }

            Alert information = new Alert(Alert.AlertType.INFORMATION);
            information.setTitle("Deletion complete");
            information.setHeaderText("We successfully deleted the location");
            information.setContentText("It cannot longer be used by the system");
            information.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to delete location");
            alert.setHeaderText("Failed to delete location");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    private HashMap<String, String> createLocationPicker(ActionEvent event) throws IOException {
        Stage modalDialog = new Stage();
        FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("location-picker-view.fxml"));
        Parent dialogRoot = dialogLoader.load();
        LocationPickerController locationPickerController = dialogLoader.getController();
        Scene dialogScene = new Scene(dialogRoot,600, 150);
        modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
        modalDialog.setScene(dialogScene);
        modalDialog.setResizable(false);
        modalDialog.showAndWait();

        return locationPickerController.getLocation();
    }
}
