/*
 * The MIT License
 *
 * Copyright 2022 Daniel G.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Daniel G
 */
public class ProductController {
    public ArrayList<HashMap<String, String>> getProductRows() throws Exception {

        ArrayList<HashMap<String, String>> productRows = new ArrayList<>();

        ArrayList<Product> products = Product.getAll();

        products.forEach((Product product) -> {
            productRows.add(product.toMap());
        });

        return productRows;
    }

    public ArrayList<String> getProductColumns() throws Exception {
        Set<String> columns = Product.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getProductRow(Integer productId) throws Exception {
        Product product = Product.find(productId);
        return product.toMap();
    }

    public Integer createProduct(String name, Integer price, Integer numberInStock, Integer storeId) throws Exception {
        Product product = new Product(name, price, numberInStock, storeId);

        product.save();

        return product.getProductId();
    }

    public void updateProduct(Integer productId, String name, Integer price, Integer numberInStock, Integer storeId) throws Exception {
        Product product = Product.find(productId);

        product.setName(name);
        product.setNumberInStock(numberInStock);
        product.setPointsPrice(price);
        product.setStoreId(storeId);
        product.save();
    }

    public ArrayList<Product> getAllProduct() throws Exception {
        return Product.getAll();
    }

    public boolean deleteProduct(Integer productId) throws Exception {
        Product product = Product.find(productId);

        product.delete();

        return product.getProductId() == null;
    }
}
