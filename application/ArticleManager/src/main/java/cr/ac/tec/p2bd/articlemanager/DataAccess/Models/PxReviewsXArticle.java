/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class PxReviewsXArticle extends AbstractModel {

    private Integer PersonId;
    private Integer ArticleId;
    private Integer nStars;

    public PxReviewsXArticle() {
    }

    public PxReviewsXArticle(Integer PersonId, Integer ArticleId, Integer nStars) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.nStars = nStars;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setnStars(Integer nStars) {
        this.nStars = nStars;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public Integer getnStars() {
        return nStars;
    }

    @Override
    public String toString() {
        return String.format(
                "PxReviewsXArticle{ PersonId: %d ArticleId: %d nStars %d}",
                PersonId,
                ArticleId,
                nStars
        );
    }

    @Override
    public void save() throws Exception {
        PxReviewsXArticle review = PxReviewsXArticle.find(PersonId, ArticleId);

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (review != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updatePxReviewsXArticle(?, ?, ?)}");
                sql.setInt(1, this.PersonId);
                sql.setInt(2, this.ArticleId);
                sql.setInt(3, this.nStars);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{call createPxReviewsXArticle(?, ?, ?)}");
                sql.setInt(1, this.PersonId);
                sql.setInt(2, this.ArticleId);
                sql.setInt(3, this.nStars);
                sql.executeQuery();
            }
        } catch (SQLException e) {
            throw new Exception("Error saving review", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.ArticleId == null & this.PersonId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletePxReviewsXArticle(?, ?)}");
            sql.setInt(1, this.PersonId);
            sql.setInt(2, this.ArticleId);
            sql.executeQuery();
            this.PersonId = null;
            this.ArticleId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete review", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> PxReviewsXArticleMap = new HashMap<>();

        PxReviewsXArticleMap.put("personId", PersonId.toString());
        PxReviewsXArticleMap.put("articleId", ArticleId.toString());
        PxReviewsXArticleMap.put("nStars", nStars.toString());
        return PxReviewsXArticleMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("personId");
        columns.add("articleId");
        columns.add("nStars");
        return columns;
    }

    public static PxReviewsXArticle find(Integer personId, Integer articleId) throws Exception {
        PxReviewsXArticle pxReviewsXArticle = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPxReviewsXArticle(?, ?)}");
            sql.setInt(1, personId);
            sql.setInt(2, articleId);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                pxReviewsXArticle = new PxReviewsXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getInt("nStars")
                );
            }

            return pxReviewsXArticle;
        } catch (SQLException e) {
            throw new Exception("Unable to get review", e);
        }
    }

    public static ArrayList<PxReviewsXArticle> getAll() throws Exception {
        ArrayList<PxReviewsXArticle> pxReviewsXArticles = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPxReviewsXArticle(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                PxReviewsXArticle pxReviewsXArticle = new PxReviewsXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getInt("nStars")
                );
                pxReviewsXArticles.add(pxReviewsXArticle);
            }

            return pxReviewsXArticles;
        } catch (SQLException e) {
            throw new Exception("Unable to get reviews", e);
        }
    }
}
