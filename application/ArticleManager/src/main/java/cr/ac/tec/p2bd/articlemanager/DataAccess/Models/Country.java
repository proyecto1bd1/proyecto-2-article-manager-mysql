/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Types;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * Represents a Country in the system
 *
 * @author Diego Herrera, Daniel G
 */
public class Country extends AbstractModel {

    private Integer countryId;
    private String countryName;

    /**
     * Initialize the object empty
     */
    public Country() {
        this.countryId = null;
        this.countryName = null;
    }

    /**
     * Initialize the object with a name, handy for new countries
     *
     * @param countryName The name of the country
     */
    public Country(String countryName) {
        this.countryName = countryName;
    }

    /**
     * Initialize the object with the internal ID and name, meant to be use for
     * dataset initialization
     *
     * @param countryId   The internal ID of the country
     * @param countryName The name of the country
     */
    private Country(Integer countryId, String countryName) {
        this.countryId = countryId;
        this.countryName = countryName;
    }

    /**
     * Changes the value of the name of the country
     *
     * @param countryName The name to be store
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * Gets the internal ID of the country
     *
     * @return The ID of the country
     */
    public Integer getCountryId() {
        return countryId;
    }

    /**
     * Gets the name of the country
     *
     * @return Name of the country
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Gives a representation of the object data as a string
     *
     * @return The object representation
     */
    @Override
    public String toString() {
        return String.format(
                "Id: %d Name: %s",
                this.countryId,
                this.countryName
        );
    }

    /**
     * Returns a key, value map that represents the object, the keys are the
     * attributes and the values their values, handy for UI manipulation
     *
     * @return A key, value representation
     */
    @Override
    public HashMap<String, String> toMap() {

        HashMap<String, String> countryMap = new HashMap<>();

        countryMap.put("countryId", countryId.toString());
        countryMap.put("countryName", countryName);

        return countryMap;
    }

    /**
     * Stores the data of the object in the database, if the object has the ID
     * defined, it will update the object, otherwise will create a new object
     * and set the ID for future operations
     *
      If something wrong happens when saving, it will be
     *                   notified
     */
    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.countryId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateCountry(?, ?)}");
                sql.setInt(1, this.countryId);
                sql.setString(2, this.countryName);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createCountry(?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.countryName);
                sql.execute();
                this.countryId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving country", e);
        }
    }

    /**
     * Deletes the object from the database, but only if it was store
     *
      If it cannot be deleted from the database, it returns
     *                   an error
     */
    @Override
    public void delete() throws Exception {
        // If id is not set, do nothing
        if (this.countryId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteCountry(?)}");
            sql.setInt(1, this.countryId);
            sql.executeQuery();
            this.countryId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting country", e);
        }
    }

    /**
     * Columns of the entity country
     *
     * @return A set with the columns of the entity
     */
    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("countryId");
        columns.add("countryName");

        return columns;
    }

    /**
     * It returns a country object based on the id
     *
     * @param id The id to search for the object
     * @return The country that matches the id, or null if none
      If it cannot connect to the database it will throw an
     *                   error
     */
    public static Country find(Integer id) throws Exception {
        Country country = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCountries(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                country = new Country(
                        result.getInt("CountryId"),
                        result.getString("CountryName")
                );
            }

            return country;
        } catch (SQLException e) {
            throw new Exception("Unable to get country:", e);
        }
    }

    /**
     * It returns a list of countries from the database
     *
     * @return The list of all available countries
      If the list of countries wasn't reachable
     */
    public static ArrayList<Country> getAll() throws Exception {
        ArrayList<Country> countries = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCountries(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Country country = new Country(
                        result.getInt("CountryId"),
                        result.getString("CountryName")
                );

                countries.add(country);
            }

            return countries;
        } catch (SQLException e) {
            throw new Exception("Unable to get countries", e);
        }
    }
}
