/*
 * The MIT License
 *
 * Copyright 2022 Daniel G.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PersonXCommittee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Daniel G
 */
public class PersonXCommitteeController {
    public ArrayList<HashMap<String, String>> getPersonXCommitteeRows() throws Exception {

        ArrayList<HashMap<String, String>> personXCommitteeRows = new ArrayList<>();

        ArrayList<PersonXCommittee> personXCommittees = PersonXCommittee.getAll();

        personXCommittees.forEach((PersonXCommittee personXCommittee) -> {
            personXCommitteeRows.add(personXCommittee.toMap());
        });

        return personXCommitteeRows;
    }

    public ArrayList<String> getPersonXCommitteeColumns() throws Exception {
        Set<String> columns = PersonXCommittee.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getPersonXCommitteeRow(Integer personId, Integer committeeId) throws Exception {
        PersonXCommittee personXCommittee = PersonXCommittee.find(personId, committeeId);

        return personXCommittee != null ? personXCommittee.toMap() : null;
    }

    public void createPersonXCommittee(Integer personId, Integer committeeId) throws Exception {
        PersonXCommittee personXCommittee = new PersonXCommittee(personId, committeeId);

        personXCommittee.save();
    }

    public boolean deletePersonXCommittee(Integer personId, Integer committeeId) throws Exception {
        PersonXCommittee personXCommittee = PersonXCommittee.find(personId, committeeId);

        personXCommittee.delete();

        return personXCommittee.getPersonId() == null && personXCommittee.getCommitteeId() == null;
    }

    public ArrayList<HashMap<String, String>> getPersonXCommitteeRows(Integer personId) throws Exception {

        ArrayList<HashMap<String, String>> personXCommitteeRows = new ArrayList<>();

        ArrayList<PersonXCommittee> personXCommittees = PersonXCommittee.getAll(personId);

        personXCommittees.forEach((PersonXCommittee personXCommittee) -> {
            personXCommitteeRows.add(personXCommittee.toMap());
        });

        return personXCommitteeRows;
    }
}
