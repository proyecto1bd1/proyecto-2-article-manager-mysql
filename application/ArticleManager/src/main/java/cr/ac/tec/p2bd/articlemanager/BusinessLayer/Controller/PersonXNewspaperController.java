/*
 * The MIT License
 *
 * Copyright 2022 Daniel G.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PersonXNewspaper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Daniel G
 */
public class PersonXNewspaperController {

    public PersonXNewspaperController() {
    }

    public ArrayList<HashMap<String, String>> getPersonXNewspaperRows() throws Exception {

        ArrayList<HashMap<String, String>> personXNewspaperRows = new ArrayList<>();

        ArrayList<PersonXNewspaper> personXNewspapers = PersonXNewspaper.getAll();

        personXNewspapers.forEach((PersonXNewspaper personXNewspaper) -> {
            personXNewspaperRows.add(personXNewspaper.toMap());
        });

        return personXNewspaperRows;
    }

    public ArrayList<HashMap<String, String>> getPersonNewspaperRows(Integer personId) throws Exception {
        ArrayList<HashMap<String, String>> personXNewspaperRows = new ArrayList<>();

        ArrayList<PersonXNewspaper> personXNewspapers = PersonXNewspaper.getAll(personId);

        personXNewspapers.forEach((PersonXNewspaper personXNewspaper) -> {
            personXNewspaperRows.add(personXNewspaper.toMap());
        });

        return personXNewspaperRows;
    }

    public ArrayList<String> getPersonXNewspaperColumns() throws Exception {
        Set<String> columns = PersonXNewspaper.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getPersonXNewspaperRow(Integer newspaperId, Integer personId) throws Exception {
        PersonXNewspaper personXNewspaper = PersonXNewspaper.find(newspaperId, personId);

        return personXNewspaper.toMap();
    }

    public void createPersonXNewspaper(Integer newspaperId, Integer personId) throws Exception {
        PersonXNewspaper personXNewspaper = new PersonXNewspaper(newspaperId, personId);

        personXNewspaper.save();
    }

    public boolean deletePersonXNewspaper(Integer newspaperId, Integer personId) throws Exception {
        PersonXNewspaper personXNewspaper = PersonXNewspaper.find(newspaperId, personId);

        personXNewspaper.delete();

        return personXNewspaper.getNewsPaperId() == null && personXNewspaper.getPersonId() == null;
    }
}
