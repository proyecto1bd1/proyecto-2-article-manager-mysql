/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class manageNewsPaper {

    private final UniversityController universityController = new UniversityController();
    private final CommitteeController committeeController = new CommitteeController();
    private final DigitalNewsPaperController newspaperController = new DigitalNewsPaperController();

    private final PersonXNewspaperController personXnewspaperController = new PersonXNewspaperController();

    private final PersonXCommitteeController personXCommitteeController = new PersonXCommitteeController();

    @FXML
    public TextField tfNewspaperName;

    @FXML
    public TextField tfCommitteeName;

    @FXML
    public ChoiceBox<Choice> cbCommittee;

    @FXML
    public ChoiceBox<Choice> cbUniversity;

    @FXML
    public ChoiceBox<Choice> cbNewspaper;

    @FXML
    public Button btnDeleteNewsPaper;

    @FXML
    public Button btnSaveNewsPaper;

    @FXML
    public Button btnDeleteCommittee;

    @FXML
    public Button btnSaveCommittee;

    @FXML
    public ChoiceBox<Choice> cbNewspaper2;

    @FXML
    public ChoiceBox<Choice> cbCommittee2;

    @FXML
    public ChoiceBox<Choice> cbNmembers;

    @FXML
    public ChoiceBox<Choice> cbCmembers;

    @FXML
    public ChoiceBox<Choice> cbNcandidates;

    @FXML
    public ChoiceBox<Choice> cbCcandidates;

    @FXML
    public Button btnNremove;

    @FXML
    public Button btnNAdd;

    @FXML
    public Button btnCremove;

    @FXML
    public Button btnCAdd;





    @FXML
    protected void initialize() throws IOException {
        ArrayList<Choice> newspaperOptions = ChoiceBuilder.getDigitalNewsPaperList();
        Choice newItem = new Choice(0, "Add new");
        newspaperOptions.add(0, newItem);
        cbNewspaper.getItems().addAll(newspaperOptions);
        cbNewspaper.setValue(newItem);
        ArrayList<Choice> committeeOptions = ChoiceBuilder.getCommitteeList();
        committeeOptions.add(0, newItem);
        cbCommittee.getItems().addAll(committeeOptions);
        cbCommittee.setValue(newItem);
        ArrayList<Choice> availableUniversities = ChoiceBuilder.getUniversityNoNewspaperList();
        cbUniversity.getItems().addAll(availableUniversities);
        cbNewspaper.setOnAction(this::onNewspaperSelected);
        cbCommittee.setOnAction(this::onCommitteeSelected);
        btnDeleteNewsPaper.setDisable(true);
        btnSaveNewsPaper.setDisable(true);
        btnDeleteCommittee.setDisable(true);
        cbUniversity.setOnAction(this::onUniversitySelected);

        cbNewspaper2.getItems().addAll(ChoiceBuilder.getDigitalNewsPaperList());
        cbCommittee2.getItems().addAll(ChoiceBuilder.getCommitteeList());
        cbNewspaper2.setOnAction(this::onNewspaperSelected2);
        cbCommittee2.setOnAction(this::onCommitteeSelected2);
        btnSaveCommittee.setDisable(true);
        btnSaveNewsPaper.setDisable(true);
        btnDeleteNewsPaper.setDisable(true);
        btnDeleteCommittee.setDisable(true);
        btnCremove.setDisable(true);
        btnNremove.setDisable(true);
        btnCAdd.setDisable(true);
        btnNAdd.setDisable(true);
        cbNmembers.setOnAction(this::onNmembersSelected);
        cbCmembers.setOnAction(this::onCmembersSelected);
        cbNcandidates.setOnAction(this::onNcandidatesSelected);
        cbCcandidates.setOnAction(this::onCcandidatesSelected);
    }


    private void onNmembersSelected(ActionEvent event){
        btnNremove.setDisable(false);
    }

    private void onCmembersSelected(ActionEvent event){
        btnCremove.setDisable(false);
    }

    private void onNcandidatesSelected(ActionEvent event){
        btnNAdd.setDisable(false);
    }

    private void onCcandidatesSelected(ActionEvent event){
        btnCAdd.setDisable(false);
    }

    private void onNewspaperSelected2(ActionEvent event){
        try{
            if (cbNewspaper2.getValue().id != null){
                cbNmembers.getItems().addAll(ChoiceBuilder.getDigitalNewsPaperMembers(cbNewspaper2.getValue().id));
                cbNcandidates.getItems().addAll(ChoiceBuilder.getNewspaperNonMembers());
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }

    private void onCommitteeSelected2(ActionEvent event){
        try{
            if (cbCommittee2.getValue().id != null){
                cbCmembers.getItems().addAll(ChoiceBuilder.getCommitteeMembers(cbCommittee2.getValue().id));
                cbCcandidates.getItems().addAll(ChoiceBuilder.getCommitteeNonMembers());
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }


    private void onUniversitySelected(ActionEvent event){
        Choice committeeSelected = cbCommittee.getValue();
        Choice newspaperSelected = cbNewspaper.getValue();
        if(committeeSelected != null && committeeSelected.id != 0 && newspaperSelected != null){
            btnSaveCommittee.setDisable(false);
        }
    }

    private void onCommitteeSelected(ActionEvent actionEvent) {
        Choice committeeSelected = cbCommittee.getValue();
        if(committeeSelected != null && committeeSelected.id != 0){
            btnDeleteCommittee.setDisable(false);
            try {
                HashMap<String, String> committee = committeeController.getCommitteeRow(committeeSelected.id);
                tfCommitteeName.setText(committee.get("description"));
                if(cbUniversity.getValue() != null){
                    btnSaveNewsPaper.setDisable(false);
                }
                if(cbNewspaper.getValue() != null && cbNewspaper.getValue().id != 0){
                    btnDeleteNewsPaper.setDisable(false);
                }
            }catch(Exception e){
                System.out.println(e);
            }
        }
        if(committeeSelected != null && committeeSelected.id == 0){
            tfCommitteeName.clear();
        }
    }

    protected void onNewspaperSelected(ActionEvent event){
        Choice newspaperSelected = cbNewspaper.getValue();
        cbUniversity.getItems().clear();
        cbCommittee.getItems().clear();
        if(newspaperSelected != null && newspaperSelected.id != 0){
            try{
                HashMap<String, String> newspaper = newspaperController.getDigitalNewsPaperRow(newspaperSelected.id);
                Integer universityId = Integer.valueOf(newspaper.get("UniversityId"));
                HashMap<String, String> university = universityController.getUniversityRow(universityId);
                ArrayList<Choice> availableUniversities = ChoiceBuilder.getUniversityNoNewspaperList();
                Choice currentUni = new Choice(universityId, university.get("universityName"));
                availableUniversities.add(0,currentUni);
                cbUniversity.getItems().addAll(availableUniversities);
                Integer committeeId = Integer.valueOf(newspaper.get("CommitteeId"));
                HashMap<String, String> committee = committeeController.getCommitteeRow(committeeId);
                Choice currentCommittee = new Choice(committeeId, committee.get("description"));
                cbCommittee.getItems().addAll(ChoiceBuilder.getCommitteeList());
                cbCommittee.setValue(currentCommittee);
                cbUniversity.setValue(currentUni);
                tfNewspaperName.setText(newspaper.get("NewsPaperName"));
                tfCommitteeName.setText(committee.get("description"));
                btnDeleteCommittee.setDisable(false);
                btnSaveNewsPaper.setDisable(false);
                btnDeleteNewsPaper.setDisable(false);

            }catch(Exception e){
                System.out.println(e);
            }
        }
        if(newspaperSelected != null && newspaperSelected.id == 0){
            btnDeleteNewsPaper.setDisable(true);
            btnSaveNewsPaper.setDisable(true);
            btnDeleteCommittee.setDisable(true);
            tfNewspaperName.clear();
            tfCommitteeName.clear();
            Choice newItem = new Choice(0, "Add new");
            ArrayList<Choice> committeeOptions = ChoiceBuilder.getCommitteeList();
            committeeOptions.add(0, newItem);
            cbCommittee.getItems().addAll(committeeOptions);
            cbCommittee.setValue(newItem);
            ArrayList<Choice> availableUniversities = ChoiceBuilder.getUniversityNoNewspaperList();
            cbUniversity.getItems().addAll(availableUniversities);
        }
    }
    public void onSaveCommittee(ActionEvent event){
        Choice committeeSelected = cbCommittee.getValue();
        if(committeeSelected.id != 0){
            try{
                committeeController.updateCommittee(committeeSelected.id, tfCommitteeName.getText());
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Committee successfully updated");
                //alert.setContentText(e.getMessage());
                alert.show();
                refreshCommittees();
            }catch(Exception e){
                System.out.println(e);
            }
        }
        else{
            try{
                committeeController.createCommittee(tfCommitteeName.getText());
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Committee successfully created");
                //alert.setContentText(e.getMessage());
                alert.show();
                refreshCommittees();
            }catch(Exception e){
                System.out.println(e);
            }
        }
    }

    public void onDeleteCommittee(ActionEvent event){
        Choice committeeSelected = cbCommittee.getValue();
        if(committeeSelected.id != 0){
            try{
                committeeController.deleteCommittee(committeeSelected.id);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Committee successfully deleted");
                //alert.setContentText(e.getMessage());
                alert.show();
                refreshCommittees();
            }catch(Exception e){
                System.out.println(e);
            }
        }
    }

    public void onSaveNewspaper(ActionEvent event){
        Choice newspaperSelected = cbNewspaper.getValue();
        Choice committeeSelected = cbCommittee.getValue();
        Choice universitySelected = cbUniversity.getValue();
        if(newspaperSelected.id != 0){
            try{
                newspaperController.updateDigitalNewsPaper(newspaperSelected.id, tfNewspaperName.getText(), committeeSelected.id, universitySelected.id);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Newspaper successfully updated");
                //alert.setContentText(e.getMessage());
                alert.show();
                refreshNewspaper();

            }catch(Exception e){
                System.out.println(e);
            }
        }
        else{
            try{
                newspaperController.createDigitalNewsPaper(tfNewspaperName.getText(), committeeSelected.id, universitySelected.id);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("Newspaper successfully created");
                //alert.setContentText(e.getMessage());
                alert.show();
                refreshNewspaper();
            }catch(Exception e){
                System.out.println(e);
            }

        }
    }

    public void onDeleteNewspaper(ActionEvent event){
        Choice newspaperSelected = cbNewspaper.getValue();
        try{
            newspaperController.deleteDigitalNewsPaper(newspaperSelected.id);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Newspaper successfully deleted");
            //alert.setContentText(e.getMessage());
            alert.show();
            refreshNewspaper();
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void refreshCommittees(){
        tfCommitteeName.clear();
        cbCommittee.getItems().clear();
        Choice newItem = new Choice(0, "Add new");
        ArrayList<Choice> committeeOptions = ChoiceBuilder.getCommitteeList();
        committeeOptions.add(0, newItem);
        cbCommittee.getItems().addAll(committeeOptions);
        cbCommittee.setValue(newItem);
        btnDeleteCommittee.setDisable(true);
    }

    public void refreshNewspaper(){
        ArrayList<Choice> newspaperOptions = ChoiceBuilder.getDigitalNewsPaperList();
        cbNewspaper.getItems().clear();
        Choice newItem = new Choice(0, "Add new");
        newspaperOptions.add(0, newItem);
        cbNewspaper.getItems().addAll(newspaperOptions);
        cbNewspaper.setValue(newItem);
        cbUniversity.getItems().clear();
        tfNewspaperName.clear();
        cbUniversity.getItems().clear();
        cbUniversity.getItems().addAll(ChoiceBuilder.getUniversityNoNewspaperList());
        refreshCommittees();
    }

    public void onAddNmember(ActionEvent event){
        try{
            Integer personId = cbNcandidates.getValue().id;
            Integer newspaperId = cbNewspaper2.getValue().id;
            personXnewspaperController.createPersonXNewspaper(newspaperId, personId);
            refreshNmembership();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Member successfully added to newspaper");
            //alert.setContentText(e.getMessage());
            alert.show();


        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void onRemoveNmember(ActionEvent event){
        try{
            Integer personId = cbNmembers.getValue().id;
            Integer newspaperId = cbNewspaper2.getValue().id;
            personXnewspaperController.deletePersonXNewspaper(newspaperId, personId);
            refreshNmembership();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Member successfully removed from newspaper");
            //alert.setContentText(e.getMessage());
            alert.show();


        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void onAddCmember(ActionEvent event){
        try{
            Integer personId = cbCcandidates.getValue().id;
            Integer committeeId = cbCommittee2.getValue().id;
            personXCommitteeController.createPersonXCommittee(personId, committeeId);
            refreshCmembership();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Member successfully added to newspaper");
            //alert.setContentText(e.getMessage());
            alert.show();
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void onRemoveCmember(ActionEvent event){
        try{
            Integer personId = cbCmembers.getValue().id;
            Integer committeeId = cbCommittee2.getValue().id;
            personXCommitteeController.deletePersonXCommittee(personId, committeeId);
            refreshCmembership();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Member successfully removed from newspaper");
            //alert.setContentText(e.getMessage());
            alert.show();
        }catch(Exception e){
            System.out.println(e);
        }
    }



    public void refreshNmembership(){
        cbNcandidates.getItems().clear();
        cbNmembers.getItems().clear();
        btnNAdd.setDisable(true);
        btnNremove.setDisable(true);
        cbNcandidates.getItems().addAll(ChoiceBuilder.getNewspaperNonMembers());
        cbNmembers.getItems().addAll(ChoiceBuilder.getDigitalNewsPaperMembers(cbNewspaper2.getValue().id));
    }

    public void refreshCmembership(){
        cbCcandidates.getItems().clear();
        cbCmembers.getItems().clear();
        btnCAdd.setDisable(true);
        btnCremove.setDisable(true);
        cbCcandidates.getItems().addAll(ChoiceBuilder.getCommitteeNonMembers());
        cbCmembers.getItems().addAll(ChoiceBuilder.getCommitteeMembers(cbCommittee2.getValue().id));
    }


}
