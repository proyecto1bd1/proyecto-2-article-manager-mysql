/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class NewsBubbleController {
    public Label lblNewsTitle;
    public Label lblContentSummary;
    public ImageView imgNews;
    public Button btnSeeMore;

    private int newsId = 0;

    public void fillNewsData(int newsId, String title, String content, String imageBase64) {
        this.newsId = newsId;
        lblNewsTitle.setText(title);
        lblContentSummary.setText(content);

        if (!imageBase64.isEmpty()) {
            byte[] imageStream = Base64.getDecoder().decode(imageBase64);
            InputStream imageData = new ByteArrayInputStream(imageStream);
            Image image = new Image(imageData);
            imgNews.setImage(image);

            try {
                File imageNews = File.createTempFile("news_%d".formatted(this.newsId), ".jpg");
                FileUtils.writeByteArrayToFile(imageNews, imageStream);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unexpected error");
                alert.setHeaderText("An error occurred while building the image");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
    }

    public void onSeeMore(ActionEvent event) throws IOException {
        SessionManager.getInstance().getHomeController().onNewsSelected(this.newsId);
    }
}
