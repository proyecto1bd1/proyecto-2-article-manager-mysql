/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Telephone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class TelephoneController {
    public ArrayList<HashMap<String, String>> getTelephoneRows() throws Exception {

        ArrayList<HashMap<String, String>> telephoneRows = new ArrayList<>();

        ArrayList<Telephone> telephones = Telephone.getAll();

        telephones.forEach((Telephone telephone) -> {
            telephoneRows.add(telephone.toMap());
        });

        return telephoneRows;
    }

    public ArrayList<String> getTelephoneColumns() throws Exception {
        Set<String> columns = Telephone.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getTelephoneRow(Integer telephoneId) throws Exception {
        Telephone telephone = Telephone.find(telephoneId);

        return telephone.toMap();
    }

    public Integer createTelephone(String telephoneName, Integer personId) throws Exception {
        Telephone telephone = new Telephone(telephoneName, personId);

        telephone.save();

        return telephone.getTelephoneId();
    }

    public String updateTelephone(Integer telephoneId, String telephoneName, Integer personId) throws Exception {
        Telephone telephone = Telephone.find(telephoneId);

        telephone.setTelephoneNumber(telephoneName);
        telephone.setPersonId(personId);
        telephone.save();

        return telephone.getTelephoneNumber();
    }

    public boolean deleteTelephone(Integer telephoneId) throws Exception {
        Telephone telephone = Telephone.find(telephoneId);

        telephone.delete();

        return telephone.getTelephoneId() == null;
    }
}
