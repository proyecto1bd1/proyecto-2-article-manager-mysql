/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DigitalNewsPaperController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.NewsArticleController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.NewsArticleTypeController;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class ArticleEditorController {
    public Button btnSubmit;
    private boolean editMode = false;
    private boolean reviewMode = false;
    private final NewsArticleController newsArticleController = new NewsArticleController();

    private final NewsArticleTypeController newsArticleTypeController = new NewsArticleTypeController();
    private final DigitalNewsPaperController digitalNewsPaperController = new DigitalNewsPaperController();
    public ImageView imgArticle;
    public TextField txtTitle;
    public TextArea txtContent;
    public ChoiceBox<Choice> cbArticleType;
    public Button btnApprove;
    private File articlePic;

    private HashMap<String, String> article;

    public void initialize() {
        cbArticleType.getItems().addAll(ChoiceBuilder.getNewsArticleTypeList());
        btnApprove.setVisible(false);
    }

    public void setEditMode(HashMap<String, String> article) {
        editMode = true;
        this.article = article;
        btnApprove.setVisible(false);

        loadArticleData();
    }

    public void setReviewMode(HashMap<String, String> article) {
        reviewMode = true;

        this.article = article;
        btnApprove.setVisible(true);
        btnSubmit.setVisible(false);
        cbArticleType.setDisable(true);
        txtTitle.setDisable(true);
        txtContent.setDisable(true);

        loadArticleData();
    }

    private void loadArticleData() {

        txtContent.setText(article.get("Content"));
        txtTitle.setText(article.get("Title"));

        try {
            HashMap<String, String> articleType = newsArticleTypeController.getNewsArticleRow(
                    Integer.valueOf(article.get("ArticleTypeId"))
            );

            if (articleType != null) {
                cbArticleType.setValue(
                        new Choice(
                                Integer.valueOf(articleType.get("TypeId")),
                                articleType.get("description")
                        )
                );
            }
        } catch (Exception e) {
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Failed to get article type");
            error.setHeaderText("We were unable get the article type");
            error.setContentText(e.getMessage());
            error.show();
        }

        try {
            if (!article.get("Photo").isEmpty()) {
                byte[] imageStream = Base64.getDecoder().decode(article.get("Photo"));
                InputStream imageData = new ByteArrayInputStream(imageStream);
                Image image = new Image(imageData);
                imgArticle.setImage(image);

                articlePic = File.createTempFile("profile", ".jpg");
                FileUtils.writeByteArrayToFile(articlePic, imageStream);
            }

        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to load image");
            alert.setHeaderText("We were unable to display the article image");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onSubmit(ActionEvent event) {
        try {
            if (editMode) {
                newsArticleController.updateNewsArticle(
                        Integer.valueOf(article.get("ArticleId")),
                        txtTitle.getText(),
                        txtContent.getText(),
                        article.get("PublicationDate"),
                        articlePic,
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        SessionManager.getInstance().getSelectedNewsPaper(),
                        article.get("CommitteeIdApprov").isEmpty() ? null : Integer.valueOf(article.get("CommitteeIdApprov"))
                );

                Alert information = new Alert(Alert.AlertType.INFORMATION);
                information.setTitle("Article submitted");
                information.setHeaderText("Your article was updated!");
                information.setContentText("Thanks for updating it");
                information.showAndWait();

                SessionManager.getInstance().getHomeController().onLoadLatestNews(event);
            } else if (reviewMode) {
                HashMap<String, String> digitalNewspaper = digitalNewsPaperController.getDigitalNewsPaperRow(
                        SessionManager.getInstance().getSelectedNewsPaper()
                );

                newsArticleController.approveArticle(
                        Integer.valueOf(article.get("ArticleId")),
                        Integer.valueOf(digitalNewspaper.get("CommitteeId"))
                );

                Alert information = new Alert(Alert.AlertType.INFORMATION);
                information.setTitle("Article approved successfully");
                information.setHeaderText("This article was approve");
                information.setContentText("You will be sent to the article approval section");
                information.showAndWait();
                SessionManager.getInstance().getHomeController().onOpenReview(event);
            } else {
                newsArticleController.createNewsArticle(
                        txtTitle.getText(),
                        txtContent.getText(),
                        LocalDate.now().toString(),
                        articlePic,
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        SessionManager.getInstance().getSelectedNewsPaper(),
                        null
                );

                Alert information = new Alert(Alert.AlertType.INFORMATION);
                information.setTitle("Article submitted");
                information.setHeaderText("Your article was uploaded!");
                information.setContentText("Now you just have to wait for get it approve!");
                information.showAndWait();

                SessionManager.getInstance().getHomeController().onLoadLatestNews(event);
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to save article");
            alert.setHeaderText("We were unable store the article data");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onImgClick(MouseEvent event) throws IOException {
        if (reviewMode) {
            return;
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Image");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg")
        );

        articlePic = fileChooser.showOpenDialog(imgArticle.getScene().getWindow());

        if (articlePic != null) {
            Image pic = new Image(articlePic.toString());
            imgArticle.setImage(pic);
        }
    }
}
