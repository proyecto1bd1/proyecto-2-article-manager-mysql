/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Canton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class CantonController {

    public CantonController() {
    }

    public ArrayList<HashMap<String, String>> getCantonRows(Integer provinceId) throws Exception {
        ArrayList<HashMap<String, String>> cantonRows = new ArrayList<>();

        ArrayList<Canton> cantons = Canton.getAll(provinceId);

        cantons.forEach((Canton canton) -> {
            cantonRows.add(canton.toMap());
        });

        return cantonRows;
    }

    public ArrayList<HashMap<String, String>> getCantonRows() throws Exception {
        ArrayList<HashMap<String, String>> cantonRows = new ArrayList<>();

        ArrayList<Canton> cantons = Canton.getAll();

        cantons.forEach((Canton canton) -> {
            cantonRows.add(canton.toMap());
        });

        return cantonRows;
    }

    public HashMap<String, String> getCantonRow(Integer cantonId) throws Exception {
        Canton canton = Canton.find(cantonId);

        return canton.toMap();
    }

    public Integer createCanton(String cantonName, Integer provinceId) throws Exception {
        Canton canton = new Canton(cantonName, provinceId);

        canton.save();

        return canton.getCantonId();
    }

    public String updateCanton(Integer cantonId, String cantonName, Integer provinceId) throws Exception {
        Canton canton = Canton.find(cantonId);

        canton.setCantonName(cantonName);
        canton.setProvinceId(provinceId);
        canton.save();

        return canton.getCantonName();
    }

    public boolean deleteCanton(Integer cantonId) throws Exception {
        Canton canton = Canton.find(cantonId);

        canton.delete();

        return canton.getCantonId() == null;
    }

    public ArrayList<String> getCantonColumns() {
        Set<String> columns = Canton.getColumns();

        return new ArrayList<>(columns);
    }
}
