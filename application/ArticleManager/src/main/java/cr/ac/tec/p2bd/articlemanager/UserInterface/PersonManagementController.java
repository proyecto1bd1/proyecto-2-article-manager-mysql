/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.AdminController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.AuthorController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ParameterController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonController;
import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Parameter;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class PersonManagementController {

    private final PersonController personController = new PersonController();
    private final AdminController adminController = new AdminController();
    private final AuthorController authorController = new AuthorController();
    private final ParameterController parameterController = new ParameterController();

    public void onCreatePerson(ActionEvent event) throws IOException {
        SessionManager.getInstance().getHomeController().onCreatePerson();
    }

    public void onEditPerson(ActionEvent event) throws IOException {
        HashMap<String, String> person = createPersonPicker(event);

        if (person != null) {
            SessionManager.getInstance().getHomeController().onEditPerson(person);
        }
    }

    public void onDeletePerson(ActionEvent event) throws IOException {
        HashMap<String, String> person = createPersonPicker(event);

        if (person == null) {
            return;
        }

        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle("Are you sure?");
        confirmation.setHeaderText("The deletion of a person is irreversible");
        confirmation.setContentText("Are you sure you want to delete?");

        if (confirmation.showAndWait().get() == ButtonType.CANCEL) {
            return;
        }

        try {
            personController.deletePerson(Integer.valueOf(person.get("PersonId")));

            Alert information = new Alert(Alert.AlertType.INFORMATION);
            information.setTitle("Deletion complete");
            information.setHeaderText("We successfully deleted the person");
            information.setContentText("It cannot longer be used by the system");
            information.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to delete person");
            alert.setHeaderText("Failed to delete person");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    private HashMap<String, String> createPersonPicker(ActionEvent event) throws IOException {
        Stage modalDialog = new Stage();
        FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("person-picker-view.fxml"));
        Parent dialogRoot = dialogLoader.load();
        PersonPickerController personPickerController = dialogLoader.getController();
        Scene dialogScene = new Scene(dialogRoot,400, 140);
        modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
        modalDialog.setScene(dialogScene);
        modalDialog.setResizable(false);
        modalDialog.showAndWait();

        return personPickerController.getPerson();
    }

    public void onPromoteAdmin(ActionEvent event) throws IOException {
        HashMap<String, String> person = createPersonPicker(event);

        if (person == null) {
            return;
        }

        try {
            adminController.createAdmin(Integer.valueOf(person.get("PersonId")));

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Promotion successful");
            alert.setHeaderText("This user is now part of admin");
            alert.setContentText("The new options will show up in the next log in");
            alert.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to promote");
            alert.setHeaderText("We couldn't promote the person");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onPromoteAuthor(ActionEvent event) throws IOException {
        HashMap<String, String> person = createPersonPicker(event);

        if (person == null) {
            return;
        }

        try {
            Integer authorType = Integer.valueOf(parameterController.getParameterRow(3).get("Value"));

            authorController.createAuthor(Integer.valueOf(person.get("PersonId")), authorType);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Promotion successful");
            alert.setHeaderText("This user is now part of author");
            alert.setContentText("The new options will show up in the next log in");
            alert.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to promote");
            alert.setHeaderText("We couldn't promote the person");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onDemoteAdmin(ActionEvent event) throws IOException {
        try {
            ArrayList<HashMap<String, String>> admins = adminController.getAdminRows();

            ArrayList<Choice> choices = new ArrayList<>();

            admins.forEach(admin -> {
                try {
                    HashMap<String, String> person = personController.getPersonRow(Integer.valueOf(admin.get("personId")));

                    choices.add(
                            new Choice(
                                    Integer.valueOf(person.get("PersonId")),
                                    "%s %s %s".formatted(
                                            person.get("Name"),
                                            person.get("FirstLastName"),
                                            person.get("SecondLastName")
                                    )
                            )
                    );
                } catch (Exception e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Failed to fetch person");
                    alert.setHeaderText("Failed to get person data");
                    alert.setContentText(e.getMessage());
                    alert.show();
                }
            });

            Stage modalDialog = new Stage();
            FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("person-picker-view.fxml"));
            Parent dialogRoot = dialogLoader.load();
            PersonPickerController personPickerController = dialogLoader.getController();
            personPickerController.setChoiceList(choices);
            Scene dialogScene = new Scene(dialogRoot,400, 140);
            modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
            modalDialog.setScene(dialogScene);
            modalDialog.setResizable(false);
            modalDialog.showAndWait();

            HashMap<String, String> person = personPickerController.getPerson();

            if (person == null) {
                return;
            }

            adminController.deleteAdmin(Integer.valueOf(person.get("PersonId")));

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Demotion successful");
            alert.setHeaderText("This user is no longer part of admin");
            alert.setContentText("The options will show up in the next log in");
            alert.show();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to demote");
            alert.setHeaderText("We couldn't demote the person");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onDemoteAuthor(ActionEvent event) throws IOException {
        try {
            ArrayList<HashMap<String, String>> authors = authorController.getAuthorRows();

            ArrayList<Choice> choices = new ArrayList<>();

            authors.forEach(author -> {
                try {
                    HashMap<String, String> person = personController.getPersonRow(Integer.valueOf(author.get("AuthorId")));

                    choices.add(
                            new Choice(
                                    Integer.valueOf(person.get("PersonId")),
                                    "%s %s %s".formatted(
                                            person.get("Name"),
                                            person.get("FirstLastName"),
                                            person.get("SecondLastName")
                                    )
                            )
                    );
                } catch (Exception e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Failed to fetch person");
                    alert.setHeaderText("Failed to get person data");
                    alert.setContentText(e.getMessage());
                    alert.show();
                }
            });

            Stage modalDialog = new Stage();
            FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("person-picker-view.fxml"));
            Parent dialogRoot = dialogLoader.load();
            PersonPickerController personPickerController = dialogLoader.getController();
            personPickerController.setChoiceList(choices);
            Scene dialogScene = new Scene(dialogRoot,400, 140);
            modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
            modalDialog.setScene(dialogScene);
            modalDialog.setResizable(false);
            modalDialog.showAndWait();

            HashMap<String, String> person = personPickerController.getPerson();

            if (person == null) {
                return;
            }

            authorController.deleteAuthor(Integer.valueOf(person.get("PersonId")));

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Demotion successful");
            alert.setHeaderText("This user is no longer part of authors");
            alert.setContentText("The options will show up in the next log in");
            alert.show();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to demote");
            alert.setHeaderText("We couldn't demote the person");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }
}
