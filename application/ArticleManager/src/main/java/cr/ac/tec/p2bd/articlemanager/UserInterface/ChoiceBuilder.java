package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.*;
import javafx.scene.control.Alert;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class ChoiceBuilder {

    private static final CountryController countryController = new CountryController();
    private static final ProvinceController provinceController = new ProvinceController();
    private static final CantonController cantonController = new CantonController();
    private static final DistrictController districtController = new DistrictController();

    private static final GenderController genderController = new GenderController();
    private static final IdTypeController idTypeController = new IdTypeController();
    private static final CommitteeController committeeController = new CommitteeController();
    private static final AuthorTypeController authorTypeController = new AuthorTypeController();

    private static final PersonController personController = new PersonController();
    private static final NewsArticleTypeController newsArticleTypeController = new NewsArticleTypeController();
    private static final DigitalNewsPaperController digitalNewsPaperController = new DigitalNewsPaperController();

    private static final UniversityController universityController = new UniversityController();

    private static final cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.StoreController storeController = new cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.StoreController();

    private static final ProductController productController = new ProductController();

    private static @NotNull ArrayList<Choice> buildGenericList(
            @NotNull ArrayList<HashMap<String, String>> entities,
            String primaryKey,
            String showName) {
        ArrayList<Choice> choices = new ArrayList<>();


        entities.forEach(entity -> {
            choices.add(
                    new Choice(
                            Integer.valueOf(entity.get(primaryKey)),
                            entity.get(showName)
                    )
            );
        });

        return choices;
    }

    public static @NotNull ArrayList<Choice> getCountryList() {
        try {
            ArrayList<HashMap<String, String>> countries = countryController.getCountryRows();

            return buildGenericList(
                    countries,
                    "countryId",
                    "countryName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch countries");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getProvinceList(Integer countryId) {
        try {
            ArrayList<HashMap<String, String>> provinces = provinceController.getProvinceRows(countryId);

            return buildGenericList(
                    provinces,
                    "provinceId",
                    "provinceName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch provinces");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getCantonList(Integer provinceId) {
        try {
            ArrayList<HashMap<String, String>> cantons = cantonController.getCantonRows(provinceId);

            return buildGenericList(
                    cantons,
                    "cantonId",
                    "cantonName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch cantons");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getDistrictList(Integer cantonId) {
        try {
            ArrayList<HashMap<String, String>> districts = districtController.getDistrictRows(cantonId);

            return buildGenericList(
                    districts,
                    "districtId",
                    "districtName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch provinces");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getGenderList() {
        try {
            ArrayList<HashMap<String, String>> genders = genderController.getGenderRows();

            return buildGenericList(
                    genders,
                    "genderId",
                    "genderName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch genders");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getIdTypeList() {
        try {
            ArrayList<HashMap<String, String>> idTypes = idTypeController.getIdTypeRows();

            return buildGenericList(
                    idTypes,
                    "typeId",
                    "description"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch id types");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getCommitteeList() {
        try {
            ArrayList<HashMap<String, String>> committees = committeeController.getCommitteeRows();

            return buildGenericList(
                    committees,
                    "committeeId",
                    "description"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch committees");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getUniversityList() {
        try {
            ArrayList<HashMap<String, String>> universities = universityController.getUniversityRows();

            return buildGenericList(
                    universities,
                    "universityId",
                    "universityName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch universities");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getUniversityNoNewspaperList() {
        try {
            ArrayList<HashMap<String, String>> universities = universityController.getUniversityNoNewspaper();
            return buildGenericList(
                    universities,
                    "universityId",
                    "universityName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch universities");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }



    public static @NotNull ArrayList<Choice> getNewsArticleTypeList() {
        try {
            ArrayList<HashMap<String, String>> newsArticleTypes = newsArticleTypeController.getNewsArticleTypeRows();

            return buildGenericList(
                    newsArticleTypes,
                    "TypeId",
                    "description"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch news article types");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getDigitalNewsPaperList() {
        try {
            ArrayList<HashMap<String, String>> digitalNewsPapers = digitalNewsPaperController.getDigitalNewsPaperRows();

            return buildGenericList(
                    digitalNewsPapers,
                    "NewsPaperId",
                    "NewsPaperName"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch digital news papers");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getDigitalNewsPaperMembers(Integer selectedNewspaper) {
        try {
            ArrayList<HashMap<String, String>> members = digitalNewsPaperController.getNewspaperMembers(selectedNewspaper);

            ArrayList<Choice> memberChoices = new ArrayList<>();

            members.forEach(person -> {
                memberChoices.add(
                        new Choice(
                                Integer.valueOf(person.get("PersonId")),
                                "%s %s %s".formatted(
                                        person.get("Name"),
                                        person.get("FirstLastName"),
                                        person.get("SecondLastName")
                                )
                        )
                );
            });

            memberChoices.sort(Comparator.comparing(Choice::toString));

            return memberChoices;

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unable to load members");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getCommitteeMembers(Integer selectedCommittee) {
        try {
            ArrayList<HashMap<String, String>> members = committeeController.getCommitteeMembers(selectedCommittee);

            ArrayList<Choice> memberChoices = new ArrayList<>();

            members.forEach(person -> {
                memberChoices.add(
                        new Choice(
                                Integer.valueOf(person.get("PersonId")),
                                "%s %s %s".formatted(
                                        person.get("Name"),
                                        person.get("FirstLastName"),
                                        person.get("SecondLastName")
                                )
                        )
                );
            });

            memberChoices.sort(Comparator.comparing(Choice::toString));

            return memberChoices;

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unable to load members");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getCommitteeNonMembers() {
        try {
            ArrayList<HashMap<String, String>> members = personController.getPersonNoCommittee();

            ArrayList<Choice> memberChoices = new ArrayList<>();

            members.forEach(person -> {
                memberChoices.add(
                        new Choice(
                                Integer.valueOf(person.get("PersonId")),
                                "%s %s %s".formatted(
                                        person.get("Name"),
                                        person.get("FirstLastName"),
                                        person.get("SecondLastName")
                                )
                        )
                );
            });

            memberChoices.sort(Comparator.comparing(Choice::toString));

            return memberChoices;

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unable to load committee non members");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getNewspaperNonMembers() {
        try {
            ArrayList<HashMap<String, String>> members = personController.getPersonNoNewspaper();

            ArrayList<Choice> memberChoices = new ArrayList<>();

            members.forEach(person -> {
                memberChoices.add(
                        new Choice(
                                Integer.valueOf(person.get("PersonId")),
                                "%s %s %s".formatted(
                                        person.get("Name"),
                                        person.get("FirstLastName"),
                                        person.get("SecondLastName")
                                )
                        )
                );
            });

            memberChoices.sort(Comparator.comparing(Choice::toString));

            return memberChoices;

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unable to load newspaper non-members");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }



    public static @NotNull ArrayList<Choice> getAuthorTypesList() {
        try {
            ArrayList<HashMap<String, String>> authorTypes = authorTypeController.getAuthorTypeRows();

            return buildGenericList(
                    authorTypes,
                    "AuthorTypeId",
                    "Description"
            );

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch author types");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static @NotNull ArrayList<Choice> getPersonList() {
        try {
            ArrayList<HashMap<String, String>> persons = personController.getPersonRows();

            ArrayList<Choice> personChoices = new ArrayList<>();

            persons.forEach(person -> {
                personChoices.add(
                        new Choice(
                                Integer.valueOf(person.get("PersonId")),
                                "%s %s %s".formatted(
                                        person.get("Name"),
                                        person.get("FirstLastName"),
                                        person.get("SecondLastName")
                                )
                        )
                );
            });

            personChoices.sort(Comparator.comparing(Choice::toString));

            return personChoices;

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch person types");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static ArrayList<Choice> getStoreList() {
        try {
            ArrayList<HashMap<String, String>> stores = storeController.getStoreRows();

            ArrayList<Choice> storeChoices = new ArrayList<>();

            stores.forEach(store -> {
                storeChoices.add(
                        new Choice(
                                Integer.valueOf(store.get("storeId")),
                                store.get("description")
                        )
                );
            });

            return storeChoices;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch stores");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }

    public static ArrayList<Choice> getProductList() {
        try {
            ArrayList<HashMap<String, String >> products = productController.getProductRows();

            ArrayList<Choice> productChoices = new ArrayList<>();

            products.forEach(product -> {
                productChoices.add(
                        new Choice(
                                Integer.valueOf(product.get("productId")),
                                product.get("Name")
                        )
                );
            });

            return productChoices;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch products");
            alert.setContentText(e.getMessage());
            alert.show();
            return new ArrayList<>();
        }
    }
}
