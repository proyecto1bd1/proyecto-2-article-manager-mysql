/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PxFavoriteXArticle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class PxFavoriteXArticleController {

    public ArrayList<HashMap<String, String>> getPxFavoriteXArticleRows() throws Exception {
        ArrayList<HashMap<String, String>> PxFavoriteXArticleRows = new ArrayList<>();

        ArrayList<PxFavoriteXArticle> favorite = PxFavoriteXArticle.getAll();

        favorite.forEach((PxFavoriteXArticle pxfavoritexarticle) -> {
            PxFavoriteXArticleRows.add(pxfavoritexarticle.toMap());
        });

        return PxFavoriteXArticleRows;

    }

    public ArrayList<String> getPxFavoriteXArticleColumns() throws Exception {
        Set<String> pXFavoriteXArticle = PxFavoriteXArticle.getColumns();

        return new ArrayList<>(pXFavoriteXArticle);
    }

    public HashMap<String, String> getPxFavoriteXArticleRow(Integer personId, Integer articleId) throws Exception {
        PxFavoriteXArticle pxfavoritexarticle = PxFavoriteXArticle.find(personId, articleId);

        return pxfavoritexarticle != null ? pxfavoritexarticle.toMap() : null;
    }

    public Integer createPxFavoriteXArticle(Integer personId, Integer articleId) throws Exception {
        PxFavoriteXArticle pxfavoritexarticle = new PxFavoriteXArticle(personId, articleId, 0);

        pxfavoritexarticle.save();

        return pxfavoritexarticle.getPersonId();
    }

    public Integer updatePxFavoriteXArticle(Integer personId, Integer articleId) throws Exception {
        PxFavoriteXArticle pxfavoritexarticle = PxFavoriteXArticle.find(personId, articleId);

        pxfavoritexarticle.setDeleted(0);
        pxfavoritexarticle.save();

        return pxfavoritexarticle.getPersonId();
    }

    public boolean deletePxFavoriteXArticle(Integer personId, Integer articleId) throws Exception {
        PxFavoriteXArticle pxfavoritexarticle = PxFavoriteXArticle.find(personId, articleId);

        pxfavoritexarticle.delete();

        return pxfavoritexarticle.getPersonId() == null;
    }
}
