/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class District extends AbstractModel {

    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("districtId");
        columns.add("districtName");
        columns.add("cantonId");

        return columns;
    }

    private Integer districtId;
    private String districtName;
    private Integer cantonId;

    public District() {
    }

    private District(Integer districtId, String districtName, Integer cantonId) {
        this.districtId = districtId;
        this.districtName = districtName;
        this.cantonId = cantonId;
    }

    public District(String districtName, Integer cantonId) {
        this.districtName = districtName;
        this.cantonId = cantonId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setCantonId(Integer cantonId) {
        this.cantonId = cantonId;
    }

    public Integer getCantonId() {
        return cantonId;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (districtId != null) {
                CallableStatement sql = connection.prepareCall("{call updateDistrict(?, ?, ?)}");
                sql.setInt(1, districtId);
                sql.setString(2, districtName);
                sql.setInt(3, cantonId);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createDistrict(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, districtName);
                sql.setInt(3, cantonId);
                sql.execute();
                districtId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving district", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (districtId == null)
            return;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteDistrict(?)}");
            sql.setInt(1, districtId);
            sql.executeQuery();

            districtId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting district", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "District{ Id: %d Name: %s CantonId: %d }",
                districtId,
                districtName,
                cantonId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> districtMap = new HashMap<>();

        districtMap.put("districtId", districtId.toString());
        districtMap.put("districtName", districtName);
        districtMap.put("cantonId", cantonId.toString());

        return districtMap;
    }

    private static @NotNull ArrayList<District> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<District> districts = new ArrayList<>();

        while (result.next()) {
            District district = new District(
                    result.getInt("DistrictId"),
                    result.getString("DistrictName"),
                    result.getInt("CantonId")
            );

            districts.add(district);
        }

        return districts;
    }

    public static District find(Integer id) throws Exception {
        District district = null;
        ArrayList<District> districts;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getDistricts(?, ?)}");
            sql.setInt(1, id);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            districts = District.buildList(result);

            if (!districts.isEmpty()) {
                district = districts.get(0);
            }

            return district;
        } catch (SQLException e) {
            throw new Exception("Error fetching districts", e);
        }
    }

    public static @NotNull ArrayList<District> getAll() throws Exception {
        ArrayList<District> districts;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getDistricts(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            districts = District.buildList(result);
            return districts;
        } catch (SQLException e) {
            throw new Exception("Error fetching districts", e);
        }
    }

    public static @NotNull ArrayList<District> getAll(Integer provinceId) throws Exception {
        ArrayList<District> districts;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getDistricts(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setInt(2, provinceId);

            ResultSet result = sql.executeQuery();

            districts = District.buildList(result);
            return districts;
        } catch (SQLException e) {
            throw new Exception("Error fetching districts");
        }
    }
}
