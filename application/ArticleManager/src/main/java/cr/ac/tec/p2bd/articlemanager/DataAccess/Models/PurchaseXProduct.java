/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class PurchaseXProduct extends AbstractModel {
    private Integer PurchaseId;
    private Integer ProductId;

    public PurchaseXProduct() {
    }

    public PurchaseXProduct(Integer PurchaseId, Integer ProductId) {
        this.PurchaseId = PurchaseId;
        this.ProductId = ProductId;
    }

    public void setPurchaseId(Integer PurchaseId) {
        this.PurchaseId = PurchaseId;
    }

    public void setProductId(Integer ProductId) {
        this.ProductId = ProductId;
    }

    public Integer getPurchaseId() {
        return PurchaseId;
    }

    public Integer getProductId() {
        return ProductId;
    }

    @Override
    public String toString() {
        return String.format(
                "PurchaseXProduct{ PurchaseId: %d ProductId: %d }",
                PurchaseId,
                ProductId
        );
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            CallableStatement sql;
            assert connection != null;
            sql = connection.prepareCall("{call createpurchaseXproduct(?, ?)}");
            sql.setInt(1, this.PurchaseId);
            sql.setInt(2, this.ProductId);
            sql.executeQuery();

        } catch (SQLException e) {
            throw new Exception("Error: Unable to save relationship between purchase and product", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.PurchaseId == null & this.ProductId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletepurchaseXproduct(?, ?)}");
            sql.setInt(1, this.PurchaseId);
            sql.setInt(2, this.ProductId);
            sql.executeQuery();

            this.PurchaseId = null;
            this.ProductId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete relationship between purchase and product", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> purchaseXProductMap = new HashMap<>();

        purchaseXProductMap.put("purchaseId", PurchaseId.toString());
        purchaseXProductMap.put("productId", ProductId.toString());

        return purchaseXProductMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("purchaseId");
        columns.add("productId");

        return columns;
    }

    public static PurchaseXProduct find(Integer purchaseId, Integer productId) throws Exception {
        PurchaseXProduct purchaseXproduct = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpurchaseXproduct(?, ?)}");
            sql.setInt(1, purchaseId);
            sql.setInt(2, productId);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                purchaseXproduct = new PurchaseXProduct(
                        result.getInt("purchaseId"),
                        result.getInt("productId")
                );
            }
            return purchaseXproduct;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between purchase and product", e);
        }
    }

    public static ArrayList<PurchaseXProduct> getAll() throws Exception {
        ArrayList<PurchaseXProduct> purchaseXproducts = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpurchaseXproduct(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                PurchaseXProduct purchaseXproduct = new PurchaseXProduct(
                        result.getInt("purchaseId"),
                        result.getInt("productId")
                );
                purchaseXproducts.add(purchaseXproduct);
            }

            return purchaseXproducts;
        } catch (SQLException e) {
            throw new Exception("Unable to get relationship between purchase and product", e);
        }
    }
}
