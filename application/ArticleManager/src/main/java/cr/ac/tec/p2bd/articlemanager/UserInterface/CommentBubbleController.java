/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonController;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

import java.util.HashMap;

public class CommentBubbleController {

    private final PersonController personController = new PersonController();
    public Label lblCreatedAt;
    public Label lblUsername;
    public Label lblComment;
    HashMap<String, String> comment;

    public void setComment(HashMap<String, String> comment) {
        this.comment = comment;

        lblCreatedAt.setText(this.comment.get("createdAt"));
        lblComment.setText(this.comment.get("comments"));
        try {
            HashMap<String, String> person = personController.getPersonRow(
                    Integer.valueOf(this.comment.get("personId"))
            );
            lblUsername.setText(person.get("Username"));
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch comment data");
            alert.setHeaderText("We where unable to get comment data");
            alert.setContentText(e.getMessage());
            alert.show();
        }

    }
}
