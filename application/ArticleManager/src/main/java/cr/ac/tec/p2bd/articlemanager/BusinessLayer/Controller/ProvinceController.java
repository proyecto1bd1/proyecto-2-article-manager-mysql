/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Province;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class ProvinceController {

    public ProvinceController() {
    }

    public ArrayList<HashMap<String, String>> getProvinceRows(Integer countryId) throws Exception {
        ArrayList<HashMap<String, String>> provinceRows = new ArrayList<>();

        ArrayList<Province> provinces = Province.getAll(countryId);

        provinces.forEach((Province province) -> {
            provinceRows.add(province.toMap());
        });

        return provinceRows;
    }

    public ArrayList<HashMap<String, String>> getProvinceRows() throws Exception {
        ArrayList<HashMap<String, String>> provinceRows = new ArrayList<>();

        ArrayList<Province> provinces = Province.getAll();

        provinces.forEach((Province province) -> {
            provinceRows.add(province.toMap());
        });

        return provinceRows;
    }

    public HashMap<String, String> getProvinceRow(Integer provinceId) throws Exception {
        Province province = Province.find(provinceId);

        return province.toMap();
    }

    public Integer createProvince(String provinceName, Integer countryId) throws Exception {
        Province province = new Province(provinceName, countryId);

        province.save();

        return province.getProvinceId();
    }

    public String updateProvince(Integer provinceId, String provinceName, Integer countryId) throws Exception {
        Province province = Province.find(provinceId);

        province.setProvinceName(provinceName);
        province.setCountryId(countryId);

        province.save();

        return province.getProvinceName();
    }

    public boolean deleteProvince(Integer provinceId) throws Exception {
        Province province = Province.find(provinceId);

        province.delete();

        return province.getProvinceId() == null;
    }

    public ArrayList<String> getProvinceColumns() throws Exception {
        Set<String> columns = Province.getColumns();

        return new ArrayList<>(columns);
    }
}
