/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProductController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

public class ProductsManagementController {
    private final ProductController productController = new ProductController();

    public void onProductCreate(ActionEvent event) throws IOException {
        SessionManager.getInstance().getHomeController().onCreateProduct();
    }

    public void onProductEdit(ActionEvent event) throws IOException {
        HashMap<String, String> product = createProductPicker(event);

        if (product != null) {
            SessionManager.getInstance().getHomeController().onEditProduct(product);
        }
    }

    public void onProductDelete(ActionEvent event) throws IOException {
        HashMap<String, String> product = createProductPicker(event);

        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle("Are you sure?");
        confirmation.setHeaderText("The deletion of a person is irreversible");
        confirmation.setContentText("Are you sure you want to delete?");

        if (confirmation.showAndWait().get() == ButtonType.CANCEL) {
            return;
        }

        try {
            productController.deleteProduct(Integer.valueOf(product.get("productId")));

            Alert information = new Alert(Alert.AlertType.INFORMATION);
            information.setTitle("Deletion complete");
            information.setHeaderText("We successfully deleted the product");
            information.setContentText("It cannot longer be used by the system");
            information.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to delete product");
            alert.setHeaderText("Failed to delete product");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    private HashMap<String, String> createProductPicker(ActionEvent event) throws IOException {
        Stage modalDialog = new Stage();
        FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("product-picker-view.fxml"));
        Parent dialogRoot = dialogLoader.load();
        ProductPickerController productPickerController = dialogLoader.getController();
        Scene dialogScene = new Scene(dialogRoot,355, 150);
        modalDialog.initOwner(((Node)event.getSource()).getScene().getWindow());
        modalDialog.setScene(dialogScene);
        modalDialog.setResizable(false);
        modalDialog.showAndWait();

        return productPickerController.getProduct();
    }
}
