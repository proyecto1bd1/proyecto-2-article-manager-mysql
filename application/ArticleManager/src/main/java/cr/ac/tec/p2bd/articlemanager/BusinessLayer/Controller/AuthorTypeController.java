/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.AuthorType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class AuthorTypeController {

    /**
     * Gets a list of all authorTypes
     *
     * @return All the entries in authorTypes as rows
     */
    public ArrayList<HashMap<String, String>> getAuthorTypeRows() throws Exception {

        ArrayList<HashMap<String, String>> authorTypeRows = new ArrayList<>();

        ArrayList<AuthorType> authorTypes = AuthorType.getAll();

        authorTypes.forEach((AuthorType authorType) -> {
            authorTypeRows.add(authorType.toMap());
        });

        return authorTypeRows;
    }

    /**
     * Get the columns of the authorType model
     *
     * @return A list of columns
     */
    public ArrayList<String> getAuthorTypeColumns() throws Exception {
        Set<String> columns = AuthorType.getColumns();

        return new ArrayList<>(columns);
    }

    /**
     * Get a authorType as a row
     *
     * @param authorTypeId The authorType to locate
     * @return The row representing the authorType
     */
    public HashMap<String, String> getAuthorTypeRow(Integer authorTypeId) throws Exception {
        AuthorType authorType = AuthorType.find(authorTypeId);

        return authorType != null ? authorType.toMap() : null;
    }

    /**
     * Stores a new authorType
     *
     * @param description The name of the authorType
     * @return The id of the created element
     */
    public Integer createAuthorType(String description) throws Exception {
        AuthorType authorType = new AuthorType(description);

        authorType.save();

        return authorType.getAuthorTypeId();
    }

    /**
     * Updates the value of a authorType
     *
     * @param authorTypeId The id of the authorType to update
     * @param description  The new name of the authorType
     * @return The new name of the authorType
     */
    public String updateAuthorType(Integer authorTypeId, String description) throws Exception {
        AuthorType authorType = AuthorType.find(authorTypeId);

        authorType.setDescription(description);
        authorType.save();

        return authorType.getDescription();
    }

    /**
     * Deletes a authorType from the system
     *
     * @param authorTypeId The id of the author type to delete
     * @return True if authorType was deleted, false otherwise
     * Could fail if authorType has references attach
     */
    public boolean deleteAuthorType(Integer authorTypeId) throws Exception {
        AuthorType authorType = AuthorType.find(authorTypeId);

        authorType.delete();

        return authorType.getAuthorTypeId() == null;
    }
}
