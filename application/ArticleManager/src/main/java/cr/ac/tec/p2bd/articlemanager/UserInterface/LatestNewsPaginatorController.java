/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.NewsArticleController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Pagination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.HashMap;

public class LatestNewsPaginatorController {
    public Pagination newsPaginator;
    private final NewsArticleController newsArticleController = new NewsArticleController();

    private Integer newsPages = 0;

    @FXML
    public void initialize() {
        try {
            newsPages = newsArticleController.latestNewsPages(
                    SessionManager.getInstance().getSelectedNewsPaper()
            );
            newsPaginator.setPageCount(newsPages);

            newsPaginator.setPageFactory(pageIndex -> {
                if (pageIndex <= newsPages) {
                    return onPageChanged(pageIndex);
                } else {
                    return null;
                }
            });

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public VBox onPageChanged(int pageIndex) {
        try {
            VBox box = new VBox();

            ArrayList<HashMap<String, String>> newsRows = newsArticleController.getNewsLatestArticleRows(
                    pageIndex,
                    SessionManager.getInstance().getSelectedNewsPaper()
            );

            for (HashMap<String, String> newsRow : newsRows) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("news-bubble-view.fxml"));
                AnchorPane newsBubble = loader.load();
                NewsBubbleController newsBubbleController = loader.getController();
                newsBubbleController.fillNewsData(
                        Integer.parseInt(newsRow.get("ArticleId")),
                        newsRow.get("Title"),
                        newsRow.get("Content"),
                        newsRow.get("Photo")
                );
                box.getChildren().add(newsBubble);
            }

            return box;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
            return null;
        }
    }
}
