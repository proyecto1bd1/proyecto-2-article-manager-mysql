/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class Province extends AbstractModel {

    private Integer provinceId;
    private String provinceName;
    private Integer countryId;

    public Province() {
    }

    public Province(String provinceName, Integer countryId) {
        this.provinceName = provinceName;
        this.countryId = countryId;
    }

    private Province(Integer provinceId, String provinceName, Integer countryId) {
        this.provinceId = provinceId;
        this.provinceName = provinceName;
        this.countryId = countryId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public Integer getCountryId() {
        return countryId;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.provinceId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateProvince(?, ?, ?)}");
                sql.setInt(1, this.provinceId);
                sql.setString(2, this.provinceName);
                sql.setInt(3, this.countryId);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createProvince(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.provinceName);
                sql.setInt(3, this.countryId);
                sql.execute();

                this.provinceId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving province", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Province{ Id: %d Name: %s CountryId: %d }",
                this.provinceId,
                this.provinceName,
                this.countryId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> provinceMap = new HashMap<>();

        provinceMap.put("provinceId", this.provinceId.toString());
        provinceMap.put("provinceName", this.provinceName);
        provinceMap.put("countryId", this.countryId.toString());

        return provinceMap;
    }

    @Override
    public void delete() throws Exception {
        // If id is not set, do nothing
        if (this.provinceId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteProvince(?)}");
            sql.setInt(1, this.provinceId);
            sql.executeQuery();

            this.provinceId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting country", e);
        }
    }

    private static @NotNull ArrayList<Province> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Province> provinces = new ArrayList<>();

        while (result.next()) {
            Province province = new Province(
                    result.getInt("ProvinceId"),
                    result.getString("ProvinceName"),
                    result.getInt("CountryId")
            );

            provinces.add(province);
        }

        return provinces;
    }

    public static Province find(Integer id) throws Exception {
        Province province = null;
        ArrayList<Province> provinces;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getProvinces(?, ?)}");
            sql.setInt(1, id);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            provinces = Province.buildList(result);

            if (!provinces.isEmpty()) {
                province = provinces.get(0);
            }

            return province;
        } catch (SQLException e) {
            throw new Exception("Error searching province", e);
        }
    }

    public static @NotNull ArrayList<Province> getAll() throws Exception {
        ArrayList<Province> provinces;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getProvinces(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            provinces = Province.buildList(result);

            return provinces;
        } catch (SQLException e) {
            throw new Exception("Error searching provinces", e);
        }
    }

    public static @NotNull ArrayList<Province> getAll(Integer countryId) throws Exception {
        ArrayList<Province> provinces;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getProvinces(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setInt(2, countryId);

            ResultSet result = sql.executeQuery();

            provinces = Province.buildList(result);

            return provinces;
        } catch (SQLException e) {
            throw new Exception("Error searching provinces", e);
        }
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("provinceId");
        columns.add("provinceName");
        columns.add("countryId");

        return columns;
    }
}
