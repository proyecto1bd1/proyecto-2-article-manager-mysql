/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class StatProductPurchases {

    private String productName;
    private Integer totalPurchases;

    private Integer topN;

    public StatProductPurchases(){};



    public StatProductPurchases(String productName, Integer totalPurchases) {
        this.productName = productName;
        this.totalPurchases = totalPurchases;
    }

    public StatProductPurchases(Integer topN) {
        this.topN = topN;
    }

    public void setproductName(String productName) {
        this.productName = productName;
    }

    public void settotalPurchases(Integer totalPurchases) {
        this.totalPurchases = totalPurchases;
    }

    public String getproductName() {
        return productName;
    }

    public Integer gettotalPurchases() {
        return totalPurchases;
    }

    public ArrayList<StatProductPurchases> getAll() throws Exception {
        ArrayList<StatProductPurchases> rows = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call statMostPurchasedProducts(?)}");
            if(this.topN != null){
                sql.setInt(1,this.topN);
            }
            else{
                sql.setNull(1, Types.BIGINT);
            }
            ResultSet result = sql.executeQuery();

            while (result.next()) {
                StatProductPurchases stat = new StatProductPurchases(
                        result.getString("ProductName"),
                        result.getInt("Purchases")
                );
                rows.add(stat);
            }
            return rows;
        } catch (SQLException e) {
            throw new Exception("Unable to get statistic", e);
        }
    }

    public HashMap<String, String> toMap() {

        HashMap<String, String> statMap = new HashMap<>();

        statMap.put("Product", productName);
        statMap.put("Purchases", totalPurchases.toString());

        return statMap;
    }
}
