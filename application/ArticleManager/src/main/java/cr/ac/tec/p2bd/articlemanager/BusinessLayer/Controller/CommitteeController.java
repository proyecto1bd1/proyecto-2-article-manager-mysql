/*
 * The MIT License
 *
 * Copyright 2022 Daniel G.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Committee;
import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Person;
import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PersonXCommittee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;


/**
 * @author Daniel G, Diego Herrera
 */
public class CommitteeController {

    public ArrayList<HashMap<String, String>> getCommitteeRows() throws Exception {

        ArrayList<HashMap<String, String>> committeeRows = new ArrayList<>();

        ArrayList<Committee> committees = Committee.getAll();

        committees.forEach((Committee committee) -> {
            committeeRows.add(committee.toMap());
        });

        return committeeRows;
    }

    public ArrayList<String> getCommitteeColumns() throws Exception {
        Set<String> columns = Committee.getColumns();

        return new ArrayList<>(columns);
    }


    public HashMap<String, String> getCommitteeRow(Integer committeeId) throws Exception {
        Committee committee = Committee.find(committeeId);

        return committee.toMap();
    }

    public Integer createCommittee(String description) throws Exception {
        Committee committee = new Committee(description);

        committee.save();

        return committee.getCommitteeId();
    }

    public String updateCommittee(Integer committeeId, String description) throws Exception {
        Committee committee = Committee.find(committeeId);

        committee.setDescription(description);
        committee.save();

        return committee.getDescription();
    }

    public boolean deleteCommittee(Integer committeeId) throws Exception {
        Committee committee = Committee.find(committeeId);

        committee.delete();

        return committee.getCommitteeId() == null;
    }

    public ArrayList<HashMap<String, String>> getCommitteeMembers(Integer selectedCommittee) throws Exception {
        ArrayList<PersonXCommittee> members;
        ArrayList<HashMap<String, String>> desireMembers = new ArrayList<>();

        members = PersonXCommittee.getAll();

        for (PersonXCommittee member : members) {
            Committee committee = Committee.find(member.getCommitteeId());

            if (Objects.equals(committee.getCommitteeId(), selectedCommittee)) {
                desireMembers.add(Person.find(member.getPersonId()).toMap());
            }
        }

        return desireMembers;
    }
}
