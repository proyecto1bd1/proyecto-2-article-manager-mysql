/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Store extends AbstractModel {
    private Integer storeId;
    private String description;
    private Integer newspaperId;

    public Store() {
    }

    private Store(Integer storeId, String description, Integer newspaperId) {
        this.storeId = storeId;
        this.description = description;
        this.newspaperId = newspaperId;
    }

    public Store(String description, Integer newspaperId) {
        this.description = description;
        this.newspaperId = newspaperId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setNewspaperId(Integer newspaperId) {
        this.newspaperId = newspaperId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public String getDescription() {
        return description;
    }

    public Integer getNewspaperId() {
        return newspaperId;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.storeId != null) {
                CallableStatement sql = connection.prepareCall("{call updatestore(?, ?, ?)}");
                sql.setInt(1, this.storeId);
                sql.setString(2, this.description);
                sql.setInt(3, this.newspaperId);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createstore(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.description);
                sql.setInt(3, this.newspaperId);
                sql.execute();
                this.storeId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error: Unable to save store", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.storeId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletestore(?)}");
            sql.setInt(1, this.storeId);
            sql.executeQuery();
            this.storeId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete store", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "store{ storeId: %d description: %s newspaperId %d }",
                storeId,
                description,
                newspaperId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> storeMap = new HashMap<>();

        storeMap.put("storeId", storeId.toString());
        storeMap.put("description", description);
        storeMap.put("newspaperId", newspaperId.toString());

        return storeMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("storeId");
        columns.add("description");
        columns.add("newspaperId");

        return columns;
    }

    public static Store find(Integer id) throws Exception {
        Store store = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getstore(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                store = new Store(
                        result.getInt("StoreId"),
                        result.getString("Description"),
                        result.getInt("NewspaperId")
                );
            }

            return store;
        } catch (SQLException e) {
            throw new Exception("Unable to get store", e);
        }
    }

    public static ArrayList<Store> getAll() throws Exception {
        ArrayList<Store> stores = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getstore(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Store store = new Store(
                        result.getInt("StoreId"),
                        result.getString("Description"),
                        result.getInt("NewsPaperId")
                );
                stores.add(store);
            }

            return stores;
        } catch (SQLException e) {
            throw new Exception("Unable to get stores");
        }
    }
}
