/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import java.util.HashMap;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Product extends AbstractModel {
    private Integer ProductId;
    private String Name;
    private Integer PointsPrice;
    private Integer NumberInStock;
    private Integer StoreId;

    public Product() {
    }

    public Product(Integer ProductId, String Name, Integer PointsPrice, Integer NumberInStock, Integer StoreId) {
        this.ProductId = ProductId;
        this.Name = Name;
        this.PointsPrice = PointsPrice;
        this.NumberInStock = NumberInStock;
        this.StoreId = StoreId;
    }

    public Product(String Name, Integer PointsPrice, Integer NumberInStock, Integer StoreId) {
        this.Name = Name;
        this.PointsPrice = PointsPrice;
        this.NumberInStock = NumberInStock;
        this.StoreId = StoreId;
    }

    public void setProductId(Integer ProductId) {
        this.ProductId = ProductId;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setPointsPrice(Integer PointsPrice) {
        this.PointsPrice = PointsPrice;
    }

    public void setNumberInStock(Integer NumberInStock) {
        this.NumberInStock = NumberInStock;
    }

    public void setStoreId(Integer StoreId) {
        this.StoreId = StoreId;
    }

    public Integer getProductId() {
        return ProductId;
    }

    public String getName() {
        return Name;
    }

    public Integer getPointsPrice() {
        return PointsPrice;
    }

    public Integer getNumberInStock() {
        return NumberInStock;
    }

    public Integer getStoreId() {
        return StoreId;
    }

    @Override
    public String toString() {
        return String.format(
                "Product{ ProductId: %d Name: %s PointsPrice %d NumberInStock: %d StoreId: %d }",
                ProductId,
                Name,
                PointsPrice,
                NumberInStock,
                StoreId
        );
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.ProductId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateProduct(?, ?, ?, ?, ?)}");
                sql.setInt(1, this.ProductId);
                sql.setString(2, this.Name);
                sql.setInt(3, this.PointsPrice);
                sql.setInt(4, this.NumberInStock);
                sql.setInt(5, this.StoreId);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createProduct(?, ?, ?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.Name);
                sql.setInt(3, this.PointsPrice);
                sql.setInt(4, this.NumberInStock);
                sql.setInt(5, this.StoreId);
                sql.execute();

                this.ProductId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving product", e);
        }
    }

    @Override
    public void delete() throws Exception {
        // If id is not set, do nothing
        if (this.ProductId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteProduct(?)}");
            sql.setInt(1, this.ProductId);
            sql.executeQuery();

            this.ProductId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting product", e);
        }
    }


    @Override
    public HashMap<String, String> toMap() {

        HashMap<String, String> productMap = new HashMap<>();

        productMap.put("productId", ProductId.toString());
        productMap.put("Name", Name);
        productMap.put("PointsPrice", PointsPrice.toString());
        productMap.put("NumberInStock", NumberInStock.toString());
        productMap.put("StoreId", StoreId.toString());

        return productMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("productId");
        columns.add("name");
        columns.add("pointsPrice");
        columns.add("numberInStock");
        columns.add("storeId");

        return columns;
    }

    public static Product find(Integer id) throws Exception {
        Product product = null;
        ArrayList<Product> products;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getProduct(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            products = buildList(result);

            if (!products.isEmpty()) {
                product = products.get(0);
            }

            return product;
        } catch (SQLException e) {
            throw new Exception("Unable to get product", e);
        }
    }

    private static @NotNull ArrayList<Product> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Product> products = new ArrayList<>();

        while (result.next()) {
            Product product = new Product(
                    result.getInt("ProductId"),
                    result.getString("Name"),
                    result.getInt("PointsPrice"),
                    result.getInt("NumberInStock"),
                    result.getInt("StoreId")
            );

            products.add(product);
        }
        return products;
    }

    public static ArrayList<Product> getAll() throws Exception {
        ArrayList<Product> products = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getproduct(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {

                Product product = new Product(
                        result.getInt("ProductId"),
                        result.getString("Name"),
                        result.getInt("PointsPrice"),
                        result.getInt("NumberInStock"),
                        result.getInt("StoreId")
                );

                products.add(product);
            }

            return products;
        } catch (SQLException e) {
            throw new Exception("Unable to get products", e);
        }
    }
}
