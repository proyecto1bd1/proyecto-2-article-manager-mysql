/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.University;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class UniversityController {

    public UniversityController() {
    }

    public ArrayList<HashMap<String, String>> getUniversityRows() throws Exception {

        ArrayList<HashMap<String, String>> universityRows = new ArrayList<>();

        ArrayList<University> universities = University.getAll();

        universities.forEach((University university) -> {
            universityRows.add(university.toMap());
        });

        return universityRows;
    }

    public ArrayList<String> getUniversityColumns() throws Exception {
        Set<String> columns = University.getColumns();

        return new ArrayList<>(columns);
    }


    public HashMap<String, String> getUniversityRow(Integer universityId) throws Exception {
        University university = University.find(universityId);

        return university.toMap();
    }

    public Integer createUniversity(String universityName, String localAddress, Integer districtId) throws Exception {
        University university = new University(universityName, localAddress, districtId);

        university.save();

        return university.getUniversityId();
    }

    public String updateUniversity(Integer universityId, String universityName, String localAddress, Integer districtId) throws Exception {
        University university = University.find(universityId);

        university.setUniversityName(universityName);
        university.setDistrictId(districtId);
        university.setLocalAddress(localAddress);
        university.save();

        return university.getUniversityName();
    }

    public boolean deleteUniversity(Integer universityId) throws Exception {
        University university = University.find(universityId);

        university.delete();

        return university.getUniversityId() == null;
    }

    public ArrayList<HashMap<String, String>> getUniversityNoNewspaper() throws Exception {

        ArrayList<HashMap<String, String>> universityRows = new ArrayList<>();

        ArrayList<University> universities = University.getAllWithoutNewspaper();

        universities.forEach((University university) -> {
            universityRows.add(university.toMap());
        });

        return universityRows;
    }
}
