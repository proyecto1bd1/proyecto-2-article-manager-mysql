package cr.ac.tec.p2bd.articlemanager.UserInterface;

/**
 * Basic class to show choices in a javafx choice box
 */
public class Choice {
    /**
     * The value the choice will have when selected
     */
    final Integer id;
    /**
     * The text to be displayed in the choice box
     */
    final String displayText;

    /**
     * Creates a new choice with only the id
     * @param id The id of the choice
     */
    public Choice(Integer id) {
        this(id, null);
    }

    /**
     * Creates a new choice with only the name
     * @param displayText The name of the choice
     */
    public Choice(String displayText) {
        this(null, displayText);
    }

    /**
     * Creates a new choice with both id and name
     * @param id The id of the choice
     * @param displayText The name of the choice
     */
    public Choice(Integer id, String displayText) {
        this.id = id;
        this.displayText = displayText;
    }

    /**
     * Shows the name of the choice
     * @return The name of the choice
     */
    @Override
    public String toString() {
        return displayText;
    }

    /**
     * Determinate if the object to compare to is equals
     * @param o The object to compare against
     * @return true if is equals, false otherwise
     */
    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Choice choice = (Choice) o;
        return displayText != null && displayText.equals(choice.displayText) || id != null && id.equals(choice.id);
    }
}
