/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.NewsArticleController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PxCommentXArticleController;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.HashMap;

public class NewsShowcaseController {

    private final NewsArticleController newsArticleController = new NewsArticleController();
    private final PxCommentXArticleController pxCommentXArticleController = new PxCommentXArticleController();
    public BorderPane newsViewport;

    public void loadNews(Integer newsId) {
        try {
            HashMap<String, String> news = newsArticleController.getNewsArticleRow(newsId);

            FXMLLoader newsLoader = new FXMLLoader(getClass().getResource("news-visualizer-view.fxml"));
            AnchorPane newsVisualizer = newsLoader.load();
            NewsVisualizerController newsVisualizerController = newsLoader.getController();
            newsVisualizerController.setNews(news);

            newsViewport.setTop(newsVisualizer);

            FXMLLoader commentBoxLoader = new FXMLLoader(getClass().getResource("create-comment-view.fxml"));
            AnchorPane commentBox = commentBoxLoader.load();
            CreateCommentController createCommentController = commentBoxLoader.getController();
            createCommentController.setNewsId(newsId);

            newsViewport.setCenter(commentBox);

            ArrayList<HashMap<String, String>> comments = pxCommentXArticleController.getPxCommentXArticleRows(newsId);

            VBox box = new VBox();

            for (var comment : comments) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("comment-bubble-view.fxml"));
                AnchorPane commentBubble = loader.load();
                CommentBubbleController commentBubbleController = loader.getController();
                commentBubbleController.setComment(comment);
                box.getChildren().add(commentBubble);
            }

            newsViewport.setBottom(box);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
        }

    }
}
