/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */

public class Gender extends AbstractModel {

    private Integer genderId;
    private String genderName;

    public Gender() {
    }

    private Gender(Integer genderId, String genderName) {
        this.genderId = genderId;
        this.genderName = genderName;
    }

    public Gender(String genderName) {
        this.genderName = genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public String getGenderName() {
        return genderName;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (genderId != null) {
                CallableStatement sql = connection.prepareCall("{call updateGender(?, ?)}");
                sql.setInt(1, genderId);
                sql.setString(2, genderName);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createGender(?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, genderName);
                sql.execute();
                genderId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error creating gender", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (genderId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteGender(?)}");
            sql.setInt(1, genderId);
            sql.executeQuery();
            genderId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting gender", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Gender{ Id: %d Name: %s }",
                genderId,
                genderName
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> genderMap = new HashMap<>();

        genderMap.put("genderId", genderId.toString());
        genderMap.put("genderName", genderName);

        return genderMap;
    }

    private static @NotNull ArrayList<Gender> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Gender> genders = new ArrayList<>();

        while (result.next()) {
            Gender gender = new Gender(
                    result.getInt("genderId"),
                    result.getString("GenderName")
            );

            genders.add(gender);
        }

        return genders;
    }

    public static Gender find(Integer id) throws Exception {
        Gender gender = null;
        ArrayList<Gender> genders;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{ call getGenders(?) }");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            genders = buildList(result);

            if (!genders.isEmpty()) {
                gender = genders.get(0);
            }

            return gender;
        } catch (SQLException e) {
            throw new Exception("Error fetching genders", e);
        }
    }

    public static @NotNull ArrayList<Gender> getAll() throws Exception {
        ArrayList<Gender> genders = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{ call getGenders(?) }");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            genders = buildList(result);

            return genders;
        } catch (SQLException e) {
            throw new Exception("Error fetching genders", e);
        }
    }

    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("genderId");
        columns.add("genderName");

        return columns;
    }
}
