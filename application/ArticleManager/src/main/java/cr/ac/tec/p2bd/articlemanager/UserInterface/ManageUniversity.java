/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;


import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ManageUniversity {

    private final UniversityController universityController = new UniversityController();
    private final CantonController cantonController = new CantonController();
    private final ProvinceController provinceController = new ProvinceController();
    private final DistrictController districtController = new DistrictController();
    private final CountryController countryController = new CountryController();


    @FXML
    public TextField tfAddress;

    @FXML
    public TextField tfName;

    @FXML
    public ChoiceBox<Choice> cbCountry;

    @FXML
    public ChoiceBox<Choice> cbProvince;

    @FXML
    public ChoiceBox<Choice> cbCanton;

    @FXML
    public ChoiceBox<Choice> cbDistrict;

    @FXML
    public ChoiceBox<Choice> cbUniversity;

    @FXML
    public Button btnDelete;

    @FXML
    public Button btnSave;



    @FXML
    protected void initialize() throws IOException {
        ArrayList<Choice> options = ChoiceBuilder.getUniversityList();
        Choice newUniversity = new Choice(0, "Add new");
        options.add(0, newUniversity);
        cbUniversity.getItems().addAll(options);
        cbUniversity.setValue(newUniversity);
        cbCountry.getItems().addAll(ChoiceBuilder.getCountryList());
        cbUniversity.setOnAction(this::onUniversitySelected);
        cbCountry.setOnAction(this::onCountrySelected);
        cbProvince.setOnAction(this::onProvinceSelected);
        cbCanton.setOnAction(this::onCantonSelected);
        btnDelete.setDisable(true);

    }

    public void onUniversitySelected(ActionEvent event){
        Choice universitySelected = cbUniversity.getValue();
        if (universitySelected != null && universitySelected.id != 0){
            try{
                btnDelete.setDisable(false);
                HashMap<String, String> university = universityController.getUniversityRow(universitySelected.id);
                Integer districtId = Integer.valueOf(university.get("districtId"));
                HashMap<String, String> district = districtController.getDistrictRow(districtId);
                Integer cantonId = Integer.valueOf(district.get("cantonId"));
                HashMap<String, String> canton = cantonController.getCantonRow(cantonId);
                Integer provinceId = Integer.valueOf(canton.get("provinceId"));
                HashMap<String, String> province = provinceController.getProvinceRow(provinceId);
                Integer countryId = Integer.valueOf(province.get("countryId"));
                HashMap<String, String> country = countryController.getCountryRow(countryId);
                Choice countryChoice = new Choice(countryId, country.get("countryName"));
                cbCountry.setValue(countryChoice);
                Choice provinceChoice = new Choice(provinceId, province.get("provinceName"));
                Choice cantonChoice = new Choice(cantonId, canton.get("cantonName"));
                Choice districtChoice = new Choice(districtId, district.get("districtName"));
                cbProvince.setValue(provinceChoice);
                cbCanton.setValue(cantonChoice);
                cbDistrict.setValue(districtChoice);
                tfAddress.setText(university.get("localAddress"));
                tfName.setText(universitySelected.displayText);
            }catch (Exception e){
                System.out.println(e);
            }
        }
        if(universitySelected != null && universitySelected.id == 0){
            btnDelete.setDisable(true);
            tfName.clear();
            tfAddress.clear();
        }


    }

    public void onCountrySelected(ActionEvent event) {
        Choice countrySelected = cbCountry.getValue();
        if (countrySelected!= null){
            cbProvince.getItems().clear();
            cbProvince.getItems().addAll(ChoiceBuilder.getProvinceList(countrySelected.id));
        }

    }

    public void onProvinceSelected(ActionEvent event) {
        Choice provinceSelected = cbProvince.getValue();
        if (provinceSelected != null){
            cbCanton.getItems().clear();
            cbCanton.getItems().addAll(ChoiceBuilder.getCantonList(provinceSelected.id));
        }

    }

    public void onCantonSelected(ActionEvent event) {
        Choice cantonSelected = cbCanton.getValue();
        if (cantonSelected != null){
            cbDistrict.getItems().clear();
            cbDistrict.getItems().addAll(ChoiceBuilder.getDistrictList(cantonSelected.id));
        }

    }

    public void onSave(ActionEvent event) {
        Choice universitySelected = cbUniversity.getValue();
        if (universitySelected.id != 0){
            try{
                universityController.updateUniversity(universitySelected.id, tfName.getText(), tfAddress.getText(), cbDistrict.getValue().id);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("University successfully updated");
                //alert.setContentText(e.getMessage());
                alert.show();
                refresh();

            }
            catch(Exception e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Failed operation");
                alert.setHeaderText("University could not be updated");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
        else{
            try{
                universityController.createUniversity(tfName.getText(), tfAddress.getText(), cbDistrict.getValue().id);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("University successfully created");
                //alert.setContentText(e.getMessage());
                alert.show();
                refresh();

            }catch(Exception e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Failed operation");
                alert.setHeaderText("University could not be created");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
        }

    public void onDelete(ActionEvent event){
        Choice universitySelected = cbUniversity.getValue();
        if(universitySelected.id != 0){
            try{
                universityController.deleteUniversity(universitySelected.id);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("University successfully deleted");
                //alert.setContentText(e.getMessage());
                alert.show();
                refresh();

            }catch(Exception e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Failed operation");
                alert.setHeaderText("University could not be deleted");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
    }

    public void refresh(){
        cbUniversity.getItems().clear();
        ArrayList<Choice> options = ChoiceBuilder.getUniversityList();
        Choice newUniversity = new Choice(0, "Add new");
        options.add(0, newUniversity);
        cbUniversity.getItems().addAll(options);
        cbUniversity.setValue(newUniversity);
        tfName.clear();
        tfAddress.clear();
        btnDelete.setDisable(true);
    }










}
