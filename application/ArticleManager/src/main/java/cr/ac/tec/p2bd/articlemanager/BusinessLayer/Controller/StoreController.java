/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class StoreController {
    public ArrayList<HashMap<String, String>> getStoreRows() throws Exception {

        ArrayList<HashMap<String, String>> storeRows = new ArrayList<>();

        ArrayList<Store> stores = Store.getAll();

        stores.forEach((Store store) -> {
            storeRows.add(store.toMap());
        });

        return storeRows;
    }

    public ArrayList<String> getStoreColumns() throws Exception {
        Set<String> columns = Store.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getStoreRow(Integer storeId) throws Exception {
        Store store = Store.find(storeId);

        return store.toMap();
    }

    public Integer createStore(String storeName, Integer newsPaperId) throws Exception {
        Store store = new Store(storeName, newsPaperId);

        store.save();

        return store.getStoreId();
    }

    public String updateStore(Integer storeId, String storeName, Integer newsPaperId) throws Exception {
        Store store = Store.find(storeId);

        store.setDescription(storeName);
        store.setNewspaperId(newsPaperId);
        store.save();

        return store.getDescription();
    }

    public boolean deleteStore(Integer storeId) throws Exception {
        Store store = Store.find(storeId);

        store.delete();

        return store.getStoreId() == null;
    }
}
