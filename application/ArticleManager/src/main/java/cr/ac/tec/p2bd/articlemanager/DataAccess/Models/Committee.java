/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Committee extends AbstractModel {

    private Integer CommitteeId;
    private String Description;

    public Committee() {
    }

    public Committee(Integer CommitteeId, String Description) {
        this.CommitteeId = CommitteeId;
        this.Description = Description;
    }

    public Committee(String Description) {
        this.Description = Description;
    }

    public void setCommitteeId(Integer CommitteeId) {
        this.CommitteeId = CommitteeId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getCommitteeId() {
        return CommitteeId;
    }

    public String getDescription() {
        return Description;
    }

    @Override
    public String toString() {
        return String.format(
                "Committee{ CommitteeId: %d, Description: %s}",
                CommitteeId,
                Description
        );
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.CommitteeId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateCommittee(?, ?)}");
                sql.setInt(1, this.CommitteeId);
                sql.setString(2, this.Description);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createCommittee(?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.Description);
                sql.execute();
                this.CommitteeId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error: Unable to save committee", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.CommitteeId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteCommittee(?)}");
            sql.setInt(1, this.CommitteeId);
            sql.executeQuery();
            this.CommitteeId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete committee", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> committeeMap = new HashMap<>();

        committeeMap.put("committeeId", CommitteeId.toString());
        committeeMap.put("description", Description);

        return committeeMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("committeeId");
        columns.add("description");

        return columns;
    }

    public static Committee find(Integer id) throws Exception {
        Committee committee = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCommittee(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                committee = new Committee(
                        result.getInt("CommitteeId"),
                        result.getString("Description")
                );
            }

            return committee;
        } catch (SQLException e) {
            throw new Exception("Unable to get committee", e);
        }
    }

    public static ArrayList<Committee> getAll() throws Exception {
        ArrayList<Committee> committees = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCommittee(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Committee committee = new Committee(
                        result.getInt("CommitteeId"),
                        result.getString("Description")
                );
                committees.add(committee);
            }

            return committees;
        } catch (SQLException e) {
            throw new Exception("Unable to get committees", e);
        }
    }
}
