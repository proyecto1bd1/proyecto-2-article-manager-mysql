/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CantonController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.CountryController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DistrictController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProvinceController;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.HashMap;

public class LocationPickerController {
    public AnchorPane anchorPane;
    private String selectedLocation;
    private final CountryController countryController = new CountryController();
    private final ProvinceController provinceController = new ProvinceController();
    private final CantonController cantonController = new CantonController();
    private final DistrictController districtController = new DistrictController();

    private HashMap<String, String> location;
    public ChoiceBox<String> cbLocationType;
    public ChoiceBox<Choice> cbCountry;
    public ChoiceBox<Choice> cbProvince;
    public ChoiceBox<Choice> cbCanton;
    public ChoiceBox<Choice> cbDistrict;

    public void initialize() {
        String[] locationOptions = {"Country", "Province", "Canton", "District"};
        cbLocationType.getItems().addAll(locationOptions);
        cbLocationType.setOnAction(this::onLocationSelected);
        cbCountry.getItems().addAll(ChoiceBuilder.getCountryList());
        cbCountry.setOnAction(this::onCountrySelected);

        cbProvince.setOnAction(this::onProvinceSelected);

        cbCanton.setOnAction(this::onCantonSelected);

        cbCountry.setDisable(true);
        cbProvince.setDisable(true);
        cbCanton.setDisable(true);
        cbDistrict.setDisable(true);
    }

    public void onLocationSelected(ActionEvent event) {
        selectedLocation = cbLocationType.getValue();

        cbCountry.getItems().clear();
        cbProvince.getItems().clear();
        cbCanton.getItems().clear();

        switch (selectedLocation) {
            case "Country" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(true);
                cbCanton.setDisable(true);
                cbDistrict.setDisable(true);
            }
            case "Province" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(false);
                cbCanton.setDisable(true);
                cbDistrict.setDisable(true);
            }
            case "Canton" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(false);
                cbCanton.setDisable(false);
                cbDistrict.setDisable(true);
            }
            case "District" -> {
                cbCountry.setDisable(false);
                cbProvince.setDisable(false);
                cbCanton.setDisable(false);
                cbDistrict.setDisable(false);
            }
        }

        cbCountry.getItems().addAll(ChoiceBuilder.getCountryList());
    }

    public void onCountrySelected(ActionEvent event) {
        Choice countrySelected = cbCountry.getValue();
        if (countrySelected != null) {
            cbProvince.getItems().clear();
            cbProvince.getItems().addAll(ChoiceBuilder.getProvinceList(countrySelected.id));
        }
    }

    public void onProvinceSelected(ActionEvent event) {
        Choice provinceSelected = cbProvince.getValue();
        if (provinceSelected != null) {
            cbCanton.getItems().clear();
            cbCanton.getItems().addAll(ChoiceBuilder.getCantonList(provinceSelected.id));
        }
    }

    public void onCantonSelected(ActionEvent event) {
        Choice cantonSelected = cbCanton.getValue();
        if (cantonSelected != null) {
            cbDistrict.getItems().clear();
            cbDistrict.getItems().addAll(ChoiceBuilder.getDistrictList(cantonSelected.id));
        }
    }

    public void onConfirm(ActionEvent event) {
        try {
            switch (selectedLocation) {
                case "Country" -> {
                    location = countryController.getCountryRow(cbCountry.getValue().id);
                }
                case "Province" -> {
                    location = provinceController.getProvinceRow(cbProvince.getValue().id);
                }
                case "Canton" -> {
                    location = cantonController.getCantonRow(cbCanton.getValue().id);
                }
                case "District" -> {
                    location = districtController.getDistrictRow(cbDistrict.getValue().id);
                }
            }

            Stage stage = (Stage) anchorPane.getScene().getWindow();
            stage.close();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch location");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public HashMap<String, String> getLocation() {
        return location;
    }
}
