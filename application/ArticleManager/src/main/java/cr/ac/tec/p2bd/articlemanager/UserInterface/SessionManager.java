package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.*;
import javafx.scene.control.Alert;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class SessionManager {
    /**
     * Singleton declaration of the session
     */
    private static SessionManager session = null;
    private final PersonController personController;
    private final AuthorController authorController;
    private final PersonXCommitteeController personXCommitteeController;
    private final AdminController adminController;

    private Integer selectedNewsPaper;

    /**
     * The data of the person logged in
     */
    private HashMap<String, String> personData;
    private boolean isPersonAdmin;
    private boolean isPersonAuthor;
    private boolean isPersonGuest;
    private boolean isPersonInCommittee;
    private final ArrayList<Integer> committeesId;
    private int points;
    private HomeController homeController;

    public static SessionManager getInstance() {
        if (session == null) {
            session = new SessionManager();
        }

        session.determinateUserData();

        return session;
    }

    private SessionManager() {
        points = 0;
        isPersonAdmin = false;
        isPersonAuthor = false;
        isPersonGuest = true;
        isPersonInCommittee = false;
        committeesId = new ArrayList<>();
        personController = new PersonController();
        authorController = new AuthorController();
        personXCommitteeController = new PersonXCommitteeController();
        adminController = new AdminController();
    }

    private void determinateUserData() {

    }

    public void logOut() {
        personData = null;
        isPersonAdmin = false;
        isPersonAuthor = false;
        isPersonGuest = true;
        isPersonInCommittee = false;
        committeesId.clear();
        homeController = null;
    }

    public void setGuest() {
        isPersonGuest = true;
        personData = null;
    }

    public boolean isGuest() {
        return isPersonGuest;
    }

    public boolean isAuthor() {
        return isPersonAuthor;
    }

    public boolean isAdmin() {
        return isPersonAdmin;
    }

    public boolean isInCommittee() {
        return isPersonInCommittee;
    }

    public int getPoints() {
        try {
            points = authorController.authorAvailablePoints(Integer.valueOf(personData.get("PersonId")));
            return points;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
            return 0;
        }
    }

    public void setPersonData(HashMap<String, String> personData) {
        this.isPersonGuest = false;
        this.personData = personData;
        Integer personId = Integer.valueOf(personData.get("PersonId"));
        try {
            HashMap<String, String> admin = adminController.getAdminRow(personId);

            isPersonAdmin = admin != null;

            HashMap<String, String> author = authorController.getAuthorRow(personId);

            isPersonAuthor = author != null;

            if (isPersonAuthor) {
                points = authorController.authorAvailablePoints(personId);
            }

            ArrayList<HashMap<String, String>> committees = personXCommitteeController.getPersonXCommitteeRows();

            for (var committee : committees) {
                if (committee.get("personId").equals(personId.toString())) {
                    committeesId.add(Integer.valueOf(committee.get("committeeId")));
                }
            }

            isPersonInCommittee = !committeesId.isEmpty();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public HashMap<String, String> getPersonData() {
        return personData;
    }

    //TODO change for same values as controller, connect with person controller
    public void updateProfile(Integer personId, String IdCard, String Name, String Firstlastname, String Secondlastname, String Birthdate, File Photo, String LocalAddress, String OldPassword, String NewPassword, Integer Districtid, Integer Genderid, Integer typeid) throws Exception {
        personController.updatePerson(
                personId,
                IdCard,
                Name,
                Firstlastname,
                Secondlastname,
                Birthdate,
                Photo,
                LocalAddress,
                OldPassword,
                NewPassword,
                Districtid,
                Genderid,
                typeid
        );

        setPersonData(personController.getPersonRow(personId));

    }

    public HomeController getHomeController() {
        return homeController;
    }

    public void setHomeController(HomeController homeController) {
        this.homeController = homeController;
    }

    public Integer getSelectedNewsPaper() {
        return selectedNewsPaper;
    }

    public void setSelectedNewsPaper(Integer selectedNewsPaper) {
        this.selectedNewsPaper = selectedNewsPaper;
    }
}
