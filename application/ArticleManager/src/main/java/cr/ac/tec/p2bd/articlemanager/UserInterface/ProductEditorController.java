/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.ProductController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.StoreController;
import javafx.event.ActionEvent;
import javafx.scene.control.*;

import java.io.IOException;
import java.util.HashMap;

public class ProductEditorController {
    private final StoreController storeController = new StoreController();
    private final ProductController productController = new ProductController();
    private HashMap<String, String> product = null;

    public TextField txtName;
    public Spinner<Integer> spnStock;
    public Spinner<Integer> spnPoints;
    public ChoiceBox<Choice> cbStore;

    
    public void initialize() {
        spnPoints.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 99)
        );

        spnStock.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 99)
        );

        cbStore.getItems().addAll(ChoiceBuilder.getStoreList());
    }
    
    public void editMode(HashMap<String, String> product) {
        this.product = product;
        spnPoints.getValueFactory().setValue(Integer.valueOf(product.get("PointsPrice")));
        spnStock.getValueFactory().setValue(Integer.valueOf(product.get("NumberInStock")));
        txtName.setText(product.get("Name"));

        try {
            HashMap<String, String> store = storeController.getStoreRow(Integer.valueOf(product.get("StoreId")));
            cbStore.setValue(
                    new Choice(
                            Integer.valueOf(store.get("storeId")),
                            store.get("description")
                    )
            );
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch store data");
            alert.setHeaderText("We failed at fetching store data");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }
    
    public void onSave(ActionEvent event) {
        if (cbStore.getValue() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Must select a store");
            alert.setHeaderText("We cannot register the product without a store");
            alert.setContentText("Please add a store to the product");
            alert.show();
            return;
        }

        try {

            if (product != null) {
                productController.updateProduct(
                        Integer.valueOf(product.get("productId")),
                        txtName.getText(),
                        spnPoints.getValue(),
                        spnStock.getValue(),
                        cbStore.getValue().id
                );
            } else {
                productController.createProduct(
                        txtName.getText(),
                        spnPoints.getValue(),
                        spnStock.getValue(),
                        cbStore.getValue().id
                );
            }


            Alert confirmation = new Alert(Alert.AlertType.INFORMATION);
            confirmation.setTitle("Product saved");
            confirmation.setHeaderText("The product data has been save");
            confirmation.setContentText("You will be sent to the product management screen");

            if (confirmation.showAndWait().get() == ButtonType.OK) {
                SessionManager.getInstance().getHomeController().onManageProducts();
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to save product");
            alert.setHeaderText("We failed at saving product data");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void onCancel(ActionEvent event) throws IOException {
        SessionManager.getInstance().getHomeController().onManageProducts();
    }
}
