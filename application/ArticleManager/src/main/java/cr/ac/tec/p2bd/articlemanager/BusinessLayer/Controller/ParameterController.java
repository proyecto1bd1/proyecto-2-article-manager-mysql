/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author
 */
public class ParameterController {

    public ParameterController() {
    }

    public ArrayList<HashMap<String, String>> getParameterRows() throws Exception {

        ArrayList<HashMap<String, String>> parameterRows = new ArrayList<>();

        ArrayList<Parameter> parameters = Parameter.getAll();

        parameters.forEach((Parameter parameter) -> {
            parameterRows.add(parameter.toMap());
        });

        return parameterRows;
    }

    public ArrayList<String> getParameterColumns() throws Exception {
        Set<String> columns = Parameter.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getParameterRow(Integer parameterId) throws Exception {
        Parameter parameter = Parameter.find(parameterId);

        return parameter.toMap();
    }

    public Integer createParameter(String parameterName, String description, String value) throws Exception {
        Parameter parameter = new Parameter(parameterName, description, value);

        parameter.save();

        return parameter.getParameterId();
    }

    public String updateParameter(Integer parameterId, String parameterName, String parameterDescription, String parameterValue) throws Exception {
        Parameter parameter = Parameter.find(parameterId);

        parameter.setName(parameterName);
        parameter.setDescription(parameterDescription);
        parameter.setValue(parameterValue);
        parameter.save();

        return parameter.getName();
    }

    public boolean deleteParameter(Integer parameterId) throws Exception {
        Parameter parameter = Parameter.find(parameterId);

        parameter.delete();

        return parameter.getParameterId() == null;
    }

}
