/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Diego Herrera, Daniel G
 */
public class Author extends AbstractModel {
    private Integer AuthorId;
    private Integer AuthorTypeId;

    public Author() {
    }

    public Author(Integer AuthorTypeId) {
        this.AuthorTypeId = AuthorTypeId;
    }

    public Author(Integer AuthorId, Integer AuthorTypeId) {
        this.AuthorId = AuthorId;
        this.AuthorTypeId = AuthorTypeId;
    }

    public void setAuthorId(Integer AuthorId) {
        this.AuthorId = AuthorId;
    }

    public void setAuthorTypeId(Integer AuthorTypeId) {
        this.AuthorTypeId = AuthorTypeId;
    }

    public Integer getAuthorId() {
        return AuthorId;
    }

    public Integer getAuthorTypeId() {
        return AuthorTypeId;
    }

    @Override
    public void save() throws Exception {
        // check if author exist
        Author author = find(AuthorId);

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql;
            if (author != null) {
                sql = connection.prepareCall("{call updateAuthor(?, ?)}");
            } else {
                sql = connection.prepareCall("{call createAuthor(?, ?)}");
            }
            sql.setInt(1, AuthorId);
            sql.setInt(2, AuthorTypeId);
            sql.executeQuery();
        } catch (SQLException e) {
            throw new Exception("Failed to save author", e);
        }
    }

    public static @NotNull Set<String> getColumns() {
        HashSet<String> columns = new HashSet<>();

        columns.add("authorId");
        columns.add("authorTypeId");

        return columns;
    }

    @Override
    public void delete() throws Exception {
        Author author = find(AuthorId);

        // if author doesn't exist, get out
        if (author == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteAuthor(?)}");
            sql.setInt(1, AuthorId);
            sql.executeQuery();
        } catch (SQLException e) {
            throw new Exception("Failed to delete author", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Author{ Id: %d Type: %d }",
                AuthorId,
                AuthorTypeId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> authorMap = new HashMap<>();

        authorMap.put("AuthorId", AuthorId.toString());
        authorMap.put("AuthorTypeId", AuthorTypeId.toString());

        return authorMap;
    }

    private static @NotNull ArrayList<Author> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Author> authors = new ArrayList<>();

        while (result.next()) {
            Author author = new Author(
                    result.getInt("AuthorId"),
                    result.getInt("AuthorTypeId")
            );

            authors.add(author);
        }

        return authors;
    }

    public static Author find(Integer id) throws Exception {
        Author author = null;
        ArrayList<Author> authors;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getAuthors(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            authors = buildList(result);

            if (!authors.isEmpty()) {
                author = authors.get(0);
            }

            return author;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch authors", e);
        }
    }

    public static @NotNull ArrayList<Author> getAll() throws Exception {
        ArrayList<Author> authors;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getAuthors(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            authors = buildList(result);

            return authors;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch authors", e);
        }
    }

    public static Integer getAuthorPoints(Integer authorId) throws Exception {
        int points = 0;
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getAuthorPoints(?)}");
            sql.setInt(1, authorId);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                points = result.getInt("points");
            }

            return points;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch authors", e);
        }
    }

    public static Integer getAuthorAvailablePoints(Integer authorId) throws Exception {
        int points = 0;
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{? = call getAuthorAvailablePoints(?)}");
            sql.registerOutParameter(1, Types.INTEGER);
            sql.setInt(2, authorId);
            sql.execute();

            points = sql.getInt(1);

            return points;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch authors", e);
        }
    }
}
