/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class EmailController {
    public ArrayList<HashMap<String, String>> getEmailRows() throws Exception {

        ArrayList<HashMap<String, String>> emailRows = new ArrayList<>();

        ArrayList<Email> emails = Email.getAll();

        emails.forEach((Email email) -> {
            emailRows.add(email.toMap());
        });

        return emailRows;
    }

    public ArrayList<String> getEmailColumns() throws Exception {
        Set<String> columns = Email.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getEmailRow(Integer emailId) throws Exception {
        Email email = Email.find(emailId);

        return email.toMap();
    }

    public Integer createEmail(String emailName, Integer personId) throws Exception {
        Email email = new Email(emailName, personId);

        email.save();

        return email.getEmailId();
    }

    public String updateEmail(Integer emailId, String emailName, Integer personId) throws Exception {
        Email email = Email.find(emailId);

        email.setEmailAddress(emailName);
        email.setPersonId(personId);
        email.save();

        return email.getEmailAddress();
    }

    public boolean deleteEmail(Integer emailId) throws Exception {
        Email email = Email.find(emailId);

        email.delete();

        return email.getEmailId() == null;
    }
}
