/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PxCommentXArticleController;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

public class CreateCommentController {
    private final PxCommentXArticleController pxCommentXArticleController = new PxCommentXArticleController();
    public TextArea txtComment;
    public Button btnSubmit;

    private Integer newsId;

    public void initialize() {
        if (SessionManager.getInstance().isGuest()) {
            txtComment.setEditable(false);
            txtComment.setDisable(true);
            btnSubmit.setDisable(true);
        }
    }

    public void createComment(ActionEvent event) {
        Integer personId = Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId"));

        try {
            pxCommentXArticleController.createPxCommentXArticle(personId, newsId, txtComment.getText());

            SessionManager.getInstance().getHomeController().onNewsSelected(newsId);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to comment");
            alert.setHeaderText("We where unable to register your comment");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
