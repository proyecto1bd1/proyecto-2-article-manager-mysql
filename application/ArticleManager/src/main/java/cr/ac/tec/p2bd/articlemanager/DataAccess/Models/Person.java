/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Diego Herrera, Daniel G
 */
public class Person extends AbstractModel {

    private Integer PersonId;
    private String IdCard;
    private String Name;
    private String Firstlastname;
    private String Secondlastname;
    private Date Birthdate;
    private byte[] Photo;
    private String LocalAddress;
    private String Username;
    private String Password;
    private Integer Districtid;
    private Integer Genderid;
    private Integer typeid;

    public Person() {
    }

    public Person(String IdCard, String Name, String Firstlastname, String Secondlastname, Date Birthdate, byte[] Photo, String LocalAddress, String Username, String Password, Integer Districtid, Integer Genderid, Integer typeid) {
        this.IdCard = IdCard;
        this.Name = Name;
        this.Firstlastname = Firstlastname;
        this.Secondlastname = Secondlastname;
        this.Birthdate = Birthdate;
        this.Photo = Photo;
        this.LocalAddress = LocalAddress;
        this.Username = Username;
        this.Password = Password;
        this.Districtid = Districtid;
        this.Genderid = Genderid;
        this.typeid = typeid;
    }

    private Person(Integer PersonId, String IdCard, String Name, String Firstlastname, String Secondlastname, Date Birthdate, byte[] Photo, String LocalAddress, String Username, String Password, Integer Districtid, Integer Genderid, Integer typeid) {
        this.PersonId = PersonId;
        this.IdCard = IdCard;
        this.Name = Name;
        this.Firstlastname = Firstlastname;
        this.Secondlastname = Secondlastname;
        this.Birthdate = Birthdate;
        this.Photo = Photo;
        this.LocalAddress = LocalAddress;
        this.Username = Username;
        this.Password = Password;
        this.Districtid = Districtid;
        this.Genderid = Genderid;
        this.typeid = typeid;
    }

    public byte[] getPhoto() {
        return Photo;
    }

    public void setPhoto(byte[] Photo) {
        this.Photo = Photo;
    }

    public void setIdCard(String IdCard) {
        this.IdCard = IdCard;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setFirstlastname(String Firstlastname) {
        this.Firstlastname = Firstlastname;
    }

    public void setSecondlastname(String Secondlastname) {
        this.Secondlastname = Secondlastname;
    }

    public void setBirthdate(Date Birthdate) {
        this.Birthdate = Birthdate;
    }

    public void setLocalAddress(String LocalAddress) {
        this.LocalAddress = LocalAddress;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setDistrictid(Integer Districtid) {
        this.Districtid = Districtid;
    }

    public void setGenderid(Integer Genderid) {
        this.Genderid = Genderid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public String getIdCard() {
        return IdCard;
    }

    public String getName() {
        return Name;
    }

    public String getFirstlastname() {
        return Firstlastname;
    }

    public String getSecondlastname() {
        return Secondlastname;
    }

    public Date getBirthdate() {
        return Birthdate;
    }

    public String getLocalAddress() {
        return LocalAddress;
    }

    public String getPassword() {
        return Password;
    }

    public Integer getDistrictid() {
        return Districtid;
    }

    public Integer getGenderid() {
        return Genderid;
    }

    public Integer getTypeid() {
        return typeid;
    }

    @Override
    public void save() throws Exception {
        ByteArrayInputStream photoBytes = Photo != null ? new ByteArrayInputStream(Photo) : null;
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (PersonId != null) {
                CallableStatement sql = connection.prepareCall("{call updatePerson(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

                sql.setInt(1, PersonId);
                sql.setString(2, IdCard);
                sql.setString(3, Name);
                sql.setString(4, Firstlastname);
                sql.setString(5, Secondlastname);
                sql.setDate(6, Birthdate);
                sql.setBlob(7, photoBytes);
                sql.setString(8, LocalAddress);
                sql.setString(9, Password);
                sql.setInt(10, Genderid);
                sql.setInt(11, typeid);
                sql.setInt(12, Districtid);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createPerson(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, IdCard);
                sql.setString(3, Name);
                sql.setString(4, Firstlastname);
                sql.setString(5, Secondlastname);
                sql.setDate(6, Birthdate);
                sql.setBlob(7, photoBytes);
                sql.setString(8, LocalAddress);
                sql.setString(9, Username);
                sql.setString(10, Password);
                sql.setInt(11, Genderid);
                sql.setInt(12, typeid);
                sql.setInt(13, Districtid);
                sql.execute();

                PersonId = sql.getInt(1);
            }
        } catch (SQLException e) {
            Logger.getLogger(Person.class.getName()).log(Level.INFO, null, e);
            throw new Exception("Error saving person", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (PersonId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletePerson(?)}");
            sql.setInt(1, PersonId);
            sql.executeQuery();

            PersonId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting person", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Person{ Id: %d IdCard: %s Name: %s FirstLastName: %s "
                        + "SecondLastName: %s Birthdate: %s LocalAddress: %s "
                        + "Username: %s GenderId: %d TypeId: %d DistrictId: %d }",
                PersonId,
                IdCard,
                Name,
                Firstlastname,
                Secondlastname,
                Birthdate.toString(),
                LocalAddress,
                Username,
                Genderid,
                typeid,
                Districtid
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> personMap = new HashMap<>();

        personMap.put("PersonId", PersonId.toString());
        personMap.put("IdCard", IdCard);
        personMap.put("Name", Name);
        personMap.put("FirstLastName", Firstlastname);
        personMap.put("SecondLastName", Secondlastname != null ? Secondlastname : "");
        personMap.put("Birthdate", Birthdate.toString());
        personMap.put("Photo", Photo != null ? Base64.getEncoder().encodeToString(Photo) : "");
        personMap.put("LocalAddress", LocalAddress);
        personMap.put("Username", Username);
        personMap.put("GenderId", Genderid.toString());
        personMap.put("TypeId", typeid.toString());
        personMap.put("DistrictId", Districtid.toString());

        return personMap;
    }

    private static @NotNull ArrayList<Person> buildList(@NotNull ResultSet result) throws SQLException, IOException {
        ArrayList<Person> persons = new ArrayList<>();

        while (result.next()) {
            byte[] photo = (result.getBlob("Photo") != null) ?
                    result.getBlob("Photo").getBinaryStream().readAllBytes() :
                    null;
            Person person = new Person(
                    result.getInt("PersonId"),
                    result.getString("IdCard"),
                    result.getString("Name"),
                    result.getString("FirstLastName"),
                    result.getString("SecondLastName"),
                    result.getDate("Birthdate"),
                    photo,
                    result.getString("LocalAddress"),
                    result.getString("Username"),
                    result.getString("Password"),
                    result.getInt("DistrictId"),
                    result.getInt("GenderId"),
                    result.getInt("TypeId")
            );

            persons.add(person);
        }

        return persons;
    }

    public static Person find(Integer id) throws Exception {
        Person person = null;
        ArrayList<Person> persons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPersons(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            persons = buildList(result);

            if (!persons.isEmpty()) {
                person = persons.get(0);
            }

            return person;
        } catch (SQLException e) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, e);
            throw new Exception("Error fetching user", e);
        }
    }

    public static @NotNull ArrayList<Person> getAll() throws Exception {
        ArrayList<Person> persons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPersons(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            persons = buildList(result);

            return persons;

        } catch (SQLException e) {
            throw new Exception("Error fetching users", e);
        }
    }

    public static Person find(String username) throws Exception {
        Person person = null;
        ArrayList<Person> persons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPersonByUsername(?)}");
            sql.setString(1, username);

            ResultSet result = sql.executeQuery();

            persons = buildList(result);

            if (!persons.isEmpty()) {
                person = persons.get(0);
            }

            return person;
        } catch (SQLException e) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, e);
            throw new Exception("Error fetching user", e);
        }
    }

    public static @NotNull ArrayList<Person> getPersonNoCommittee() throws Exception {
        ArrayList<Person> persons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPersonNoCommittee()}");

            ResultSet result = sql.executeQuery();

            persons = buildList(result);

            return persons;

        } catch (SQLException e) {
            throw new Exception("Error fetching users", e);
        }
    }

    public static @NotNull ArrayList<Person> getPersonNoNewspaper() throws Exception {
        ArrayList<Person> persons;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPersonNoNewspaper()}");

            ResultSet result = sql.executeQuery();

            persons = buildList(result);

            return persons;

        } catch (SQLException e) {
            throw new Exception("Error fetching users", e);
        }
    }
}
