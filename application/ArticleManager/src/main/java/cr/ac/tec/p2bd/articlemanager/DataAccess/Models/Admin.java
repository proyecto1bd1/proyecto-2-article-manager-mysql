/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Daniel G
 */
public class Admin extends AbstractModel {

    private Integer PersonId;

    public Admin() {
    }

    public Admin(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    @Override
    public String toString() {
        return String.format("Admin{ PersonId=%d }", PersonId);
    }

    @Override
    public void save() throws Exception {
        Admin admin = find(PersonId);

        try (Connection connection = DBConnection.getCon()) {

            if (admin == null) {
                assert connection != null;
                CallableStatement sql = connection.prepareCall("{ call createAdmin(?) }");
                sql.setInt(1, PersonId);
                sql.executeQuery();
            }
        } catch (SQLException e) {
            throw new Exception("Error saving admin", e);
        }
    }

    @Override
    public void delete() throws Exception {
        Admin admin = find(PersonId);

        if (admin == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteAdmin(?)}");
            sql.setInt(1, this.PersonId);
            sql.executeQuery();
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete admin", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> adminMap = new HashMap<>();
        adminMap.put("personId", PersonId.toString());
        return adminMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("PersonId");

        return columns;
    }

    public static Admin find(Integer id) throws Exception {
        Admin admin = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getadmin(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                admin = new Admin(
                        result.getInt("adminId")
                );
            }

            return admin;
        } catch (SQLException e) {
            throw new Exception("Unable to get admin", e);
        }
    }

    public static ArrayList<Admin> getAll() throws Exception {
        ArrayList<Admin> admins = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getadmin(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Admin admin = new Admin(
                        result.getInt("adminId")
                );
                admins.add(admin);
            }

            return admins;
        } catch (SQLException e) {
            throw new Exception("Unable to get admins", e);
        }
    }
}
