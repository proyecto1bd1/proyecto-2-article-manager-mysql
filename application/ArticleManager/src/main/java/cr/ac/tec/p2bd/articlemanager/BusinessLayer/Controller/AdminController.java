/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class AdminController {

    public AdminController() {
    }

    public ArrayList<HashMap<String, String>> getAdminRows() throws Exception {
        ArrayList<HashMap<String, String>> adminRows = new ArrayList<>();

        ArrayList<Admin> admins = Admin.getAll();

        admins.forEach((Admin admin) -> {
            adminRows.add(admin.toMap());
        });

        return adminRows;

    }

    public ArrayList<String> getAdminColumns() throws Exception {
        Set<String> admins = Admin.getColumns();

        return new ArrayList<>(admins);
    }

    public HashMap<String, String> getAdminRow(Integer personId) throws Exception {
        Admin admin = Admin.find(personId);

        return admin != null ? admin.toMap() : null;
    }

    public Integer createAdmin(Integer personId) throws Exception {
        Admin admin = new Admin(personId);

        admin.save();

        return admin.getPersonId();
    }

    public Integer updateAdmin(Integer personId) throws Exception {
        Admin admin = Admin.find(personId);

        admin.setPersonId(personId);
        admin.save();

        return admin.getPersonId();
    }

    public boolean deleteAdmin(Integer adminId) throws Exception {
        Admin admin = Admin.find(adminId);

        admin.delete();

        return admin.getPersonId() == null;
    }
}
