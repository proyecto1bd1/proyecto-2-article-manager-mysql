/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Country;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Handles the communication between the user interface and the data access layer
 *
 * @author Diego Herrera
 */
public class CountryController {

    public CountryController() {
    }

    /**
     * Gets a list of all countries
     *
     * @return All the entries in countries as rows
     
     */
    public ArrayList<HashMap<String, String>> getCountryRows() throws Exception {

        ArrayList<HashMap<String, String>> countryRows = new ArrayList<>();

        ArrayList<Country> countries = Country.getAll();

        countries.forEach((Country country) -> {
            countryRows.add(country.toMap());
        });

        return countryRows;
    }

    /**
     * Get the columns of the country model
     *
     * @return A list of columns
     
     */
    public ArrayList<String> getCountryColumns() throws Exception {
        Set<String> columns = Country.getColumns();

        return new ArrayList<>(columns);
    }

    /**
     * Get a country as a row
     *
     * @param countryId The country to locate
     * @return The row representing the country
     
     */
    public HashMap<String, String> getCountryRow(Integer countryId) throws Exception {
        Country country = Country.find(countryId);

        return country.toMap();
    }

    /**
     * Stores a new country
     *
     * @param countryName The name of the country
     * @return The id of the created element
     
     */
    public Integer createCountry(String countryName) throws Exception {
        Country country = new Country(countryName);

        country.save();

        return country.getCountryId();
    }

    /**
     * Updates the value of a country
     *
     * @param countryId   The id of the country to update
     * @param countryName The new name of the country
     * @return The new name of the country
     
     */
    public String updateCountry(Integer countryId, String countryName) throws Exception {
        Country country = Country.find(countryId);

        country.setCountryName(countryName);
        country.save();

        return country.getCountryName();
    }

    /**
     * Deletes a country from the system
     *
     * @param countryId The country to delete
     * @return True if country was deleted, false otherwise
      Could fail if country has references attach
     */
    public boolean deleteCountry(Integer countryId) throws Exception {
        Country country = Country.find(countryId);

        country.delete();

        return country.getCountryId() == null;
    }
}
