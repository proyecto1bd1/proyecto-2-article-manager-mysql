/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Purchase extends AbstractModel {
    private Integer PurchaseId;
    private Integer PersonId;

    public Purchase() {
    }

    public Purchase(Integer PurchaseId, Integer PersonId) {
        this.PurchaseId = PurchaseId;
        this.PersonId = PersonId;
    }

    public Purchase(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setPurchaseId(Integer PurchaseId) {
        this.PurchaseId = PurchaseId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getPurchaseId() {
        return PurchaseId;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.PurchaseId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call updatepurchase(?, ?)}");
                sql.setInt(1, this.PurchaseId);
                sql.setInt(2, this.PersonId);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createpurchase(?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setInt(2, this.PersonId);
                sql.execute();
                this.PurchaseId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error: Unable to save purchase", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.PurchaseId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletepurchase(?)}");
            sql.setInt(1, this.PurchaseId);
            sql.executeQuery();

            this.PurchaseId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete purchase", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "Purchase{ PurchaseId: %d PersonId: %d }",
                PurchaseId,
                PersonId
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> purchaseMap = new HashMap<>();

        purchaseMap.put("purchaseId", PurchaseId.toString());
        purchaseMap.put("personId", PersonId.toString());

        return purchaseMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("purchaseId");
        columns.add("personId");

        return columns;
    }

    public static Purchase find(Integer id) throws Exception {
        Purchase purchase = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpurchase(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                purchase = new Purchase(
                        result.getInt("PurchaseId"),
                        result.getInt("PersonId")
                );
            }

            return purchase;
        } catch (SQLException e) {
            throw new Exception("Unable to get purchase");
        }
    }

    public static ArrayList<Purchase> getAll() throws Exception {
        ArrayList<Purchase> purchases = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getpurchase(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Purchase purchase = new Purchase(
                        result.getInt("PurchaseId"),
                        result.getInt("PersonId")
                );
                purchases.add(purchase);
            }

            return purchases;
        } catch (SQLException e) {
            throw new Exception("Unable to get purchases");
        }
    }
}
