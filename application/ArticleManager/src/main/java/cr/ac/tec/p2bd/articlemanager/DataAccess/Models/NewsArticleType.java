/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class NewsArticleType extends AbstractModel {
    private Integer TypeId;
    private String Description;

    public NewsArticleType() {
    }

    public NewsArticleType(Integer TypeId, String Description) {
        this.TypeId = TypeId;
        this.Description = Description;
    }

    public NewsArticleType(String Description) {
        this.Description = Description;
    }

    public void setTypeId(Integer TypeId) {
        this.TypeId = TypeId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getTypeId() {
        return TypeId;
    }

    public String getDescription() {
        return Description;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.TypeId != null) {
                CallableStatement sql = connection.prepareCall("{call updatenewsArticleType(?, ?)}");
                sql.setInt(1, this.TypeId);
                sql.setString(2, this.Description);
                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createnewsArticleType(?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.Description);
                sql.execute();
                this.TypeId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error: Unable to save newsArticleType", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.TypeId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteNewsArticleType(?)}");
            sql.setInt(1, this.TypeId);
            sql.executeQuery();
            this.TypeId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete newsArticleType");
        }
    }

    @Override
    public String toString() {
        return String.format(
                "NewsArticleType{ TypeId: %d, Description %s}",
                TypeId,
                Description
        );
    }


    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> newsArticleTypeMap = new HashMap<>();

        newsArticleTypeMap.put("TypeId", TypeId.toString());
        newsArticleTypeMap.put("description", Description);

        return newsArticleTypeMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("TypeId");
        columns.add("description");

        return columns;
    }

    public static NewsArticleType find(Integer id) throws Exception {
        NewsArticleType newsArticleType = null;
        ArrayList<NewsArticleType> newsArticleTypes;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getNewsArticleType(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            newsArticleTypes = buildList(result);

            if (!newsArticleTypes.isEmpty()) {
                newsArticleType = newsArticleTypes.get(0);
            }

            return newsArticleType;
        } catch (SQLException e) {
            throw new Exception("Unable to get newsArticleType", e);
        }
    }

    private static @NotNull ArrayList<NewsArticleType> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<NewsArticleType> newsArticleTypes = new ArrayList<>();

        while (result.next()) {
            NewsArticleType newsArticleType = new NewsArticleType(
                    result.getInt("TypeId"),
                    result.getString("Description")
            );

            newsArticleTypes.add(newsArticleType);
        }
        return newsArticleTypes;
    }

    public static @NotNull ArrayList<NewsArticleType> getAll() throws Exception {
        ArrayList<NewsArticleType> newsArticleTypes;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getNewsArticleType(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            newsArticleTypes = buildList(result);

            return newsArticleTypes;
        } catch (SQLException e) {
            throw new Exception("Unable to get newsArticleTypes");
        }
    }
}
