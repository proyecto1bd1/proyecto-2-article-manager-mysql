/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PurchaseXProduct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class PurchaseXProductController {

    public PurchaseXProductController() {
    }

    public ArrayList<HashMap<String, String>> getPurchaseXProductRows() throws Exception {

        ArrayList<HashMap<String, String>> purchaseXProductRows = new ArrayList<>();

        ArrayList<PurchaseXProduct> purchaseXProducts = PurchaseXProduct.getAll();

        purchaseXProducts.forEach((PurchaseXProduct purchaseXProduct) -> {
            purchaseXProductRows.add(purchaseXProduct.toMap());
        });

        return purchaseXProductRows;
    }

    public ArrayList<String> getPurchaseXProductColumns() throws Exception {
        Set<String> columns = PurchaseXProduct.getColumns();

        return new ArrayList<>(columns);
    }

    public HashMap<String, String> getPurchaseXProductRow(Integer purchaseId, Integer productId) throws Exception {
        PurchaseXProduct purchaseXProduct = PurchaseXProduct.find(purchaseId, productId);

        return purchaseXProduct.toMap();
    }

    public void createPurchaseXProduct(Integer PurchaseId, Integer ProductId) throws Exception {
        PurchaseXProduct purchaseXProduct = new PurchaseXProduct(PurchaseId, ProductId);

        purchaseXProduct.save();
    }

    public void updatePurchaseXProduct(Integer purchaseId, Integer productId, String countryName) throws Exception {
        PurchaseXProduct purchaseXProduct = PurchaseXProduct.find(purchaseId, productId);

        purchaseXProduct.setPurchaseId(purchaseId);
        purchaseXProduct.setProductId(productId);
        purchaseXProduct.save();
    }


    public boolean deletePurchaseXProduct(Integer purchaseId, Integer productId) throws Exception {
        PurchaseXProduct purchaseXProduct = PurchaseXProduct.find(purchaseId, productId);

        purchaseXProduct.delete();

        return purchaseXProduct.getPurchaseId() == null && purchaseXProduct.getProductId() == null;
    }


}
