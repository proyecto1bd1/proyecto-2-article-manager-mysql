/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class StatUniversityArticle {

    private String universityName;
    private Integer totalArticles;

    public StatUniversityArticle(){};

    public StatUniversityArticle(String universityName, Integer totalArticles) {
        this.universityName = universityName;
        this.totalArticles = totalArticles;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public void setTotalArticles(Integer totalArticles) {
        this.totalArticles = totalArticles;
    }

    public String getUniversityName() {
        return universityName;
    }

    public Integer getTotalArticles() {
        return totalArticles;
    }

    public static ArrayList<StatUniversityArticle> getAll() throws Exception {
        ArrayList<StatUniversityArticle> rows = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call statArticlesXuniversity}");
            ResultSet result = sql.executeQuery();

            while (result.next()) {
                StatUniversityArticle stat = new StatUniversityArticle(
                        result.getString("University"),
                        result.getInt("TotalArticles")
                );

                rows.add(stat);
            }

            return rows;
        } catch (SQLException e) {
            throw new Exception("Unable to get statistic", e);
        }
    }

    public HashMap<String, String> toMap() {

        HashMap<String, String> statMap = new HashMap<>();

        statMap.put("University", universityName);
        statMap.put("Total Articles", totalArticles.toString());

        return statMap;
    }
}
