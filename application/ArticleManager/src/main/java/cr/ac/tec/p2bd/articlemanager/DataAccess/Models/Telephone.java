/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class Telephone extends AbstractModel {

    private Integer telephoneId;
    private String phoneNumber;
    private Integer personId;

    public Telephone() {
    }

    public Telephone(Integer telephoneId, String phoneNumber, Integer personId) {
        this.telephoneId = telephoneId;
        this.phoneNumber = phoneNumber;
        this.personId = personId;
    }

    public Telephone(String phoneNumber, Integer personId) {
        this.phoneNumber = phoneNumber;
        this.personId = personId;
    }

    public void setTelephoneId(Integer telephoneId) {
        this.telephoneId = telephoneId;
    }

    public void setTelephoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getTelephoneId() {
        return telephoneId;
    }

    public String getTelephoneNumber() {
        return phoneNumber;
    }

    public Integer getPersonId() {
        return personId;
    }

    @Override
    public String toString() {
        return String.format(
                "Telephone{ telephoneId: %d phoneNumber: %s personId: %d}",
                telephoneId,
                phoneNumber,
                personId
        );
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.telephoneId != null) {
                CallableStatement sql = connection.prepareCall("{call updatetelephone(?, ?, ?)}");
                sql.setInt(1, this.telephoneId);
                sql.setString(2, this.phoneNumber);
                sql.setInt(3, this.personId);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{? = call createtelephone(?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, this.phoneNumber);
                sql.setInt(3, this.personId);
                sql.execute();

                this.telephoneId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error: Unable to save telephone", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.telephoneId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletetelephone(?)}");
            sql.setInt(1, this.telephoneId);
            sql.executeQuery();

            this.telephoneId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete telephone", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> telephoneMap = new HashMap<>();

        telephoneMap.put("telephoneId", telephoneId.toString());
        telephoneMap.put("phoneNumber", phoneNumber);
        telephoneMap.put("personId", personId.toString());

        return telephoneMap;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("telephoneId");
        columns.add("phoneNumber");
        columns.add("personId");

        return columns;
    }

    public static Telephone find(Integer id) throws Exception {
        Telephone telephone = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call gettelephone(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                telephone = new Telephone(
                        result.getInt("TelephoneId"),
                        result.getString("PhoneNumber"),
                        result.getInt("PersonId")
                );
            }

            return telephone;
        } catch (SQLException e) {
            throw new Exception("Unable to get telephone", e);
        }
    }

    public static ArrayList<Telephone> getAll() throws Exception {
        ArrayList<Telephone> telephones = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call gettelephone(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                Telephone telephone = new Telephone(
                        result.getInt("TelephoneId"),
                        result.getString("PhoneNumber"),
                        result.getInt("NewsPaperId")
                );
                telephones.add(telephone);
            }
            return telephones;
        } catch (SQLException e) {
            throw new Exception("Unable to get telephones", e);
        }
    }
}
