/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class StatAgeGenderAuthor{

    private String ageRange;

    private String gender;

    private Integer totalAuthors;

    private Integer universityId;

    private Integer genderId;

    private Integer newsArticleTypeId;



    public StatAgeGenderAuthor(){};

    public StatAgeGenderAuthor(Integer universityId, Integer genderId, Integer newsArticleTypeId) {
        this.universityId = universityId;
        this.genderId = genderId;
        this.newsArticleTypeId = newsArticleTypeId;
    }

    public StatAgeGenderAuthor(String ageRange, String gender, Integer totalAuthors) {
        this.ageRange = ageRange;
        this.gender = gender;
        this.totalAuthors = totalAuthors;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getTotalAuthors() {
        return totalAuthors;
    }

    public void setTotalAuthors(Integer totalAuthors) {
        this.totalAuthors = totalAuthors;
    }

    public Integer getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Integer universityId) {
        this.universityId = universityId;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public Integer getNewsArticleTypeId() {
        return newsArticleTypeId;
    }

    public void setNewsArticleTypeId(Integer newsArticleTypeId) {
        this.newsArticleTypeId = newsArticleTypeId;
    }

    public ArrayList<StatAgeGenderAuthor> getAll() throws Exception {
        ArrayList<StatAgeGenderAuthor> rows = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call statAuthorsXageRange(?,?,?)}");
            if(this.universityId != null){
                sql.setInt(1, this.universityId);
            }
            else{
                sql.setNull(1, Types.BIGINT);
            }
            if(this.genderId != null){
                sql.setInt(2, this.genderId);
            }
            else{
                sql.setNull(2, Types.BIGINT);
            }
            if(this.newsArticleTypeId != null){
                sql.setInt(3, this.newsArticleTypeId);
            }
            else{
                sql.setNull(3, Types.BIGINT);
            }
            ResultSet result = sql.executeQuery();

            while (result.next()) {
                StatAgeGenderAuthor stat = new StatAgeGenderAuthor(
                        result.getString("AgeRange"),
                        result.getString("Gender"),
                        result.getInt("TotalAuthors")
                );
                rows.add(stat);
            }
            return rows;
        } catch (SQLException e) {
            throw new Exception("Unable to get statistic", e);
        }
    }

    public HashMap<String, String> toMap() {

        HashMap<String, String> statMap = new HashMap<>();

        statMap.put("Age Range", ageRange);
        statMap.put("Gender", gender);
        statMap.put("Total Authors", totalAuthors.toString());

        return statMap;
    }
}
