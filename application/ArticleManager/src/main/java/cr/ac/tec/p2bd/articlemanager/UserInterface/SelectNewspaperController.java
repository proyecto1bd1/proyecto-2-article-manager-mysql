/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DigitalNewsPaperController;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectNewspaperController {

    private final DigitalNewsPaperController digitalNewsPaperController = new DigitalNewsPaperController();
    public ChoiceBox<Choice> cbNewspapers;
    public AnchorPane anchorDialog;

    private Integer selectedNewsPaperId = 0;

    public void initialize() {
        cbNewspapers.setOnAction(this::onNewsPaperSelected);
    }

    public void setNewsPapersIds(ArrayList<Integer> newsPapersIds) {
        ArrayList<HashMap<String, String>> newsPapers = new ArrayList<>();
        for (var newsPaperId: newsPapersIds) {
            try {
                newsPapers.add(
                        digitalNewsPaperController.getDigitalNewsPaperRow(newsPaperId)
                );
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Failed to get newspapers");
                alert.setHeaderText("We where unable to get the list of newsPapers");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }

        ArrayList<Choice> newsPaperChoices = new ArrayList<>();

        for (var newsPaper : newsPapers) {
            newsPaperChoices.add(
                    new Choice(
                            Integer.valueOf(newsPaper.get("NewsPaperId")),
                            newsPaper.get("NewsPaperName")
                    )
            );
        }
        cbNewspapers.getItems().clear();
        cbNewspapers.getItems().addAll(newsPaperChoices);
    }

    public void onNewsPaperSelected(ActionEvent event) {
        selectedNewsPaperId = cbNewspapers.getValue().id;
    }

    public Integer getSelectedNewsPaperId() {
        return selectedNewsPaperId;
    }

    public void onConfirm(ActionEvent event) {

        if (selectedNewsPaperId == 0) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("News paper not selected");
            alert.setHeaderText("We detected that you didn't choose a newspaper");
            alert.setContentText("Please select a newspaper");
            alert.show();
            return;
        }

        Stage stage = (Stage) anchorDialog.getScene().getWindow();
        stage.close();
    }
}
