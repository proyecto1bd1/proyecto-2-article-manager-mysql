/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */


public class NewsArticle {
    private Integer ArticleId;
    private String Title;
    private String Content;
    private Date PublicationDate;
    private byte[] Photo;
    private Integer AuthorId;
    private Integer ArticleTypeId;
    private Integer NewsPaperId;
    private Integer CommitteeIdAprov;

    public NewsArticle() {
    }

    public NewsArticle(Integer ArticleId,
                       String Title,
                       String content,
                       Date PublicationDate,
                       byte[] Photo,
                       Integer AuthorId,
                       Integer ArticleTypeId,
                       Integer NewsPaperId,
                       Integer CommitteeIdAprov) {
        this.ArticleId = ArticleId;
        this.Title = Title;
        this.Content = content;
        this.PublicationDate = PublicationDate;
        this.Photo = Photo;
        this.AuthorId = AuthorId;
        this.ArticleTypeId = ArticleTypeId;
        this.NewsPaperId = NewsPaperId;
        this.CommitteeIdAprov = CommitteeIdAprov;
    }

    public NewsArticle(String Title,
                       String content,
                       Date PublicationDate,
                       byte[] Photo,
                       Integer AuthorId,
                       Integer ArticleTypeId,
                       Integer NewsPaperId,
                       Integer CommitteeIdAprov) {
        this.Title = Title;
        this.Content = content;
        this.PublicationDate = PublicationDate;
        this.Photo = Photo;
        this.AuthorId = AuthorId;
        this.ArticleTypeId = ArticleTypeId;
        this.NewsPaperId = NewsPaperId;
        this.CommitteeIdAprov = CommitteeIdAprov;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setContent(String Text) {
        this.Content = Text;
    }

    public void setPublicationDate(Date PublicationDate) {
        this.PublicationDate = PublicationDate;
    }

    public void setPhoto(byte[] Photo) {
        this.Photo = Photo;
    }

    public void setAuthorId(Integer AuthorId) {
        this.AuthorId = AuthorId;
    }

    public void setArticleTypeId(Integer ArticleTypeId) {
        this.ArticleTypeId = ArticleTypeId;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setCommitteeIdAprov(Integer CommitteeIdAprov) {
        this.CommitteeIdAprov = CommitteeIdAprov;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public String getTitle() {
        return Title;
    }

    public String getContent() {
        return Content;
    }

    public Date getPublicationDate() {
        return PublicationDate;
    }

    public byte[] getPhoto() {
        return Photo;
    }

    public Integer getAuthorId() {
        return AuthorId;
    }

    public Integer getArticleTypeId() {
        return ArticleTypeId;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public Integer getCommitteeIdAprov() {
        return CommitteeIdAprov;
    }


    public void save() throws Exception {
        ByteArrayInputStream photoBytes = Photo != null ? new ByteArrayInputStream(Photo) : null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (ArticleId != null) {
                CallableStatement sql = connection.prepareCall("{call updateNewsarticle(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
                sql.setInt(1, ArticleId);
                sql.setString(2, Title);
                sql.setString(3, Content);
                sql.setDate(4, PublicationDate);
                sql.setBlob(5, photoBytes);
                sql.setInt(6, AuthorId);
                sql.setInt(7, ArticleTypeId);
                sql.setInt(8, NewsPaperId);

                if (CommitteeIdAprov != null) {
                    sql.setInt(9, CommitteeIdAprov);
                } else {
                    sql.setNull(9, Types.BIGINT);
                }

                sql.executeQuery();
            } else {
                CallableStatement sql = connection.prepareCall("{? = call createNewsArticle(?, ?, ?, ?, ?, ?, ?, ?)}");
                sql.registerOutParameter(1, Types.BIGINT);
                sql.setString(2, Title);
                sql.setString(3, Content);
                sql.setDate(4, PublicationDate);
                sql.setBlob(5, photoBytes);
                sql.setInt(6, AuthorId);
                sql.setInt(7, ArticleTypeId);
                sql.setInt(8, NewsPaperId);

                if (CommitteeIdAprov != null) {
                    sql.setInt(9, CommitteeIdAprov);
                } else {
                    sql.setNull(9, Types.BIGINT);

                }
                sql.execute();

                ArticleId = sql.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("Error saving article", e);
        }
    }

    public void approveArticle() throws Exception {
        if (CommitteeIdAprov == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (ArticleId != null) {
                CallableStatement sql = connection.prepareCall("{call approveNewsarticle(?, ?)}");
                sql.setInt(1, ArticleId);
                sql.setInt(2, CommitteeIdAprov);

                sql.executeQuery();
            }
        } catch (SQLException e) {
            throw new Exception("Error approving article", e);
        }
    }

    public void delete() throws Exception {
        if (ArticleId == null) {
            return;
        }

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteNewsarticle(?)}");
            sql.setInt(1, ArticleId);
            sql.executeQuery();

            ArticleId = null;
        } catch (SQLException e) {
            throw new Exception("Error deleting newsArticle", e);
        }
    }

    @Override
    public String toString() {
        return
                String.format(
                        "NewsArticle{ ArticleId: %d Title: %s Content: %s"
                                + "PublicationDate: %s AuthorId: %d "
                                + "ArticleTypeId: %d NewsPaperId %d CommitteeIdAprov %d }",
                        ArticleId,
                        Title,
                        Content,
                        PublicationDate.toString(),
                        AuthorId,
                        ArticleTypeId,
                        NewsPaperId,
                        CommitteeIdAprov
                );
    }

    public HashMap<String, String> toMap() {
        HashMap<String, String> articleMap = new HashMap<>();

        articleMap.put("ArticleId", ArticleId.toString());
        articleMap.put("Title", Title);
        articleMap.put("Content", Content);
        articleMap.put("PublicationDate", PublicationDate.toString());
        articleMap.put("AuthorId", AuthorId.toString());
        articleMap.put("ArticleTypeId", ArticleTypeId.toString());
        articleMap.put("Photo", Photo != null ? Base64.getEncoder().encodeToString(Photo) : "");
        articleMap.put("NewsPaperId", NewsPaperId.toString());
        articleMap.put("CommitteeIdApprov", CommitteeIdAprov != null ? CommitteeIdAprov.toString(): "");

        return articleMap;
    }

    private static @NotNull ArrayList<NewsArticle> buildList(@NotNull ResultSet result) throws SQLException, IOException {
        ArrayList<NewsArticle> articles = new ArrayList<>();

        while (result.next()) {
            byte[] photo = (result.getBlob("Photo") != null) ?
                    result.getBlob("Photo").getBinaryStream().readAllBytes() :
                    null;
            NewsArticle article = new NewsArticle(
                    result.getInt("ArticleId"),
                    result.getString("Title"),
                    result.getString("content"),
                    result.getDate("PublicationDate"),
                    photo,
                    result.getInt("AuthorId"),
                    result.getInt("ArticleTypeId"),
                    result.getInt("NewsPaperId"),
                    result.getInt("CommitteeIdApprov")
            );

            articles.add(article);
        }

        return articles;
    }

    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("articleId");
        columns.add("title");
        columns.add("content");
        columns.add("publicationDate");
        columns.add("photo");
        columns.add("authorId");
        columns.add("articleTypeId");
        columns.add("committeeIdAprov");

        return columns;
    }

    public static NewsArticle find(Integer id) throws Exception {
        NewsArticle article = null;
        ArrayList<NewsArticle> articles;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getNewsarticle(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            articles = buildList(result);

            if (!articles.isEmpty()) {
                article = articles.get(0);
            }

            return article;
        } catch (SQLException e) {
            throw new Exception("Unable to get Article", e);
        }
    }

    public static @NotNull ArrayList<NewsArticle> getAll() throws Exception {
        ArrayList<NewsArticle> articles;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getNewsarticle(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            articles = buildList(result);

            return articles;
        } catch (SQLException e) {
            throw new Exception("Unable to get articles", e);
        }
    }

    public static @NotNull ArrayList<NewsArticle> getAllLatest(Integer pageNumber, Integer newsPaperId) throws Exception {
        ArrayList<NewsArticle> articles;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getLatestNewsPaginated(?, ?)}");
            sql.setInt(1, pageNumber);
            sql.setInt(2, newsPaperId);
            ResultSet result = sql.executeQuery();

            articles = buildList(result);

            return articles;
        } catch (SQLException e) {
            throw new Exception("Unable to get articles", e);
        }
    }

    public static @NotNull Integer getLatestNewsPages(Integer newsPaperId) throws Exception {
        int pages = 0;
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;

            CallableStatement sql = connection.prepareCall("{? = call getLatestNewsPages(?)}");
            sql.registerOutParameter(1, Types.INTEGER);
            sql.setInt(2, newsPaperId);
            sql.execute();

            pages = sql.getInt(1);

            return pages;
        } catch (SQLException e) {
            throw new Exception("Unable to get amount of pages", e);
        }
    }

    public static @NotNull ArrayList<NewsArticle> getAllPending(Integer pageNumber, Integer newsPaperId) throws Exception {
        ArrayList<NewsArticle> articles;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getNewsArticlePending(?, ?)}");
            sql.setInt(1, pageNumber);
            sql.setInt(2, newsPaperId);
            ResultSet result = sql.executeQuery();

            articles = buildList(result);

            return articles;
        } catch (SQLException e) {
            throw new Exception("Unable to get articles", e);
        }
    }

    public static @NotNull Integer getPendingNewsPages(Integer newsPaperId) throws Exception {
        int pages = 0;
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;

            CallableStatement sql = connection.prepareCall("{? = call getNewsArticlePendingPages(?)}");
            sql.registerOutParameter(1, Types.INTEGER);
            sql.setInt(2, newsPaperId);
            sql.execute();

            pages = sql.getInt(1);

            return pages;
        } catch (SQLException e) {
            throw new Exception("Unable to get amount of pages", e);
        }
    }
}
