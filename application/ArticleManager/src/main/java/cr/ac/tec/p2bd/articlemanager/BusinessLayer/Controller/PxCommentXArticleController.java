/*
 * The MIT License
 *
 * Copyright 2022 .
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;


import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.PxCommentXArticle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class PxCommentXArticleController {

    public PxCommentXArticleController() {
    }


    public ArrayList<HashMap<String, String>> getPxCommentXArticleRows() throws Exception {
        ArrayList<HashMap<String, String>> PxCommentXArticleRows = new ArrayList<>();

        ArrayList<PxCommentXArticle> comments = PxCommentXArticle.getAll();

        comments.forEach((PxCommentXArticle pxcommentxarticle) -> {
            PxCommentXArticleRows.add(pxcommentxarticle.toMap());
        });

        return PxCommentXArticleRows;

    }

    public ArrayList<HashMap<String, String>> getPxCommentXArticleRows(Integer articleId) throws Exception {
        ArrayList<HashMap<String, String>> PxCommentXArticleRows = new ArrayList<>();

        ArrayList<PxCommentXArticle> comments = PxCommentXArticle.getAllInArticle(articleId);

        comments.forEach((PxCommentXArticle pxcommentxarticle) -> {
            PxCommentXArticleRows.add(pxcommentxarticle.toMap());
        });

        return PxCommentXArticleRows;

    }

    public ArrayList<String> getPxCommentXArticleColumns() throws Exception {
        Set<String> pXCommentsXArticle = PxCommentXArticle.getColumns();

        return new ArrayList<>(pXCommentsXArticle);
    }

    public HashMap<String, String> getPxCommentXArticleRow(Integer articleId, Integer personId) throws Exception {
        PxCommentXArticle pxcommentxarticle = PxCommentXArticle.find(articleId, personId);

        return pxcommentxarticle.toMap();
    }

    public Integer createPxCommentXArticle(Integer personId, Integer articleId, String comment) throws Exception {
        PxCommentXArticle pxcommentxarticle = new PxCommentXArticle(personId, articleId, comment);

        pxcommentxarticle.save();

        return pxcommentxarticle.getPersonId();
    }

    public Integer updatePxCommentXArticle(Integer personId, Integer articleId, String comment) throws Exception {
        PxCommentXArticle pxcommentxarticle = PxCommentXArticle.find(articleId, personId);

        pxcommentxarticle.setComments(comment);
        pxcommentxarticle.save();

        return pxcommentxarticle.getPersonId();
    }

    public boolean deletePxCommentXArticle(Integer personId, Integer articleId) throws Exception {
        PxCommentXArticle pxcommentxarticle = PxCommentXArticle.find(articleId, personId);

        pxcommentxarticle.delete();

        return pxcommentxarticle.getPersonId() == null;
    }
}
