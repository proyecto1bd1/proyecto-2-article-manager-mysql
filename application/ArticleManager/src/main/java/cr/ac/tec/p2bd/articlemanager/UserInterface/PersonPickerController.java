/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonController;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;

public class PersonPickerController {
    private final PersonController personController = new PersonController();
    public ChoiceBox<Choice> cbPerson;
    public AnchorPane anchorPane;
    private HashMap<String, String> person;

    public void initialize() {
        cbPerson.getItems().addAll(ChoiceBuilder.getPersonList());
    }

    public void setChoiceList(ArrayList<Choice> choices) {
        cbPerson.getItems().clear();
        cbPerson.getItems().addAll(choices);
    }
    public void onConfirm(ActionEvent event) {

        try {
            if (cbPerson.getValue() != null) {
                person = personController.getPersonRow(cbPerson.getValue().id);
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed to fetch user data");
            alert.setHeaderText("We failed at fetching user data");
            alert.setContentText(e.getMessage());
            alert.show();
        }

        Stage stage = (Stage) anchorPane.getScene().getWindow();
        stage.close();
    }

    public HashMap<String, String> getPerson() {
        return person;
    }
}
