/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.DigitalNewsPaperController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.NewsArticleController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PersonXCommitteeController;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Pagination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReviewNewsPaginatorController {

    public Pagination newsPaginator;

    private final NewsArticleController newsArticleController = new NewsArticleController();
    private final DigitalNewsPaperController digitalNewsPaperController = new DigitalNewsPaperController();
    private final PersonXCommitteeController personXCommitteeController = new PersonXCommitteeController();
    private final PersonController personController = new PersonController();

    private Integer newsPages = 0;

    public void initialize() {
        try {
            HashMap<String, String> newspaper = digitalNewsPaperController.getDigitalNewsPaperRow(
                    SessionManager.getInstance().getSelectedNewsPaper()
            );

            ArrayList<HashMap<String, String>> committees = personXCommitteeController.getPersonXCommitteeRows(
                    Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId"))
            );

            AtomicBoolean committeeFound = new AtomicBoolean(false);

            committees.forEach(committee -> {
                if (committee.get("committeeId").equals(newspaper.get("CommitteeId"))) {
                    committeeFound.set(true);
                }
            });

            if (committeeFound.get()) {
                newsPages = newsArticleController.pendingNewsPages(
                        SessionManager.getInstance().getSelectedNewsPaper()
                );
                newsPaginator.setPageCount(newsPages);

                newsPaginator.setPageFactory(pageIndex -> {
                    if (pageIndex <= newsPages) {
                        return onPageChanged(pageIndex);
                    } else {
                        return null;
                    }
                });
            }

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    public VBox onPageChanged(int pageIndex) {
        try {
            VBox box = new VBox();

            ArrayList<HashMap<String, String>> newsRows = newsArticleController.getArticlesPending(
                    SessionManager.getInstance().getSelectedNewsPaper(),
                    pageIndex
            );

            for (HashMap<String, String> newsRow : newsRows) {

                HashMap<String, String> author = personController.getPersonRow(
                        Integer.valueOf(newsRow.get("AuthorId"))
                );

                FXMLLoader loader = new FXMLLoader(getClass().getResource("news-approval-bubble-view.fxml"));
                AnchorPane newsBubble = loader.load();
                NewsApprovalBubbleController newsBubbleController = loader.getController();
                newsBubbleController.fillNewsData(
                        Integer.parseInt(newsRow.get("ArticleId")),
                        newsRow.get("Title"),
                        "%s %s %s".formatted(
                                author.get("Name"),
                                author.get("FirstLastName"),
                                author.get("SecondLastName")
                        )
                );
                box.getChildren().add(newsBubble);
            }

            return box;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while fetching data");
            alert.setContentText(e.getMessage());
            alert.show();
            return null;
        }
    }
}
