/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Daniel G
 */
public class PxFavoriteXArticle extends AbstractModel {
    private Integer PersonId;
    private Integer ArticleId;
    private Integer deleted; // 1 if deleted, 0 if not deleted

    public PxFavoriteXArticle() {
    }

    public PxFavoriteXArticle(Integer PersonId, Integer ArticleId, Integer deleted) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.deleted = deleted;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public Integer isDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return String.format(
                "PxFavoriteXArticle{ PersonId: %d ArticleId: %d Deleted: %b }",
                PersonId,
                ArticleId,
                deleted
        );
    }

    @Override
    public void save() throws Exception {
        PxFavoriteXArticle favorite = PxFavoriteXArticle.find(this.PersonId, this.ArticleId);

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql;
            if (favorite != null) {
                sql = connection.prepareCall("{call updatePxfavoritexarticle(?, ?)}");
            } else {
                sql = connection.prepareCall("{call createPxFavoriteXArticle(?, ?)}");
            }
            sql.setInt(1, this.PersonId);
            sql.setInt(2, this.ArticleId);
            sql.executeQuery();
        } catch (SQLException e) {
            throw new Exception("Error saving favorite", e);
        }
    }

    @Override
    public void delete() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deletePxFavoriteXArticle(?, ?)}");
            sql.setInt(1, this.PersonId);
            sql.setInt(2, this.ArticleId);
            sql.executeQuery();
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete Favorite", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> PxFavoriteXArticleMap = new HashMap<>();

        PxFavoriteXArticleMap.put("personId", PersonId.toString());
        PxFavoriteXArticleMap.put("articleId", ArticleId.toString());
        PxFavoriteXArticleMap.put("deleted", deleted.toString());

        return PxFavoriteXArticleMap;
    }

    public static Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();
        columns.add("personId");
        columns.add("articleId");
        return columns;
    }

    public static PxFavoriteXArticle find(Integer personId, Integer articleId) throws Exception {
        PxFavoriteXArticle pxFavoriteXArticle = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPxFavoriteXArticle(?, ?)}");
            sql.setInt(1, personId);
            sql.setInt(2, articleId);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                pxFavoriteXArticle = new PxFavoriteXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getInt("deleted")
                );
            }

            return pxFavoriteXArticle;
        } catch (SQLException e) {
            throw new Exception("Unable to get favorite", e);
        }
    }

    public static ArrayList<PxFavoriteXArticle> getAll() throws Exception {
        ArrayList<PxFavoriteXArticle> pxFavoriteXArticles = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getPxFavoriteXArticle(?, ?)}");
            sql.setNull(1, Types.BIGINT);
            sql.setNull(2, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                PxFavoriteXArticle pxFavoriteXArticle = new PxFavoriteXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getInt("deleted")
                );
                pxFavoriteXArticles.add(pxFavoriteXArticle);
            }

            return pxFavoriteXArticles;
        } catch (SQLException e) {
            throw new Exception("Unable to get favorite: " + e);
        }
    }
}
