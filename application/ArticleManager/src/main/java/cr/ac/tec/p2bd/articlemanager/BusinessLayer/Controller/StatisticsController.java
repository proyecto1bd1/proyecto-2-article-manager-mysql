/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.*;

import java.util.ArrayList;
import java.util.HashMap;

public class StatisticsController {



    public ArrayList<HashMap<String, String>> getStatRows(Integer statNumber, Integer universityId, Integer genderId, Integer newsArticleTypeId, Integer topN) throws Exception {
        /*
         * Possible values for statNumber
         * 1: Total articles for each university
         * 2: Total authors for each university
         * 3: Total articles for each author
         * 4: Top n authors with the most published articles
         * 5: Average article review for each author
         * 6: Total authors for each age range and gender
         * 7: Most purchased products and total purchases for each one
         * */

        switch(statNumber){
            case 1:
                ArrayList<StatUniversityArticle> rows = StatUniversityArticle.getAll();
                ArrayList<HashMap<String, String>> statRows = new ArrayList<>();
                rows.forEach((StatUniversityArticle stat) -> {
                    statRows.add(stat.toMap());
                });
                return statRows;
            case 2:
                ArrayList<StatUniversityAuthor> rows2 = StatUniversityAuthor.getAll();
                ArrayList<HashMap<String, String>> statRows2 = new ArrayList<>();
                rows2.forEach((StatUniversityAuthor stat) -> {
                    statRows2.add(stat.toMap());
                });
                return statRows2;
            case 3:
                ArrayList<StatAuthorArticle> rows3 = StatAuthorArticle.getAll();
                ArrayList<HashMap<String, String>> statRows3 = new ArrayList<>();
                rows3.forEach((StatAuthorArticle stat) -> {
                    statRows3.add(stat.toMap());
                });
                return statRows3;
            case 4:
                StatTopAuthorArticle statInfo = new StatTopAuthorArticle(universityId, genderId, newsArticleTypeId, topN);
                ArrayList<StatTopAuthorArticle> rows4 = statInfo.getAll();
                ArrayList<HashMap<String, String>> statRows4 = new ArrayList<>();
                rows4.forEach((StatTopAuthorArticle stat) -> {
                    statRows4.add(stat.toMap());
                });
                return statRows4;
            case 5:
                ArrayList<StatAuthorReview> rows5 = StatAuthorReview.getAll();
                ArrayList<HashMap<String, String>> statRows5 = new ArrayList<>();
                rows5.forEach((StatAuthorReview stat) -> {
                    statRows5.add(stat.toMap());
                });
                return statRows5;
            case 6:
                StatAgeGenderAuthor statInfo2 = new StatAgeGenderAuthor(universityId, genderId, newsArticleTypeId);
                ArrayList<StatAgeGenderAuthor> rows6 = statInfo2.getAll();
                ArrayList<HashMap<String, String>> statRows6 = new ArrayList<>();
                rows6.forEach((StatAgeGenderAuthor stat) -> {
                    statRows6.add(stat.toMap());
                });
                return statRows6;
            case 7:
                StatProductPurchases statInfo3 = new StatProductPurchases(topN);
                ArrayList<StatProductPurchases> rows7 = statInfo3.getAll();
                ArrayList<HashMap<String, String>> statRows7 = new ArrayList<>();
                rows7.forEach((StatProductPurchases stat) -> {
                    statRows7.add(stat.toMap());
                });
                return statRows7;
            default:
                return null;
        }
    }
}
