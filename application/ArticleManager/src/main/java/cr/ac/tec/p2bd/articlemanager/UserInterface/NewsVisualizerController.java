/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PxFavoriteXArticleController;
import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.PxReviewsXArticleController;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Objects;

public class NewsVisualizerController {
    private final PxFavoriteXArticleController pxFavoriteXArticleController = new PxFavoriteXArticleController();
    private final PxReviewsXArticleController pxReviewsXArticleController = new PxReviewsXArticleController();
    public Label lblTitle;
    public ImageView imgNewsImage;
    public Label lblContent;
    public Spinner<Integer> spnScore;
    public CheckBox chkFavorite;
    public Button btnSubmit;

    private HashMap<String, String> favorite;
    private HashMap<String, String> review;
    private HashMap<String, String> news;

    public void initialize() {
        spnScore.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 5)
        );
    }

    public void setNews(HashMap<String, String> news) {
        this.news = news;

        lblTitle.setText(news.get("Title"));
        lblContent.setText(news.get("Content"));

        if (!SessionManager.getInstance().isGuest()) {
            try {
                favorite = pxFavoriteXArticleController.getPxFavoriteXArticleRow(
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        Integer.valueOf(news.get("ArticleId"))
                );
                review = pxReviewsXArticleController.getPxReviewsXArticleRow(
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        Integer.valueOf(news.get("ArticleId"))
                );

                if (review != null) {
                    spnScore.getValueFactory().setValue(
                            Integer.valueOf(review.get("nStars"))
                    );
                }

                if (favorite != null) {
                    chkFavorite.setSelected(Objects.equals(favorite.get("deleted"), "0"));
                }

            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unexpected error");
                alert.setHeaderText("An error occurred fetching news data");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        } else {
            chkFavorite.setDisable(true);
            spnScore.setDisable(true);
            btnSubmit.setDisable(true);
        }


        if (!news.get("Photo").isEmpty()) {
            byte[] imageStream = Base64.getDecoder().decode(news.get("Photo"));
            InputStream imageData = new ByteArrayInputStream(imageStream);
            Image image = new Image(imageData);
            imgNewsImage.setImage(image);

            try {
                File imageNews = File.createTempFile("news_%s".formatted(news.get("ArticleId")), ".jpg");
                FileUtils.writeByteArrayToFile(imageNews, imageStream);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unexpected error");
                alert.setHeaderText("An error occurred while building the image");
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
    }
    
    public void onSetScore(ActionEvent event) {
        try {
            if (review != null) {
                pxReviewsXArticleController.updatePxReviewsXArticle(
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        Integer.valueOf(news.get("ArticleId")),
                        spnScore.getValue()
                );
            } else {
                pxReviewsXArticleController.createPxReviewsXArticle(
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        Integer.valueOf(news.get("ArticleId")),
                        spnScore.getValue()
                );

                review = pxReviewsXArticleController.getPxReviewsXArticleRow(
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        Integer.valueOf(news.get("ArticleId"))
                );
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while marking as favorite");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }
    
    public void onCheckFavorite(ActionEvent event) {
        boolean checkFavorite = chkFavorite.isSelected();

        try {
            if (checkFavorite) {
                if (favorite != null) {
                    pxFavoriteXArticleController.updatePxFavoriteXArticle(
                            Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                            Integer.valueOf(news.get("ArticleId"))
                    );
                } else {
                    pxFavoriteXArticleController.createPxFavoriteXArticle(
                            Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                            Integer.valueOf(news.get("ArticleId"))
                    );

                    favorite = pxFavoriteXArticleController.getPxFavoriteXArticleRow(
                            Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                            Integer.valueOf(news.get("ArticleId"))
                    );
                }
            } else {
                pxFavoriteXArticleController.deletePxFavoriteXArticle(
                        Integer.valueOf(SessionManager.getInstance().getPersonData().get("PersonId")),
                        Integer.valueOf(news.get("ArticleId"))
                );
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unexpected error");
            alert.setHeaderText("An error occurred while marking as favorite");
            alert.setContentText(e.getMessage());
            alert.show();
        }

    }
}
