/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller.StatisticsController;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class Statistics {

    private final StatisticsController statController = new StatisticsController();

    @FXML
    public PieChart chartStatA;
    @FXML
    public PieChart chartStatB;

    @FXML
    public BarChart<String, Integer> chartStatC;

    @FXML
    public BarChart<String, Integer> chartStatD;

    @FXML
    private Spinner<Integer> spinnerDTopN;

    @FXML
    private ChoiceBox<Choice> cbUniversityD;

    @FXML
    private ChoiceBox<Choice> cbGenderD;

    @FXML
    private ChoiceBox<Choice> cbArticleTypeD;

    @FXML
    public BarChart<String, Integer> chartStatE;

    @FXML
    public BarChart<String, Integer> chartStatF;

    @FXML
    private ChoiceBox<Choice> cbUniversityF;

    @FXML
    private ChoiceBox<Choice> cbGenderF;

    @FXML
    private ChoiceBox<Choice> cbArticleTypeF;

    @FXML
    public BarChart<String, Integer> chartStatG;

    @FXML
    private Spinner<Integer> spinnerGTopN;


    @FXML
    public void initialize() throws Exception {
        ArrayList<HashMap<String, String>> statA = statController.getStatRows(1,null,null,null,5);
        ArrayList<HashMap<String, String>> statB = statController.getStatRows(2,null,null,null,5);
        ArrayList<HashMap<String, String>> statC = statController.getStatRows(3,null,null,null,5);
        ArrayList<HashMap<String, String>> statD = statController.getStatRows(4,null,null,null,5);
        ArrayList<HashMap<String, String>> statE = statController.getStatRows(5,null,null,null,5);
        ArrayList<HashMap<String, String>> statF = statController.getStatRows(6,null,null,null,5);
        ArrayList<HashMap<String, String>> statG = statController.getStatRows(7,null,null,null,5);
        ObservableList<PieChart.Data> pieChartDataA = FXCollections.observableArrayList();
        for (HashMap<String, String> stat: statA){
            pieChartDataA.add(new PieChart.Data(stat.get("University"), Integer.parseInt(stat.get("Total Articles"))));
        }
        pieChartDataA.forEach(data ->
                data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " total articles: ", data.pieValueProperty()
                        )
                )
        );
        chartStatA.getData().addAll(pieChartDataA);

        ObservableList<PieChart.Data> pieChartDataB = FXCollections.observableArrayList();
        for (HashMap<String, String> stat: statB){
            pieChartDataB.add(new PieChart.Data(stat.get("University"), Integer.parseInt(stat.get("Total Authors"))));
        }
        pieChartDataB.forEach(data ->
                data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " total authors: ", data.pieValueProperty()
                        )
                )
        );
        chartStatB.getData().addAll(pieChartDataB);

        XYChart.Series<String, Integer> seriesC = new XYChart.Series<>();
        seriesC.setName("Articles");
        for (HashMap<String, String> stat: statC){
            seriesC.getData().add(new XYChart.Data<>(stat.get("Author"), Integer.valueOf(stat.get("Total Articles"))));
        }

        chartStatC.getData().addAll(seriesC);

        XYChart.Series<String, Integer> seriesD = new XYChart.Series<>();
        seriesD.setName("Articles");
        for (HashMap<String, String> stat: statD){
            seriesD.getData().add(new XYChart.Data<>(stat.get("Author"), Integer.valueOf(stat.get("Total Articles"))));
        }

        chartStatD.getData().addAll(seriesD);

        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,10000);
        valueFactory.setValue(5);
        spinnerDTopN.setValueFactory(valueFactory);
        spinnerDTopN.valueProperty().addListener(new ChangeListener<Integer>(){
            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
                refreshStatD();
            }
        });

        cbUniversityD.getItems().addAll(ChoiceBuilder.getUniversityList());
        cbGenderD.getItems().addAll(ChoiceBuilder.getGenderList());
        cbArticleTypeD.getItems().addAll(ChoiceBuilder.getNewsArticleTypeList());

        cbUniversityD.setOnAction(this::refreshFilters);
        cbGenderD.setOnAction(this::refreshFilters);
        cbArticleTypeD.setOnAction(this::refreshFilters);

        XYChart.Series<String, Integer> seriesE = new XYChart.Series<>();
        seriesD.setName("Review");
        for (HashMap<String, String> stat: statE){
            seriesE.getData().add(new XYChart.Data<>(stat.get("Author"), Integer.valueOf(stat.get("Average Review"))));
        }

        chartStatE.getData().addAll(seriesE);

        XYChart.Series<String, Integer> seriesF = new XYChart.Series<>();
        seriesD.setName("Articles");
        for (HashMap<String, String> stat: statF){
            String description = stat.get("Age Range").concat(" - ").concat(stat.get("Gender"));
            seriesF.getData().add(new XYChart.Data<>(description, Integer.valueOf(stat.get("Total Authors"))));
        }

        chartStatF.getData().addAll(seriesF);

        cbUniversityF.getItems().addAll(ChoiceBuilder.getUniversityList());
        cbGenderF.getItems().addAll(ChoiceBuilder.getGenderList());
        cbArticleTypeF.getItems().addAll(ChoiceBuilder.getNewsArticleTypeList());

        cbUniversityF.setOnAction(this::refreshFiltersF);
        cbGenderF.setOnAction(this::refreshFiltersF);
        cbArticleTypeF.setOnAction(this::refreshFiltersF);

        XYChart.Series<String, Integer> seriesG = new XYChart.Series<>();
        seriesG.setName("Purchases");
        for (HashMap<String, String> stat: statG){
            seriesG.getData().add(new XYChart.Data<>(stat.get("Product"), Integer.valueOf(stat.get("Purchases"))));
        }

        chartStatG.getData().addAll(seriesG);

        SpinnerValueFactory<Integer> valueFactoryG = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,10000);
        valueFactory.setValue(5);
        spinnerGTopN.setValueFactory(valueFactory);
        spinnerGTopN.valueProperty().addListener(new ChangeListener<Integer>(){
            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
                refreshStatG();
            }
        });




    }

    private void refreshStatG() {
        try{
            chartStatG.getData().clear();

            ArrayList<HashMap<String, String>> statG = statController.getStatRows(7,null,null,null, spinnerGTopN.getValue());
            XYChart.Series<String, Integer> seriesG = new XYChart.Series<>();
            seriesG.setName("Purchases");
            for (HashMap<String, String> stat: statG){
                seriesG.getData().add(new XYChart.Data<>(stat.get("Product"), Integer.valueOf(stat.get("Purchases"))));
            }
            chartStatG.getData().addAll(seriesG);
        }catch(Exception e){
            System.out.println(e);
        }
    }

    private void refreshFiltersF(ActionEvent actionEvent) {
        refreshStatF();
    }

    private void refreshStatF() {
        try{
            chartStatF.getData().clear();
            Integer universityId = null;
            Integer genderId = null;
            Integer articleTypeId = null;
            if(cbUniversityF.getValue() != null){
                universityId = cbUniversityF.getValue().id;
            }
            if(cbGenderF.getValue() != null){
                genderId = cbGenderF.getValue().id;
            }
            if(cbArticleTypeF.getValue() != null){
                articleTypeId = cbArticleTypeF.getValue().id;
            }


            ArrayList<HashMap<String, String>> statF = statController.getStatRows(6,universityId,genderId,articleTypeId,null);
            XYChart.Series<String, Integer> seriesF = new XYChart.Series<>();
            seriesF.setName("Authors");
            for (HashMap<String, String> stat: statF){
                String description = stat.get("Age Range").concat(" - ").concat(stat.get("Gender"));
                seriesF.getData().add(new XYChart.Data<>(description, Integer.valueOf(stat.get("Total Authors"))));
            }
            chartStatF.getData().addAll(seriesF);
        }catch(Exception e){
            System.out.println(e);
        }
    }

    private void refreshFilters(ActionEvent actionEvent) {
        refreshStatD();
    }


    public void refreshStatD(){
        try{
            chartStatD.getData().clear();
            Integer universityId = null;
            Integer genderId = null;
            Integer articleTypeId = null;
            if(cbUniversityD.getValue() != null){
                universityId = cbUniversityD.getValue().id;
            }
            if(cbGenderD.getValue() != null){
                genderId = cbGenderD.getValue().id;
            }
            if(cbArticleTypeD.getValue() != null){
                articleTypeId = cbArticleTypeD.getValue().id;
            }


            ArrayList<HashMap<String, String>> statD = statController.getStatRows(4,universityId,genderId,articleTypeId, spinnerDTopN.getValue());
            XYChart.Series<String, Integer> seriesD = new XYChart.Series<>();
            seriesD.setName("Articles");
            for (HashMap<String, String> stat: statD){
                seriesD.getData().add(new XYChart.Data<>(stat.get("Author"), Integer.valueOf(stat.get("Total Articles"))));
            }
            chartStatD.getData().addAll(seriesD);
        }catch(Exception e){
            System.out.println(e);
        }


    }


}
