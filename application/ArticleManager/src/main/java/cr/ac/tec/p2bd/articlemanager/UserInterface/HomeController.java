/*
 *  The MIT License
 *
 *  Copyright 2022.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package cr.ac.tec.p2bd.articlemanager.UserInterface;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

public class HomeController {
    public Label lblUser;
    public Label lblPoints;
    public Button btnLogOut;
    public MenuItem mnItmCreateAccount;
    public MenuItem mnItmEditProfile;
    public MenuItem mnItmLogOut;
    private final String pointsMessage = "Points: %d";
    public Button btnRefreshPoints;
    public BorderPane homeViewport;
    public Menu mnLstAdmin;
    public Menu mnLstStore;
    public MenuItem mnCreateArticle;
    public MenuItem mnApproveArticle;

    @FXML
    protected void initialize() throws IOException {
        String welcomeMessage = "Welcome %s!";
        if (SessionManager.getInstance().isGuest()) {
            lblUser.setText(welcomeMessage.formatted("Guest"));
            lblPoints.setVisible(false);

            mnItmLogOut.setText("Close");
            mnItmEditProfile.setVisible(false);
            mnLstAdmin.setVisible(false);
            mnLstStore.setVisible(false);
            mnCreateArticle.setVisible(false);
            mnApproveArticle.setVisible(false);
            btnLogOut.setVisible(false);
            btnRefreshPoints.setVisible(false);
            onLoadLatestNews(null);
            return;
        }

        mnLstAdmin.setVisible(SessionManager.getInstance().isAdmin());
        mnLstStore.setVisible(SessionManager.getInstance().isAuthor());
        mnCreateArticle.setVisible(SessionManager.getInstance().isAuthor());
        mnApproveArticle.setVisible(SessionManager.getInstance().isInCommittee());

        HashMap<String, String> personData = SessionManager.getInstance().getPersonData();

        lblUser.setText(welcomeMessage.formatted(personData.get("Username")));

        lblPoints.setVisible(SessionManager.getInstance().isAuthor());
        btnRefreshPoints.setVisible(SessionManager.getInstance().isAuthor());
        lblPoints.setText(pointsMessage.formatted(SessionManager.getInstance().getPoints()));

        mnItmCreateAccount.setVisible(false);

        onLoadLatestNews(null);
    }

    public void logout(ActionEvent event) throws IOException {
        SessionManager.getInstance().logOut();

        FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("login-view.fxml")));
        Stage stage = (Stage)(homeViewport).getScene().getWindow();
        Scene scene = new Scene(loader.load(), 300, 400);
        stage.setScene(scene);
        stage.show();
    }

    public void onEditProfileClick(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("person-editor-view.fxml"));
        AnchorPane editView = loader.load();
        PersonEditorController personEditorController = loader.getController();
        personEditorController.setProfileUpdateMode();

        homeViewport.setCenter(editView);
    }

    public void onRefreshPoints(ActionEvent event) {
        lblPoints.setVisible(SessionManager.getInstance().isAuthor());
        lblPoints.setText(pointsMessage.formatted(SessionManager.getInstance().getPoints()));
    }

    public void onManageUniversity(ActionEvent event) throws IOException {
        AnchorPane manageUniversity = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("manage-university.fxml")));
        homeViewport.setCenter(manageUniversity);
    }

    public void onLoadLatestNews(ActionEvent event) throws IOException {
        AnchorPane newsView = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("latest-news-paginator-view.fxml")));

        homeViewport.setCenter(newsView);
    }

    public void onManageNewspaper(ActionEvent event) throws IOException {
        AnchorPane manageNewspaper = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("manage-newspaper.fxml")));
        homeViewport.setCenter(manageNewspaper);
    }

    public void onNewsSelected(Integer newsId) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("news-showcase-view.fxml"));
        AnchorPane newsShowcase = loader.load();
        NewsShowcaseController newsShowcaseController = loader.getController();
        newsShowcaseController.loadNews(newsId);
        homeViewport.setCenter(newsShowcase);
    }

    public void onViewStatistics(ActionEvent event) throws IOException{
        AnchorPane statistics = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("statistics-view.fxml")));
        homeViewport.setCenter(statistics);
    }

    public void onCreateLocation() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("location-editor-view.fxml"));
        AnchorPane locationEditor = loader.load();
        homeViewport.setCenter(locationEditor);
    }

    public void onManageLocations(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("location-management-view.fxml"));
        AnchorPane locationManager = loader.load();
        homeViewport.setCenter(locationManager);
    }

    public void onEditLocation(HashMap<String, String> location) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("location-editor-view.fxml"));
        AnchorPane locationEditor = loader.load();
        LocationEditorController locationEditorController = loader.getController();
        locationEditorController.setEditMode(location);
        homeViewport.setCenter(locationEditor);
    }

    public void onManagePerson() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("person-management-view.fxml"));
        AnchorPane personManager = loader.load();
        homeViewport.setCenter(personManager);
    }

    public void onCreatePerson() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("person-editor-view.fxml"));
        AnchorPane editView = loader.load();

        homeViewport.setCenter(editView);
    }

    public void onEditPerson(HashMap<String, String> person) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("person-editor-view.fxml"));
        AnchorPane editView = loader.load();
        PersonEditorController personEditorController = loader.getController();
        personEditorController.setEditMode(person);

        homeViewport.setCenter(editView);
    }

    public void onManageProducts() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("products-management-view.fxml"));
        AnchorPane productsManager = loader.load();

        homeViewport.setCenter(productsManager);
    }

    public void onEditProduct(HashMap<String, String> product) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("product-editor-view.fxml"));
        AnchorPane editView = loader.load();
        ProductEditorController productEditorController = loader.getController();
        productEditorController.editMode(product);

        homeViewport.setCenter(editView);
    }

    public void onCreateProduct() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("product-editor-view.fxml"));
        AnchorPane editView = loader.load();

        homeViewport.setCenter(editView);
    }

    public void onOpenStore(ActionEvent event) throws IOException {
        Stage modalDialog = new Stage();
        FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("store-view.fxml"));
        Parent dialogRoot = dialogLoader.load();
        Scene dialogScene = new Scene(dialogRoot,280, 200);
        modalDialog.initOwner(homeViewport.getScene().getWindow());
        modalDialog.setScene(dialogScene);
        modalDialog.setResizable(false);
        modalDialog.showAndWait();
        onRefreshPoints(event);
    }

    public void onCreateArticle(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("article-editor-view.fxml"));
        AnchorPane editView = loader.load();

        homeViewport.setCenter(editView);
    }

    public void onEditArticle(HashMap<String, String> article) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("article-editor-view.fxml"));
        AnchorPane editView = loader.load();
        ArticleEditorController articleEditorController = loader.getController();
        articleEditorController.setEditMode(article);

        homeViewport.setCenter(editView);
    }

    public void onReviewArticle(HashMap<String, String> article) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("article-editor-view.fxml"));
        AnchorPane editView = loader.load();
        ArticleEditorController articleEditorController = loader.getController();
        articleEditorController.setReviewMode(article);

        homeViewport.setCenter(editView);
    }

    public void onOpenReview(ActionEvent event) throws IOException {
        AnchorPane newsView = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("review-news-paginator-view.fxml")));

        homeViewport.setCenter(newsView);
    }
}
