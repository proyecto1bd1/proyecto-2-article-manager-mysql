/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import java.util.Set;

/**
 * @author Diego Herrera, Daniel G
 */
public class Parameter extends AbstractModel {
    private Integer ParameterId;
    private String Name;
    private String Description;
    private String Value;

    public Parameter() {
        this.ParameterId = null;
        this.Name = null;
        this.Description = null;
        this.Value = null;
    }

    private Parameter(Integer ParameterId, String Name, String Description, String Value) {
        this.ParameterId = ParameterId;
        this.Name = Name;
        this.Description = Description;
        this.Value = Value;
    }

    public Parameter(String Name, String Description, String Value) {
        this.Name = Name;
        this.Description = Description;
        this.Value = Value;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public void setValue(String value) {
        this.Value = value;
    }

    public Integer getParameterId() {
        return ParameterId;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getValue() {
        return Value;
    }

    /**
     * Save can only update parameters
     *
      If parameter cannot be updated, notify the error
     */
    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call updateParameter(?, ?, ?, ?)}");
            sql.setInt(1, ParameterId);
            sql.setString(2, Name);
            sql.setString(3, Description);
            sql.setString(4, Value);

            sql.executeQuery();
        } catch (SQLException e) {
            throw new Exception("Error updating parameter", e);
        }
    }

    /**
     * Parameters cannot be deleted
     *
      Parameters cannot be deleted
     */
    @Override
    public void delete() throws Exception {
        throw new Exception("Cannot delete parameters");
    }


    public static @NotNull Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("parameterId");
        columns.add("parameterName");
        columns.add("parameterDescription");
        columns.add("parameterValue");

        return columns;
    }

    @Override
    public String toString() {
        return String.format(
                "Id: %d Name: %s Description: %s Value: %s",
                ParameterId,
                Name,
                Description,
                Value
        );
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> parameterMap = new HashMap<>();

        parameterMap.put("ParameterId", ParameterId.toString());
        parameterMap.put("Name", Name);
        parameterMap.put("Description", Description);
        parameterMap.put("Value", Value);

        return parameterMap;
    }

    private static @NotNull ArrayList<Parameter> buildList(@NotNull ResultSet result) throws SQLException {
        ArrayList<Parameter> parameters = new ArrayList<>();

        while (result.next()) {
            Parameter parameter = new Parameter(
                    result.getInt("ParameterId"),
                    result.getString("Name"),
                    result.getString("Description"),
                    result.getString("Value")
            );

            parameters.add(parameter);
        }

        return parameters;
    }

    public static Parameter find(Integer id) throws Exception {
        Parameter parameter = null;
        ArrayList<Parameter> parameters;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getParameter(?)}");
            sql.setInt(1, id);

            ResultSet result = sql.executeQuery();

            parameters = buildList(result);

            if (!parameters.isEmpty()) {
                parameter = parameters.get(0);
            }

            return parameter;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch parameters", e);
        }
    }

    public static @NotNull ArrayList<Parameter> getAll() throws Exception {
        ArrayList<Parameter> parameters;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getParameter(?)}");
            sql.setNull(1, Types.BIGINT);

            ResultSet result = sql.executeQuery();

            parameters = buildList(result);

            return parameters;
        } catch (SQLException e) {
            throw new Exception("Failed to fetch parameters", e);
        }
    }
}
