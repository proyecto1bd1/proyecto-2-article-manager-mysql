/*
 * The MIT License
 *
 * Copyright 2022 Diego Herrera.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.BusinessLayer.Controller;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Models.Gender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Diego Herrera
 */
public class GenderController {

    /**
     * Gets a list of all genders
     *
     * @return All the entries in genders as rows
     
     */
    public ArrayList<HashMap<String, String>> getGenderRows() throws Exception {

        ArrayList<HashMap<String, String>> genderRows = new ArrayList<>();

        ArrayList<Gender> genders = Gender.getAll();

        genders.forEach((Gender gender) -> {
            genderRows.add(gender.toMap());
        });

        return genderRows;
    }

    /**
     * Get the columns of the gender model
     *
     * @return A list of columns
     
     */
    public ArrayList<String> getGenderColumns() throws Exception {
        Set<String> columns = Gender.getColumns();

        return new ArrayList<>(columns);
    }

    /**
     * Get a gender as a row
     *
     * @param genderId The gender to locate
     * @return The row representing the gender
     
     */
    public HashMap<String, String> getGenderRow(Integer genderId) throws Exception {
        Gender gender = Gender.find(genderId);

        return gender.toMap();
    }

    /**
     * Stores a new gender
     *
     * @param genderName The name of the gender
     * @return The id of the created element
     
     */
    public Integer createGender(String genderName) throws Exception {
        Gender gender = new Gender(genderName);

        gender.save();

        return gender.getGenderId();
    }

    /**
     * Updates the value of a gender
     *
     * @param genderId   The id of the gender to update
     * @param genderName The new name of the gender
     * @return The new name of the gender
     
     */
    public String updateGender(Integer genderId, String genderName) throws Exception {
        Gender gender = Gender.find(genderId);

        gender.setGenderName(genderName);
        gender.save();

        return gender.getGenderName();
    }

    /**
     * Deletes a gender from the system
     *
     * @param genderId The id of the gender
     * @return True if gender was deleted, false otherwise
      Could fail if gender has references attach
     */
    public boolean deleteGender(Integer genderId) throws Exception {
        Gender gender = Gender.find(genderId);

        gender.delete();

        return gender.getGenderId() == null;
    }
}
