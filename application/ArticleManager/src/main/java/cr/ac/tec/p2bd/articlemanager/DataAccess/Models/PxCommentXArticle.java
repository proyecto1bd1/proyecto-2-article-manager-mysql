/*
 * The MIT License
 *
 * Copyright 2022.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package cr.ac.tec.p2bd.articlemanager.DataAccess.Models;

import cr.ac.tec.p2bd.articlemanager.DataAccess.Connection.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Daniel G
 */
public class PxCommentXArticle extends AbstractModel {
    private Integer PersonId;
    private Integer ArticleId;
    private String Comments;
    private Date createdAt;

    public PxCommentXArticle() {
    }

    public PxCommentXArticle(Integer PersonId, Integer ArticleId, String Comments, Date createdAt) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.Comments = Comments;
        this.createdAt = createdAt;
    }

    public PxCommentXArticle(Integer PersonId, Integer ArticleId, String Comments) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.Comments = Comments;
    }

    public PxCommentXArticle(Integer ArticleId, String Comments) {
        this.ArticleId = ArticleId;
        this.Comments = Comments;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public String getComments() {
        return Comments;
    }

    @Override
    public void save() throws Exception {
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            if (this.ArticleId != null & this.PersonId != null) {
                CallableStatement sql;
                sql = connection.prepareCall("{call createComment(?, ?, ?)}");
                sql.setInt(1, this.PersonId);
                sql.setInt(2, this.ArticleId);
                sql.setString(3, this.Comments);
                sql.executeQuery();
            } else {
                CallableStatement sql;
                sql = connection.prepareCall("{call updateComment(?, ?, ?)}");
                sql.setInt(1, this.PersonId);
                sql.setInt(2, this.ArticleId);
                sql.setString(3, this.Comments);
                sql.executeQuery();
            }
        } catch (SQLException e) {
            throw new Exception("Error saving comment", e);
        }
    }

    @Override
    public void delete() throws Exception {
        if (this.ArticleId == null & this.PersonId == null) {
            return;
        }
        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call deleteComment(?, ?)}");
            sql.setInt(1, this.PersonId);
            sql.setInt(2, this.ArticleId);
            sql.executeQuery();
            this.PersonId = null;
            this.ArticleId = null;
        } catch (SQLException e) {
            throw new Exception("Error: Unable to delete comment", e);
        }
    }

    @Override
    public HashMap<String, String> toMap() {
        HashMap<String, String> PxCommentXArticleMap = new HashMap<>();

        PxCommentXArticleMap.put("personId", PersonId.toString());
        PxCommentXArticleMap.put("articleId", ArticleId.toString());
        PxCommentXArticleMap.put("comments", Comments);
        PxCommentXArticleMap.put("createdAt", createdAt != null ? createdAt.toString() : "");
        return PxCommentXArticleMap;
    }

    public static Set<String> getColumns() {

        HashSet<String> columns = new HashSet<>();

        columns.add("personId");
        columns.add("articleId");
        columns.add("comments");
        columns.add("createdAt");
        return columns;
    }

    public static ArrayList<PxCommentXArticle> getAllInArticle(Integer articleId) throws Exception {
        ArrayList<PxCommentXArticle> pxCommentXArticles = new ArrayList<>();


        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getCommentsArticle(?)}");
            sql.setInt(1, articleId);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                PxCommentXArticle pxCommentXArticle = new PxCommentXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getString("comments"),
                        result.getDate("creationDate")
                );
                pxCommentXArticles.add(pxCommentXArticle);
            }

            return pxCommentXArticles;
        } catch (SQLException e) {
            throw new Exception("Unable to get comment", e);
        }
    }

    public static PxCommentXArticle find(Integer articleId, Integer personId) throws Exception {
        PxCommentXArticle pxCommentXArticle = null;

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getComment(?, ?)}");
            sql.setInt(1, articleId);
            sql.setInt(2, personId);

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                pxCommentXArticle = new PxCommentXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getString("comments"),
                        result.getDate("creationDate")
                );
            }

            return pxCommentXArticle;
        } catch (SQLException e) {
            throw new Exception("Unable to get comment", e);
        }
    }

    public static ArrayList<PxCommentXArticle> getAll() throws Exception {
        ArrayList<PxCommentXArticle> pxCommentXArticles = new ArrayList<>();

        try (Connection connection = DBConnection.getCon()) {
            assert connection != null;
            CallableStatement sql = connection.prepareCall("{call getAllComments()}");

            ResultSet result = sql.executeQuery();

            while (result.next()) {
                PxCommentXArticle pxCommentXArticle = new PxCommentXArticle(
                        result.getInt("personId"),
                        result.getInt("articleId"),
                        result.getString("comments"),
                        result.getDate("creationDate")
                );
                pxCommentXArticles.add(pxCommentXArticle);
            }

            return pxCommentXArticles;
        } catch (SQLException e) {
            throw new Exception("Unable to get comments", e);
        }
    }

    @Override
    public String toString() {
        return String.format(
                "PxCommentXArticle{ PersonId: %d ArticleId: %d Comments: %s }",
                PersonId,
                ArticleId,
                Comments
        );
    }
}
