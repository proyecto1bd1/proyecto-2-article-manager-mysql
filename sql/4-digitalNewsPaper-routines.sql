DELIMITER $$

DROP FUNCTION IF EXISTS createDigitalnewspaper$$
CREATE FUNCTION createDigitalnewspaper(
    p_newsPaperName VARCHAR(100),
    p_committeeId BIGINT,
    p_universityId BIGINT
)
RETURNS BIGINT
BEGIN
    DECLARE digitalNewspaperId BIGINT DEFAULT 0;
    INSERT INTO digitalNewspaper (newsPaperName, committeeId, universityId)
    VALUES (p_newsPaperName, p_committeeId, p_universityId);

    SET digitalNewspaperId = LAST_INSERT_ID();

    RETURN digitalNewspaperId;
END$$

DROP PROCEDURE IF EXISTS updateDigitalnewspaper$$
CREATE PROCEDURE updateDigitalnewspaper(
    p_newsPaperId BIGINT,
    p_newsPaperName VARCHAR(100),
    p_committeeId BIGINT,
    p_universityId BIGINT
)
BEGIN
    UPDATE digitalNewspaper
    SET newsPaperName = p_newsPaperName,
        committeeId = p_committeeId,
        universityId = p_universityId
    WHERE newsPaperId = p_newsPaperId;
END$$

DROP PROCEDURE IF EXISTS deleteDigitalnewspaper$$
CREATE PROCEDURE deleteDigitalnewspaper(
    p_newsPaperId BIGINT
)
BEGIN

    DECLARE defaultNewsPaper BIGINT DEFAULT 0;

    SELECT CAST(value as SIGNED)
    INTO defaultNewsPaper
    FROM parameter
    WHERE name = 'defaultRegistryNewsPaper';

    IF defaultNewsPaper = p_newsPaperId THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete default digital newspaper', MYSQL_ERRNO = 1001;
    END IF;

    DELETE FROM digitalNewspaper
    WHERE newsPaperId = p_newsPaperId;
END$$
DROP PROCEDURE IF EXISTS getDigitalnewspaper$$
CREATE PROCEDURE getDigitalnewspaper(
    p_newsPaperId BIGINT
)
BEGIN
    SELECT newsPaperId, newsPaperName, committeeId, universityId
    FROM digitalNewspaper
    WHERE newsPaperId = COALESCE(p_newsPaperId, newsPaperId);
END$$

DELIMITER ;
