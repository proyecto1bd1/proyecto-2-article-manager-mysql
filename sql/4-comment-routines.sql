DELIMITER $$

DROP PROCEDURE IF EXISTS createComment$$
CREATE PROCEDURE createComment(
    p_personId BIGINT,
    p_articleId BIGINT,
    p_comments VARCHAR(250)
)
BEGIN
    INSERT INTO pxcommentxarticle(
        personId,
        articleId,
        comments
    )
    VALUES(
        p_personId,
        p_articleId,
        p_comments
    );
END$$

DROP PROCEDURE IF EXISTS updateComment$$
CREATE PROCEDURE updateComment(
    p_personId BIGINT,
    p_articleId BIGINT,
    p_comments VARCHAR(250)
)
BEGIN
    UPDATE pxcommentxarticle
    SET comments = p_comments
    WHERE articleId = p_articleId AND personId = p_personId;
END$$

DROP PROCEDURE IF EXISTS deleteComment$$
CREATE PROCEDURE deleteComment(
    p_articleId BIGINT,
    p_personId BIGINT
)
BEGIN
    DELETE FROM pxcommentxarticle
    WHERE articleId = p_articleId AND personId = p_personId;
END$$

DROP PROCEDURE IF EXISTS getCommentsArticle$$
CREATE PROCEDURE getCommentsArticle(
    p_articleId BIGINT
)
BEGIN
    SELECT personId,
           articleId,
           comments,
           creationDate
    FROM pxcommentxarticle
    WHERE articleId = COALESCE(p_articleId, articleId);
END$$

DROP PROCEDURE IF EXISTS getAllComments$$
CREATE PROCEDURE getAllComments()
BEGIN
    SELECT personId,
           articleId,
           comments,
           creationDate
    FROM pxcommentxarticle;
END$$

DROP PROCEDURE IF EXISTS getComment$$
CREATE PROCEDURE getComment(
    p_articleId BIGINT,
    p_personId BIGINT
)
BEGIN
    SELECT personId,
           articleId,
           comments,
           creationDate
    FROM pxcommentxarticle
    WHERE articleId = p_articleId AND personId = p_personId;
END$$

DELIMITER ;
