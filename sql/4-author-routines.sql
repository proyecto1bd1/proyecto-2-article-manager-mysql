DELIMITER $$

DROP PROCEDURE IF EXISTS createAuthor$$
CREATE PROCEDURE createAuthor(
    p_personId BIGINT,
    p_authorTypeId BIGINT
)
BEGIN
    INSERT INTO author(authorId, authorTypeId)
    VALUES(p_personId, p_authorTypeId);
END$$

DROP PROCEDURE IF EXISTS updateAuthor$$
CREATE PROCEDURE updateAuthor(
    p_authorId BIGINT,
    p_authorTypeId BIGINT
)
BEGIN
    UPDATE author
    SET authorTypeId = p_authorTypeId
    WHERE authorId = p_authorId;
END$$

DROP PROCEDURE IF EXISTS deleteAuthor$$
CREATE PROCEDURE deleteAuthor(
    p_authorId BIGINT
)
BEGIN
    DELETE FROM author WHERE authorId = p_authorId;
END$$

DROP PROCEDURE IF EXISTS getAuthors$$
CREATE PROCEDURE getAuthors(
    p_authorId BIGINT
)
BEGIN
    SELECT authorId,
           authorTypeId
    FROM author
    WHERE authorId = COALESCE(p_authorId, authorId);
END$$

DROP PROCEDURE IF EXISTS getAuthorPoints$$
CREATE PROCEDURE getAuthorPoints(
    p_authorId BIGINT
)
BEGIN
    SELECT count(1) points,
           newsarticle.authorId
    FROM pxfavoritexarticle
    INNER JOIN newsarticle
    ON newsarticle.articleId = pxfavoritexarticle.articleId
    GROUP BY newsarticle.authorId
    HAVING newsarticle.authorId = coalesce(p_authorId, newsarticle.authorId);
END$$

DROP FUNCTION IF EXISTS getAuthorAvailablePoints$$
CREATE FUNCTION getAuthorAvailablePoints(
    p_authorId BIGINT
)
RETURNS INT
BEGIN
    DECLARE allPoints INT DEFAULT 0;
    DECLARE expendPoints INT DEFAULT 0;
    DECLARE totalPoints INT DEFAULT 0;

    SELECT count(1) points
    INTO allPoints
    FROM pxfavoritexarticle
    INNER JOIN newsarticle
    ON newsarticle.articleId = pxfavoritexarticle.articleId
    GROUP BY newsarticle.authorId
    HAVING newsarticle.authorId = coalesce(p_authorId, newsarticle.authorId);


    SELECT sum(product.pointsPrice)
    INTO expendPoints
    FROM product
    INNER JOIN purchasexproduct
    ON product.productId = purchasexproduct.purchaseId
    INNER JOIN purchase
    ON purchasexproduct.purchaseId = purchase.purchaseId
    WHERE purchase.personId = coalesce(p_authorId, purchase.personId);

    SET totalPoints = allPoints - expendPoints;

    RETURN totalPoints;
END$$

DELIMITER ;
