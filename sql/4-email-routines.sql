DELIMITER $$

DROP FUNCTION IF EXISTS createEmail$$
CREATE FUNCTION createEmail(
    p_emailAddress VARCHAR(100),
    p_personId BIGINT
)
RETURNS BIGINT
BEGIN
    DECLARE emailId BIGINT DEFAULT 0;

    INSERT INTO email (emailAddress, personId)
    VALUES (p_emailAddress, p_personId);

    SET emailId = LAST_INSERT_ID();

    RETURN emailId;
END$$

DROP PROCEDURE IF EXISTS updateEmail$$
CREATE PROCEDURE updateEmail(
    p_emailId BIGINT,
    p_emailAddress VARCHAR(100),
    p_personId BIGINT
)
BEGIN
    UPDATE email
    SET emailAddress = p_emailAddress,
        personId = p_personId
    WHERE emailId = p_emailId;
END$$

DROP PROCEDURE IF EXISTS deleteEmail$$
CREATE PROCEDURE deleteEmail(
    p_emailId BIGINT
)
BEGIN
    DELETE FROM email
    WHERE emailId = p_emailId;
END$$

DROP PROCEDURE IF EXISTS getEmail$$
CREATE PROCEDURE getEmail(
    p_emailId BIGINT
)
BEGIN
    SELECT emailId,
           emailAddress,
           personId
    FROM email
    WHERE emailId = COALESCE(p_emailId, emailId);
END$$

DELIMITER ;
