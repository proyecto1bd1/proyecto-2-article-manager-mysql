-- ################ PARAMETER DATA ####################
INSERT INTO project2.`parameter` (name,description,value)
VALUES
 ('newsPaginationQuantity','How many elements will be saw per page','3'),
 ('defaultRegistryNewsPaper','The default digital news paper a user will have on sign up','1');

-- ################ COUNTRY DATA ####################

INSERT INTO country(CountryName)
VALUES (
'Costa Rica');

-- ################ Province DATA ####################

INSERT INTO province(ProvinceName,CountryId)
VALUES (
'San José',
1
);
INSERT INTO province(ProvinceName,CountryId)
VALUES (
'Alajuela',
1
);
INSERT INTO province(ProvinceName,CountryId)
VALUES (
'Cartago',
1
);
INSERT INTO province(ProvinceName,CountryId)
VALUES (
'Heredia',
1
);
INSERT INTO province(ProvinceName,CountryId)
VALUES (
'Guanacaste',
1
);
INSERT INTO province(ProvinceName,CountryId)
VALUES (
'Puntarenas',
1
);
INSERT INTO province(ProvinceName,CountryId)
VALUES (
'Limón',
1
);

-- ################ Canton DATA ####################


INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San José',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Escazú',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Descamparados',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Puriscal',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Tarrazú',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Aserrí',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Mora',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Goicoechea',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Santa Ana',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Alajuelita',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Vásquez de Coronado',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Acosta',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Tibás',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Moravia',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Montes de Oca',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Turrubares',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Dota',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Curridabat',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Peréz Zeledón',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'León Cortés Castro',
1
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Alajuela',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San Ramón',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Grecia',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San Mateo',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Atenas',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Naranjo',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Palmares',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Poás',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Orotina',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San Carlos',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Zarcero',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Sarchí',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Upala',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Los Chiles',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Guatuso',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Río Cuarto',
2
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Cartago',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Paraíso',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'La Unión',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Jimenéz',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Turrialba',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Alvarado',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Oreamuno',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'El Guarco',
3
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Heredia',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Barva',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Santo Domingo',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Santa Bárbara',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San Rafael',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San Isidro',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Belén',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Flores',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'San Pablo',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Sarapiquí',
4
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Liberia',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Nicoya',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Santa Cruz',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Bagaces',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Carrillo',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Cañas',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Abangares',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Tilarán',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Nandayure',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'La Cruz',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Hojancha',
5
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Puntarenas',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Esparza',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Buenos Aires',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Montes de Oro',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Osa',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Quepos',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Golfito',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Coto Brus',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Parrita',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Corredores',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Garabito',
6
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Limón',
7
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Pococí',
7
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Siquirres',
7
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Talamanca',
7
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Matina',
7
);
INSERT INTO canton(CantonName,ProvinceId)
VALUES (
'Guácimo',
7
);

-- ################ DISTRICT DATA ####################


INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carmen',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Merced',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Hospital',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Catedral',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Zapote',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Francisco Dos Ríos',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Uruca',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mata Redonda',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pavas',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Hatillo',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Sebastián',
1
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Escazú',
2
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
2
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
2
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Desamparados',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Miguel',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan de Dios',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael Arriba',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Frailes',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Patarrá',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Cristóbal',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Rosario',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Damas',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael Abajo',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Gravillas',
3
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Los Guido',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santiago',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mercedes Sur',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Barbacoas',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Candelarita',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Desamparaditos',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Chires',
4
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Marcos',
5
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Lorenzo',
5
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Carlos',
5
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Aserrí',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tarbaca',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Vuelta De Jorco',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Gabriel',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Legua',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Monterrey',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Salitrillos',
6
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Colón',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guayabo',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tabarcia',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Piedras Negras',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Picagres',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Jaris',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Quitirrisí',
7
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guadalupe',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Francisco',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Calle Blancos',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mata de Plátano',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Ipis',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Rancho Redondo',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Purral',
8
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Ana',
9
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Salitral',
9
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pozos',
9
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Uruca',
9
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Piedades',
9
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Brasil',
9
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Alajuelita',
10
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Josecito',
10
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
10
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Concepción',
10
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Felipe',
10
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
11
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
11
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Dulce Nombre de Jesús',
11
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Patalillo',
11
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cascajal',
11
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Ignacio',
12
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guaitil',
12
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmichal',
12
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cangrejal',
12
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sabanillas',
12
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
13
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cinco Esquinas',
13
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Aselmo Llorente',
13
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'León XIII',
13
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Colima',
13
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Vicente',
14
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Jerónimo',
14
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Trinidad',
14
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
15
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sabanilla',
15
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mercedes',
15
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
15
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pablo',
16
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
16
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan de Mata',
16
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Luis',
16
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carara',
16
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa María',
17
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Jardín',
17
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Copey',
17
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Curridabat',
18
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Granadilla',
18
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sánchez',
18
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tirrases',
18
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro El General',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El General',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Daniel Flores',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Rivas',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Platanares',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pejibaye',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cajón',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Barú',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Nuevo',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Páramo',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Amistad',
19
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pablo',
20
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Andrés',
20
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Llano Bonito',
20
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
20
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Cruz',
20
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
20
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Alajuela',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carrizal',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guácima',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sabanilla',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Segundo',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Desamparados',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Turrúcares',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tambor',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Garita',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sarapiquí',
21
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Ramón',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santiago',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Piedades Norte',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Piedades Sur',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Ángeles',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Alfaro',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Volio',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Concepción',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Zapotal',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Peñas Blancas',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Lorenzo',
22
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Grecia',
23
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
23
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José',
23
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Roque',
23
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tacares',
23
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Puente de Piedra',
23
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bolívar',
24
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Mateo',
24
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Desmonte',
24
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Labrador',
24
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Atenas',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Jesús',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mercedes',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Concepción',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Eulalia',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Escobal',
25
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Naranjo',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Miguel',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cirrí Sur',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Jerónimo',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El Rosario',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmitos',
26
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmares',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Zaragoza',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Buenos Aires',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santiago',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Candelaria',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Esquipulas',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Granja',
27
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
28
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
28
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
28
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carrillos',
28
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sabana Redonda',
28
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Orotina',
29
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El Mastate',
29
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Hacienda Vieja',
29
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Coyolar',
29
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Ceiba',
29
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Quesada',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Florencia',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Buenavista',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Aguas Zarcas',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Venecia',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pital',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Fortuna',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Tigra',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Palmera',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Venado',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cutris',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Monterrey',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pocosol',
30
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Zarcero',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Laguna',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tapesco',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guadalupe',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmira',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Zapote',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Brisas',
31
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sarchí Norte',
32
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sarchí Sur',
32
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Toro Amarillo',
32
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
32
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Rodríguez',
32
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Upala',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Aguas Claras',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José o Pizote',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bijagua',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Delicias',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Dos Ríos',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Yolillal',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Canalete',
33
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Los Chiles',
34
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Caño Negro',
34
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El Amparo',
34
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Jorge',
34
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
35
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Buenavista',
35
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cote',
35
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Katira',
35
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Cuarto',
36
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Rita',
36
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Isabel',
36
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Oriental',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Occidental',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carmen',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Nicolás',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Aguacaliente',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guadalupe o Arenilla',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Corralillo',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tierra Blanca',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Dulce Nombre',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Llano Grande',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Quebradilla',
37
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Paraíso',
38
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santiago',
38
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Orosi',
38
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cachí',
38
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Llanos de Santa Lucía',
38
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tres Ríos',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Diego',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Concepción',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Dulce Nombre',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Ramón',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Azul',
39
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Juan Viñas',
40
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tucurrique',
40
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pejibaye',
40
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Turrialba',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Suiza',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Peralta',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Cruz',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Teresita',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pavones',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tuis',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tayutic',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tres Equis',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Isabel',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Chirripó',
41
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pacayas',
42
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cervantes',
42
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Capellades',
42
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
43
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cot',
43
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Potrero Cerrado',
43
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cipreses',
43
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Rosa',
43
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El Tejar',
44
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
44
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tobosi',
44
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Patio de Agua',
44
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Heredia',
45
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mercedes',
45
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Francisco',
45
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Ulloa',
45
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Vara Blanca',
45
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Barva',
46
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
46
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pablo',
46
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Roque',
46
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Lucía',
46
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José de la Montaña',
46
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santo Domingo',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Vicente',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Miguel',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Paracito',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santo Tomás',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Rosa',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tures',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pará',
47
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Bárbara',
48
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pedro',
48
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
48
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Jesús',
48
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santo Domingo',
48
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Purabá',
48
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
49
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Josecito',
49
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santiago',
49
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Ángeles',
49
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Concepción',
49
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
50
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San José',
50
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Concepción',
50
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Francisco',
50
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
51
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Ribera',
51
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Asunción',
51
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Joaquín',
52
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Barrantes',
52
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Llorente',
52
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pablo',
53
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Rincón Sabanilla',
53
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Puerto Viejo',
54
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Virgen',
54
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Las Horquetas',
54
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Llanuras de Gaspar',
54
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cureña',
54
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Liberia',
55
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cañas Dulces',
55
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mayorga',
55
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Nacascolo',
55
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Curubandé',
55
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Nicoya',
56
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mansión',
56
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Antonio',
56
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Quegrada Honda',
56
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sámara',
56
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Nosara',
56
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Belén de Nosarita',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Cruz',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bolsón',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Veintisiete de Abril',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tempate',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cartagena',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cuajiniquil',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Diriá',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cabo Velas',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tamarindo',
57
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bagaces',
58
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Fortuna',
58
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mogote',
58
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Naranjo',
58
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Filadelfia',
59
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmira',
59
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sardinal',
59
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Belén',
59
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cañas',
60
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmira',
60
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Miguel',
60
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bebedero',
60
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Porozal',
60
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Las Juntas',
61
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sierra',
61
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan',
61
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Colorado',
61
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tilarán',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Quebrada Grande',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tronadora',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Rosa',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Líbano',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tierras Morenas',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Arenal',
62
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carmona',
63
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Rita',
63
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Zapotal',
63
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Pablo',
63
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Porvenir',
63
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bejuco',
63
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Cruz',
64
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Cecilia',
64
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Garita',
64
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Santa Elena',
64
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Hojancha',
65
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Monte Romo',
65
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Puerto Carrillo',
65
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Huacas',
65
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Matambú',
65
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Puntarenas',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pitahaya',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Chomes',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Lepanto',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Paquera',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Manzanillo',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guacimal',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Barranca',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Monte Verde',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cóbano',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Chacarita',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Chira',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Acapulco',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El Roble',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Arancibia',
66
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Espíritu Santo',
67
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Juan Grande',
67
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Macacona',
67
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Rafael',
67
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Jerónimo',
67
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Caldera',
67
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Buenos Aires',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Volcán',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Potrero Grande',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Boruca',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pilas',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Colinas',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Chángena',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Biolley',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Brunka',
68
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Miramar',
69
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Unión',
69
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Isidro',
69
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Puerto Cortés',
70
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Palmar',
70
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sierpe',
70
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bahía Ballena',
70
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Piedras Blancas',
70
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bahía Drake',
70
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Quepos',
71
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Savegre',
71
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Naranjito',
71
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Golfito',
72
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Puerto Jiménez',
72
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guaycará',
72
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pavón',
72
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'San Vito',
73
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sabalito',
73
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Aguabuena',
73
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Limoncito',
73
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pittier',
73
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Gutiérrez Braun',
73
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Parrita',
74
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Corredor',
75
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Cuesta',
75
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Canoas',
75
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Laurel',
75
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Jacó',
76
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Tarcoles',
76
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Limón',
77
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Valle la Estrella',
77
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Blanco',
77
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Matama',
77
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guápiles',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Jiménez',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Rita',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Roxana',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cariari',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Colorado',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Colonia',
78
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Siquirres',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pacuarito',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Florida',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Germania',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'El Cairo',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'La Alegria',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Reventazón',
79
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Bratsi',
80
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Sixaola',
80
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Cahuita',
80
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Telire',
80
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Matina',
81
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Batán',
81
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Carrandi',
81
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Guácimo',
82
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Mercedes',
82
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Pocora',
82
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Río Jiménez',
82
);
INSERT INTO district(DistrictName,CantonId)
VALUES (
'Duacarí',
82
);





-- ################ UNIVERSITY DATA ####################

INSERT INTO university(UniversityName,LocalAddress,DistrictId)
VALUES (
'TEC','1 km South from Basilica',
37
);
INSERT INTO university(UniversityName,LocalAddress,DistrictId)
VALUES (
'UCR','San Pedro',
88
);
INSERT INTO university(UniversityName,LocalAddress,DistrictId)
VALUES (
'UNED','Calle Faustino',
90
);




-- ################ AUTHORTYPE DATA ####################
INSERT INTO authortype(Description)
VALUES (
'Professor');
INSERT INTO authortype(Description)
VALUES (
'Student');
INSERT INTO authortype(Description)
VALUES (
'Admin');
INSERT INTO authortype(Description)
VALUES (
'Neigthbour');
-- ################ COMMITTEE DATA ####################

INSERT INTO committee(Description)
VALUES (
'Committee INFOTEC');
INSERT INTO committee(Description)
VALUES (
'Committee UCR');
INSERT INTO committee(Description)
VALUES (
'Committee UNED');



-- ################ NEWSPAPER DATA ####################
INSERT INTO digitalnewspaper(NewsPaperName,CommitteeId,UniversityId)
VALUES (
'TEC informs',
1
,
1
);
INSERT INTO digitalnewspaper(NewsPaperName,CommitteeId,UniversityId)
VALUES (
'Connected UCRs',
2
,
2
);
INSERT INTO digitalnewspaper(NewsPaperName,CommitteeId,UniversityId)
VALUES (
'Connected UNEDs',
3
,
3
);



-- ################ GENDER DATA ####################

INSERT INTO gender(GenderName)
VALUES (
'Male');
INSERT INTO gender(GenderName)
VALUES (
'Female');
INSERT INTO gender(GenderName)
VALUES (
'I rather not say it');

-- ################ IDTYPE DATA ####################

INSERT INTO idtype(Description,Mask)
VALUES (
'National ID','X-XXXX-XXXX');
INSERT INTO idtype(Description,Mask)
VALUES (
'Passport','XXXXXXXXX');
INSERT INTO idtype(Description,Mask)
VALUES (
'DIMEX','XXXXXXXXXXXX');





-- ################ NEWSARTICLETYPE DATA ####################

INSERT INTO newsarticletype(Description)
VALUES (
'Scientific ');
INSERT INTO newsarticletype(Description)
VALUES (
'Social work');
INSERT INTO newsarticletype(Description)
VALUES (
'Politics');
INSERT INTO newsarticletype(Description)
VALUES (
'Culture');
INSERT INTO newsarticletype(Description)
VALUES (
'Sports');
INSERT INTO newsarticletype(Description)
VALUES (
'Events - Museum exhibition');
INSERT INTO newsarticletype(Description)
VALUES (
'Opinion');
INSERT INTO newsarticletype(Description)
VALUES (
'Job offers');
INSERT INTO newsarticletype(Description)
VALUES (
'Events - Welcome first entry students');
INSERT INTO newsarticletype(Description)
VALUES (
'Entertainment');



-- ################ STORE DATA ####################

INSERT INTO store(description,newsPaperId)
VALUES (
'TECstore',
1
);
INSERT INTO store(description,newsPaperId)
VALUES (
'UCRstore',
2
);
INSERT INTO store(description,newsPaperId)
VALUES (
'UNEDstore',
3
);

-- ################ PRODUCT DATA ####################

INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Slate',
10
,
15
,
1
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Mouse',
10
,
26
,
1
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Set Pailorts',
4
,
14
,
1
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Holster',
3
,
20
,
2
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Graphics tablet',
20
,
5
,
2
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Pencils and pencils',
4
,
29
,
2
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Stapler',
5
,
10
,
3
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Wooden Metro',
7
,
7
,
3
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Punch',
5
,
14
,
3
);
INSERT INTO product(Name,PointsPrice,NumberInStock,StoreId)
VALUES (
'Chalk',
5
,
20
,
3
);



-- ################ PERSON DATA ####################

INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'119423231','Diego','Herrera','Alvarez',

STR_TO_DATE('2-8-1905','%d-%m-%Y')

,'Carmen, San Jose','diera','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
1
,
1
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'316257265','Alfonso','Miranda','Leon',

STR_TO_DATE('12-4-1985','%d-%m-%Y')

,'Paraíso, Paraíso','fomiranda','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
1
,
248
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'418985624','María','Nuñez','Elizondo',

STR_TO_DATE('21-10-1990','%d-%m-%Y')

,'Matina, Matina','mari00','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
2
,
1
,
475
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'X2007983Z','Juan','Peréz','Meza',

STR_TO_DATE('27-2-1980','%d-%m-%Y')

,'Colón, San Jose','juansal','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
3
,
45
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'512517262','Humberto','Beckford','Salazar',

STR_TO_DATE('21-5-2001','%d-%m-%Y')

,'Santiago, Puriscal','salHum','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
1
,
137
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'116267622','Rodolfo','Guzmán','Sandí',

STR_TO_DATE('7-11-1993','%d-%m-%Y')

,'Liberia, Guanacaste','rodo71','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
1
,
334
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'116279275','Mauricio','Porras','Jimenéz',

STR_TO_DATE('22-10-1999','%d-%m-%Y')

,'San Carlos, Tarrazú','mauriP','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
1
,
37
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'44030412V','Celeste','Cubero','Hernandez',

STR_TO_DATE('17-12-1971','%d-%m-%Y')

,'Santa Cruz,Guanacaste','celecubero','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
2
,
3
,
120
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'617271623','Pedro','Vega','Oviedo',

STR_TO_DATE('24-4-2003','%d-%m-%Y')

,'Guacimal, Puntarenas','pedrovega','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
1
,
400
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'7827172182','Daniel','Mendoza','Rodriguez',

STR_TO_DATE('4-4-1984','%d-%m-%Y')

,'Manzanillo, Puntarenas','dany81','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
3
,
399
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'717261521','Javier','Cruz','Solis',

STR_TO_DATE('1-1-1997','%d-%m-%Y')

,'San Diego, La Unión','george122','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
1
,
254
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'C36259281','Fernando','Monterrey','Calvo',

STR_TO_DATE('25-7-2004','%d-%m-%Y')

,'Buenavista,Guatuso','ferMonte','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
2
,
195
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'317278716','Sebastian','Villagra','Uriarte',

STR_TO_DATE('29-5-1997','%d-%m-%Y')

,'Mercedes Sur, Puriscal','sebasvillagra','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
1
,
29
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'517261628','Lucia','Gutierrez','Abarca',

STR_TO_DATE('13-1-1984','%d-%m-%Y')

,'Daniel Flores, Peréz Zeledón','luciaAbarca','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
1
,
106
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'112300048','Sandra','Valdez','Villela',

STR_TO_DATE('21-8-1973','%d-%m-%Y')

,'Bolivar, San Mateo','sanvaldez','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
2
,
1
,
156
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'X0523821F','Marco','Figueres','Castro',

STR_TO_DATE('2-8-1975','%d-%m-%Y')

,'San Carlos, Tarrazú','marcCas','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
2
,
37
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'611278172','José','Calderón','Vargas',

STR_TO_DATE('8-8-2000','%d-%m-%Y')

,'Huacas, Hojancha','josema','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
1
,
392
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'A45419546','Laura','Sandí','Alfaro',

STR_TO_DATE('27-3-1970','%d-%m-%Y')

,'Sixaola, Talamanca','lauraalfaro','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
2
,
2
,
472
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'716712612','Abraham','Martinez','Ulloa',

STR_TO_DATE('12-10-1972','%d-%m-%Y')

,'Santa Bárbara, Santa Bárbara ','uabraham','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
1
,
306
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'155835073224','Julian','Mora','Mendoza',

STR_TO_DATE('15-7-1980','%d-%m-%Y')

,'Tierra Blanca, Cartago','juliMora','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
1
,
3
,
244
);
INSERT INTO person(IdCard,Name,FirstLastName,SecondLastName,Birthdate,LocalAddress,Username,PASSWORD,GenderId,TypeId,DistrictId)
VALUES (
'111111111','admin','admin','admin',

STR_TO_DATE('2-2-2022','%d-%m-%Y')

,'located','admin','$argon2i$v=19$m=65536,t=10,p=1$NTdbEbBrvcaVGvDcyKAWqA$q4eoztL5K/IYKlQlMETpMlzpZPiMwOkCRlEHDx1XSP8',
3
,
1
,
1
);



-- ################ ADMIN DATA ####################

INSERT INTO admin(AdminId)
VALUES (
1
);
INSERT INTO admin(AdminId)
VALUES (
7
);
INSERT INTO admin(AdminId)
VALUES (
3
);
INSERT INTO admin(AdminId)
VALUES (
10
);
INSERT INTO admin(AdminId)
VALUES (
15
);
INSERT INTO admin(AdminId)
VALUES (
18
);
INSERT INTO admin(AdminId)
VALUES (
20
);

-- ################ AUTHOR DATA ####################

INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
10
,
1
);
INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
4
,
3
);
INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
7
,
2
);
INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
8
,
1
);
INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
17
,
2
);
INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
18
,
3
);
INSERT INTO author(AuthorId,AuthorTypeId)
VALUES (
5
,
1
);


-- ################ EMAIL DATA ####################

INSERT INTO email(EmailAddress,PersonId)
VALUES (
'diera@gmail.com',
1
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'fomiranda@hotmail.com',
2
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'mari00@gmail.com',
3
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'juansal@hotmail.com',
4
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'salHum@gmail.com',
5
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'rodo71@outlook.com',
6
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'mauriP@gmail.com',
7
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'celecubero@gmail.com',
8
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'pedrovega@gmail.com',
9
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'dany81@hotmail.com',
10
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'george122@gmail.com',
11
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'ferMonte@outlook.com',
12
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'sebasvillagra@gmail.com',
13
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'luciaAbarca@hotmail.com',
14
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'sanvaldez91@gmail.com',
15
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'marcCas91@hotmail.com',
16
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'josema@gmail.com',
17
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'lauraalfara@hotmail.com',
18
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'uabraham@outlook.com',
19
);
INSERT INTO email(EmailAddress,PersonId)
VALUES (
'juliMora@gmail.com',
20
);

INSERT INTO email(EmailAddress,PersonId)
VALUES (
'admin@gmail.com',
21
);

-- ################ NEWSARTICLE DATA ####################

INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'FEITEC Prepares Welcome for First Entry Students','As usual, the Tecnológico de Costa Rica (TEC) will carry out at the start of this 2016 school year with various welcome activities for the new students of the Institution, who will begin lessons this February 8.

From the first day of classes, events and presentations aimed at the new student body will take place at the Cartago Headquarters. The event itself has been dubbed "Week One" and is organized by the Federation of Students of the TEC (FEITEC).

A partir del primer día de clases, en la Sede Central Cartago tendrán lugar actos y presentaciones dirigidas al nuevo estudiantado. El evento en sí, ha sido denominado como “Semana Uno” y está organizado por la Federación de Students del TEC (FEITEC).',

STR_TO_DATE('2-1-2022','%d-%m-%Y')

,
10
,
9
,
1
,
1
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Amon Cultural House','Casa Cultural Amón is a cultural extension program attached to the School of Culture and Sports of the San José Academic Center. It was inaugurated in 1998, within the policy of "creating cultural centers in the communities where there are headquarters of the TEC".

Its model was based on the philosophy and experience of the Casa de la Ciudad de Cartago, with variants of adaptation to the context of the capital city, a heritage neighborhood and the characteristics of the institutional presence in its San José Campus.

Su modelo se basó en la filosofía y experiencia de la Casa de la Ciudad de Cartago, con variantes de adaptación al contexto de la ciudad capital, de un barrio patrimonial y las características de la presencia institucional en su Campus San José.',

STR_TO_DATE('13-10-2021','%d-%m-%Y')

,
10
,
4
,
1
,
1
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'OAICE UCR Requires:','Day: 1/2 TC
Salary Category: 9
Occupational Class: SPECIALIZED TECHNICIAN D, Square: 46878
Gross Base Salary ¢303 285.00

REQUIREMENTS:
• Third year approved in a career in the area of Computing and Informatics; or title of
Baccalaureate in Secondary Education and equivalent preparation in a career in the area
Mentioned. (See additional observations)
•Fourteen months of experience in work related to the position. (See remarks
additional)
•Knowledge in the following aspects:
• Hardware and software maintenance.
•Administration of servers and operating systems.
• Web page maintenance.
•Security regulations for access to telecommunications equipment and networks.
•CISCO / CCNA.
• Management of computer programs, as appropriate.
• Techniques and protocols for computer security and existing equipment.',

STR_TO_DATE('3-10-2022','%d-%m-%Y')

,
10
,
8
,
1
,
1
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Institutional Council pronounces on FEES negotiation process for 2023','It is due to the lack of response from the Executive Branch to the efforts of the Rectors of the Public Universities to carry out and concretize the negotiation process of the FEES. 

Situation generates uncertainty due to the immediate and significant consequences on the impact of university action on Costa Rican society.',

STR_TO_DATE('23-8-2022','%d-%m-%Y')

,
4
,
3
,
1
,
1
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Indigenous Student Obtained Important Distinction in the Game of the Little Devils of Boruca','Play was declared Intangible Cultural Heritage of Costa Rica.
Student studies the career of Teaching Mathematics with Technological Environments.',

STR_TO_DATE('8-3-2022','%d-%m-%Y')

,
4
,
2
,
1
,
1
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Edition 2023 UCR-FIFA-CIES International Programme for Sport Management','Following the successful original academic scheme proposed by CIES and FIFA, used in similar courses in various parts of the world, and with the necessary modifications to make this program a course of local interest, we present the International Sports Management Program of the University of Costa Rica (UCR) and the International Center for Sports Studies (CIES).',

STR_TO_DATE('18-6-2022','%d-%m-%Y')

,
4
,
5
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Once upon a time…','Today we celebrate this date with a series of fragments of Costa Rican childrens stories accompanied by the expert criteria of Carlos Rubio.',

STR_TO_DATE('27-8-2021','%d-%m-%Y')

,
8
,
10
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Communication may be managed by the community','Radio Cultural Los Santos has almost four decades of serving the population of this area of the center-south of the country.',

STR_TO_DATE('22-7-2022','%d-%m-%Y')

,
8
,
7
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Latest research for livestock production: find it online','The research carried out by the School of Zootechnics of the University of Costa Rica is now available in digital format for the livestock sector and the general public.',

STR_TO_DATE('17-10-2022','%d-%m-%Y')

,
8
,
1
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Costa Rican high-performance sports have an ally in science from the UCR','The Research Center in Human Movement Sciences offers its knowledge to different national entities that administer sport • Agreement with the National Olympic Committee will be signed in the coming days.',

STR_TO_DATE('30-9-2022','%d-%m-%Y')

,
17
,
5
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'The UCR community reflects the cultural diversity of the country','In 2002, the United Nations Educational, Scientific and Cultural Organization (UNESCO) established May 21 of each year as the World Day for Cultural Diversity, in order to promote the recognition of this theme in the daily actions of daily life. 

As a true reflection of Costa Rican society, the UCR community is extremely diverse and is nourished every day by the diverse origins and cultures contributed by its students, teaching and administrative staff.

Como fiel reflejo de la sociedad costarricense, la comunidad UCR es sumamente diversa y se nutre cada día de los diversos orígenes y culturas que aportan sus estudiantes, personal docente y administrativo. ',

STR_TO_DATE('21-5-2021','%d-%m-%Y')

,
17
,
4
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Activities of the Sports Programme','The Sports program of the UNED is responsible for promoting, encouraging, providing, coordinating and launching a range of sports activities at university, national and international level.  This in order to allow the university population to promote their health, improve their sport, develop or expand their qualities, the good use of their free time and sports projection in the future and immediate.  The variety of sports that are developed allow a wide spectrum of activities for all students of the various careers taught by the UNED. Emphasis is placed on sports activities that break the routine and motivate you to get ahead hand in hand with sport.',

STR_TO_DATE('7-3-2022','%d-%m-%Y')

,
17
,
5
,
3
,
3
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'Exhibition of the TEC museum at the San Carlos Campus.','After passing through the José Figueres Ferrer Library in the Cartago Campus, the first exhibition of the TEC Museum will be available in San Carlos. The Library of the Local Technological Campus of the TEC the point where the exhibition It will be present from August 22 to October 30 free of charge and open to the public.The exhibition entitled: "The great feat: The birth and development of the Technological Institute of Costa Rica", has a collection that includes different objects, documents, photographs, information panels, audiovisual material, among other items collected and displayed as part of the initiatives celebration held within the framework of the TECs 50th anniversary.However, more than an exhibition proposal, the exhibition is part of a joint initiative of various institutional instances to strengthen a TEC university museum as a new space to support research, conservation, restoration and exhibition of institutional and national heritage, as well as the world of science and technology.For this reason, the initiative promotes the awareness of safeguarding the work that the Institution has created during these 51 years, as well as a way of sharing with current and future generations stories, projects, objects, installations, people, among others, of the Technological Institute in different points of the national territory.The opening of this exhibition will be held on Thursday, August 25 at 10 a.m.',

STR_TO_DATE('14-10-2022','%d-%m-%Y')

,
10
,
6
,
1
,
1
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'The UCR will transform a parking lot for 66 cars into a biological and pedestrian connector.','The parking lot of the old building of the Faculty of Engineering, located in the heart of the “Rodrigo Facio Brenes” University City, will become a biological connector that will articulate the Leonel Oviedo Forest and the José María Orozco Botanical Garden to give way to fauna urban and pedestrians.This is the project proposal "Urban Ensemble: School of General Studies, School of Plastic Arts and Articulation of Biological Reserves", designed by the Executing Office of the Investment Program (OEPI), which responds to the Land Management Plan (POT) that currently guides the management of spaces in the UCR.With this strategic project, the institution will recover 2,000 m² of natural surface and 850 m² will be allocated for sidewalks covered with permeable cobblestones that will allow the passage of water from the surface to the ground. In addition, the planting of 25 trees and the construction of aerial steps for the fauna that will favor the mobility of the species is contemplated.The project is already in the bidding stage, according to Esteban Camacho, OEPI architect, and construction is expected to begin at the end of this year. "I would say that this project marks a milestone because it is the first one elaborated taking as reference the POT, the prioritization criteria and the siting criteria" underlined the architect.',

STR_TO_DATE('8-3-2021','%d-%m-%Y')

,
4
,
2
,
2
,
2
);
INSERT INTO newsarticle(Title,Content,PublicationDate,AuthorId,ArticleTypeId,NewsPaperId,COMMITTEEIDAPPROV)
VALUES (
'University Council swears in the new Headquarters of the Publishing House of the UNED.','Master Solórzano Alfaro will be the new leader of the Editorial of the UNED. His appointment will be for four years from September 22, 2022. Said swearing-in act took place in session 2931-2022.For Solórzano Alfaro, this trust that was placed in him represents an absolute commitment to editorial work as an essential pillar of university work, of its cultural and educational contribution to the student body and the community in general. It consists of a responsibility that entails all the seriousness of the case and with the aim of positioning the EUNED at the forefront of book production and of the UNED as a creator of diverse content.Mr. Solórzano was consulted about his work plan, how he envisions and where the editorial is projected, for which an excerpt from the interview is shown.',

STR_TO_DATE('1-6-2022','%d-%m-%Y')

,
8
,
3
,
3
,
3
);

-- ################ PERSONXCOMMITTEE DATA ####################
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
10
,
1
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
8
,
2
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
2
,
3
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
5
,
1
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
18
,
2
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
16
,
3
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
1
,
1
);
INSERT INTO personxcommittee(PersonId,CommitteeId)
VALUES (
9
,
2
);

-- ################ PERSONXNEWSPAPER DATA ####################
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
1
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
2
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
3
,
3
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
4
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
5
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
3
,
6
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
7
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
8
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
3
,
9
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
10
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
11
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
3
,
12
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
13
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
14
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
3
,
15
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
16
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
17
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
3
,
18
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
1
,
19
);
INSERT INTO personxnewspaper(NewspaperId,PersonId)
VALUES (
2
,
20
);
-- ################ PXFAVORITEXARTICLE DATA ####################

INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
2
,
1
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
2
,
3
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
2
,
5
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
12
,
2
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
12
,
10
,
1
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
12
,
13
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
1
,
5
,
1
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
1
,
7
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
1
,
15
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
3
,
4
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
3
,
5
,
1
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
4
,
2
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
4
,
3
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
6
,
7
,
0
);
INSERT INTO pxfavoritexarticle(PersonId,ArticleId,Deleted)
VALUES (
7
,
8
,
0
);

-- ################ PURCHASE DATA ####################
INSERT INTO purchase(PersonId)
VALUES (

5
);
INSERT INTO purchase(PersonId)
VALUES (

11
);
INSERT INTO purchase(PersonId)
VALUES (

18
);
-- ################ PURCHASEXPRODUCT DATA ####################
INSERT INTO purchasexproduct(PurchaseId,ProductId)
VALUES (
1
,
5
);
INSERT INTO purchasexproduct(PurchaseId,ProductId)
VALUES (
2
,
2
);
INSERT INTO purchasexproduct(PurchaseId,ProductId)
VALUES (
3
,
9
);

-- ################ TELEPHONE DATA ####################
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

72172172
,
2
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

81728172
,
1
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

22192817
,
5
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

64121821
,
18
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

87271827
,
13
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

62515213
,
15
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

67162612
,
3
);
INSERT INTO telephone(PhoneNumber,PersonId)
VALUES (

22121213
,
6
);

-- ################ PXREVIEWSXARTICLE DATA ####################
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
10
,
4
,
2
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
1
,
2
,
3
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
3
,
12
,
1
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
9
,
7
,
5
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
4
,
5
,
4
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
6
,
9
,
3
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
8
,
1
,
4
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
3
,
2
,
2
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
4
,
7
,
3
);
INSERT INTO pxreviewsxarticle(PersonId,ArticleId,Nstars)
VALUES (
5
,
15
,
1
);
-- ################ PXCOMMENTXARTICLE DATA ####################
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
10
,
4
,'Excellent article.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
4
,
2
,'I loved the article.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
5
,
12
,'My favorite article.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
8
,
7
,'I like the article.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
15
,
14
,'Interesting article.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
17
,
3
,'Amazing Article');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
20
,
9
,'Bad Article.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
7
,
8
,'I hated.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
1
,
5
,'The author of this article needs to learn to write.');
INSERT INTO pxcommentxarticle(PersonId,ArticleId,Comments)
VALUES (
3
,
1
,'I am excited to assist to this event.');

INSERT INTO PARAMETER(name, description, value)
VALUES('TopNpublishers', 'This is used for a statistic about the authors the published the most news articles', 5);