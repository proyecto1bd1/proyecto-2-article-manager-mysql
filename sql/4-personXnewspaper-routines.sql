DELIMITER $$
DROP PROCEDURE IF EXISTS createPersonxnewspaper$$
CREATE PROCEDURE createPersonxnewspaper(p_NewsPaperId  BIGINT, p_PersonId  BIGINT) 
BEGIN
INSERT INTO PERSONXNEWSPAPER (NewsPaperId,PersonId)
VALUES (p_NewsPaperId, p_PersonId);
END$$



DROP PROCEDURE IF EXISTS deletePersonxnewspaper$$
CREATE PROCEDURE deletePersonxnewspaper(p_NewsPaperId  BIGINT, p_PersonId  BIGINT) 
BEGIN
DELETE FROM PERSONXNEWSPAPER
WHERE NewsPaperId = p_NewsPaperId AND PersonId = p_PersonId;
COMMIT;
END$$

DROP PROCEDURE IF EXISTS getPersonxnewspaper$$
CREATE PROCEDURE getPersonxnewspaper(p_NewsPaperId  BIGINT,p_PersonId  BIGINT) 
BEGIN
SELECT NewsPaperId,PersonId
FROM PERSONXNEWSPAPER
WHERE NewsPaperId = coalesce(p_NewsPaperId, NewsPaperId) AND PersonId = coalesce(p_PersonId, PersonId);
END$$ 

DROP PROCEDURE IF EXISTS getNewsPaperRows$$
CREATE PROCEDURE getNewsPaperRows(p_PersonId BIGINT)
BEGIN
    SELECT NewsPaperId,PersonId
    FROM PERSONXNEWSPAPER
    WHERE PersonId = coalesce(p_PersonId, PersonId);
END$$

DELIMITER ;








