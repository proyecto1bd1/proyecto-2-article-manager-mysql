DELIMITER $$
DROP PROCEDURE IF EXISTS getLog$$
CREATE PROCEDURE getLog(p_logId BIGINT)
BEGIN
    SELECT logId,
           fieldName,
           tableName,
           currentValue,
           previousValue,
           articleId
    FROM log
    WHERE logId = COALESCE(p_logId, logId);
END$$

DELIMITER ;
