DROP TABLE IF EXISTS country;
CREATE TABLE country (
  countryId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the country',
  countryName VARCHAR(100) NOT NULL COMMENT 'The name of the country',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS province;
CREATE TABLE province (
  provinceId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the province',
  provinceName VARCHAR(100) NOT NULL COMMENT 'The name of the province',
  countryId BIGINT NOT NULL COMMENT 'Parent country in which it belongs',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS canton;
CREATE TABLE canton (
  cantonId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the canton',
  cantonName VARCHAR(100) NOT NULL COMMENT 'The name of the canton',
  provinceId BIGINT NOT NULL COMMENT 'The parent province in which it belongs',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS district;
CREATE TABLE district (
  districtId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the district',
  districtName VARCHAR(100) NOT NULL COMMENT 'The name of the district',
  cantonId BIGINT NOT NULL COMMENT 'The parent canton in which it belongs',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS parameter;
CREATE TABLE parameter (
  parameterId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the parameter',
  name VARCHAR(100) NOT NULL COMMENT 'The name of the parameter',
  description VARCHAR(200) NOT NULL COMMENT 'The description of what the parameter does',
  value VARCHAR(100) NOT NULL COMMENT 'The value the parameter has',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS idType;
CREATE TABLE idType (
  typeId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the id type',
  description VARCHAR(100) NOT NULL COMMENT 'The description of what type of id is',
  mask VARCHAR(100) NOT NULL COMMENT 'How the id should look',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS gender;
CREATE TABLE gender (
  genderId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the gender',
  genderName VARCHAR(100) NOT NULL COMMENT 'The name of the gender',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS authorType;
CREATE TABLE authorType (
  authorTypeId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the type of author',
  description VARCHAR(100) NOT NULL COMMENT 'The description of what type it is',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS newsArticleType;
CREATE TABLE newsArticleType (
  typeId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the type of article',
  description VARCHAR(100) NOT NULL COMMENT 'The description of what type it is',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS committee;
CREATE TABLE committee (
  committeeId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the committee',
  description VARCHAR(100) NOT NULL COMMENT 'The name of the committee',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS university;
CREATE TABLE university (
  universityId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the university',
  universityName VARCHAR(150) NOT NULL COMMENT 'The name of the university',
  districtId BIGINT NOT NULL COMMENT 'The district in which the university is',
  localAddress VARCHAR(150) COMMENT 'The exact location where to go from the district',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS person;
CREATE TABLE person (
  personId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the person',
  idCard VARCHAR(80) NOT NULL COMMENT 'The identification document of the person',
  name VARCHAR(80) NOT NULL COMMENT 'The name of the person',
  firstLastName VARCHAR(80) NOT NULL COMMENT 'The first last name of the person',
  secondLastName VARCHAR(80) COMMENT 'The second last name of the person, optional',
  birthdate DATE NOT NULL COMMENT 'The date of birth of the person',
  photo MEDIUMBLOB COMMENT 'A picture to represent the person',
  localAddress TEXT COMMENT 'The exact location of the person from the district',
  username VARCHAR(100) NOT NULL COMMENT 'The username the person choose to log into the system',
  password VARCHAR(100) NOT NULL COMMENT 'The password the person needs to log into the system',
  genderId BIGINT NOT NULL COMMENT 'The gender the person identifies with',
  typeId BIGINT NOT NULL COMMENT 'The type of document the person uses',
  districtId BIGINT NOT NULL COMMENT 'The district in which the person lives',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS telephone;
CREATE TABLE telephone (
  telephoneId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the telephone',
  phoneNumber VARCHAR(100) NOT NULL COMMENT 'The phone number of the person',
  personId BIGINT NOT NULL COMMENT 'The person that owns this phone',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS email;
CREATE TABLE email (
  emailId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the email',
  emailAddress VARCHAR(100) NOT NULL COMMENT 'The email route for contact',
  personId BIGINT NOT NULL COMMENT 'The person that owns this email',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS admin;
CREATE TABLE admin (
  adminId BIGINT PRIMARY KEY COMMENT 'The shared id of the person that is admin',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS author;
CREATE TABLE author (
  authorId BIGINT PRIMARY KEY COMMENT 'The shared id of the person that is an author',
  authorTypeId BIGINT NOT NULL COMMENT 'The type of author that this person is',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS digitalNewspaper;
CREATE TABLE digitalNewspaper (
  newspaperId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the newspaper',
  newspaperName VARCHAR(100) NOT NULL COMMENT 'The name of the newspaper',
  committeeId BIGINT NOT NULL COMMENT 'The committee in which the newspaper belongs',
  universityId BIGINT NOT NULL COMMENT 'The university in which the newspaper belongs',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS store;
CREATE TABLE store (
  storeId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the store',
  description VARCHAR(100) NOT NULL COMMENT 'The name of the store',
  newspaperId BIGINT NOT NULL COMMENT 'The newspaper in which the store belongs',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS product;
CREATE TABLE product (
  productId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the product',
  name VARCHAR(100) NOT NULL COMMENT 'The name of the product',
  pointsPrice INT NOT NULL COMMENT 'The cost of the product in points',
  numberInStock INT NOT NULL COMMENT 'The amount of products that are available',
  storeId BIGINT NOT NULL COMMENT 'The store that distributes say product',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS purchase;
CREATE TABLE purchase (
  purchaseId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the purchase',
  personId BIGINT NOT NULL COMMENT 'The person who made the purchase',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS purchaseXproduct;
CREATE TABLE purchaseXproduct (
  purchaseId BIGINT NOT NULL COMMENT 'The id of the purchase done',
  productId BIGINT NOT NULL COMMENT 'The id of the product bought',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified',
  PRIMARY KEY (purchaseId, productId)
);

DROP TABLE IF EXISTS personXnewspaper;
CREATE TABLE personXnewspaper (
  newspaperId BIGINT NOT NULL COMMENT 'The newspaper the person pick up',
  personId BIGINT NOT NULL COMMENT 'The person that is watching the newspaper',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified',
  PRIMARY KEY (newspaperId, personId)
);

DROP TABLE IF EXISTS personXcommittee;
CREATE TABLE personXcommittee (
  personId BIGINT NOT NULL COMMENT 'The person that belongs to a committee',
  committeeId BIGINT NOT NULL COMMENT 'The committee in which the person belongs',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified',
  PRIMARY KEY (personId, committeeId)
);

DROP TABLE IF EXISTS newsArticle;
CREATE TABLE newsArticle (
  articleId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the article',
  title VARCHAR(100) NOT NULL COMMENT 'The title of the article',
  content TEXT NOT NULL COMMENT 'The text of the article',
  publicationDate DATETIME COMMENT 'The date the article was allow to the public',
  photo MEDIUMBLOB COMMENT 'An optional picture attach to the article',
  authorId BIGINT NOT NULL COMMENT 'The author that writed the article',
  articleTypeId BIGINT NOT NULL COMMENT 'The type of article',
  newspaperId BIGINT NOT NULL COMMENT 'The newspaper in which the article belongs',
  committeeIdApprov BIGINT COMMENT 'The committee that approved the article',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

DROP TABLE IF EXISTS pXcommentXArticle;
CREATE TABLE pXcommentXArticle (
  personId BIGINT NOT NULL COMMENT 'The person who made the comment',
  articleId BIGINT NOT NULL COMMENT 'The article in which the comment was written',
  comments VARCHAR(250) NOT NULL COMMENT 'The response to the article',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified',
  PRIMARY KEY (personId, ArticleId)
);

DROP TABLE IF EXISTS pXfavoriteXArticle;
CREATE TABLE pXfavoriteXArticle (
  personId BIGINT NOT NULL COMMENT 'The person who favorite the article',
  articleId BIGINT NOT NULL COMMENT 'The article that was favorite',
  deleted TINYINT COMMENT 'If the favorite was deleted',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified',
  PRIMARY KEY (personId, articleId)
);

DROP TABLE IF EXISTS pXreviewsXarticle;
CREATE TABLE pXreviewsXarticle (
  personId BIGINT NOT NULL COMMENT 'The person who left the review',
  articleId BIGINT NOT NULL COMMENT 'The article with the review',
  nStars FLOAT NOT NULL COMMENT 'The score the person gave',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified',
  PRIMARY KEY (personId, articleId)
);

DROP TABLE IF EXISTS log;
CREATE TABLE log (
  logId BIGINT PRIMARY KEY AUTO_INCREMENT COMMENT 'The internal id of the log',
  fieldName VARCHAR(100) NOT NULL COMMENT 'The field that got modified',
  tableName VARCHAR(100) NOT NULL COMMENT 'The table that got modified',
  currentValue TEXT NOT NULL COMMENT 'The value that now holds',
  previousValue TEXT NOT NULL COMMENT 'The value that was updated',
  articleId BIGINT NOT NULL COMMENT 'The article that was updated',
  creationUser VARCHAR(50) COMMENT 'User who created this row',
  creationDate DATETIME COMMENT 'Date when the row was created',
  lastModifiedUser VARCHAR(50) COMMENT 'Last user who modifed the row',
  lastModifiedDate DATETIME COMMENT 'Date when the row was modified'
);

CREATE UNIQUE INDEX uk_country_name ON country (countryName);

CREATE UNIQUE INDEX uk_province_name_country ON province (countryId, provinceName);

CREATE UNIQUE INDEX uk_canton_name_province ON canton (provinceId, cantonName);

CREATE UNIQUE INDEX uk_district_name_canton ON district (cantonId, districtName);

CREATE UNIQUE INDEX uk_parameter_name ON parameter (name);

CREATE UNIQUE INDEX uk_idType_description ON idType (description);

CREATE UNIQUE INDEX uk_gender_description ON gender (genderName);

CREATE UNIQUE INDEX uk_authorType_description ON authorType (description);

CREATE UNIQUE INDEX uk_newsArticleType_description ON newsArticleType (description);

CREATE UNIQUE INDEX uk_committee_description ON committee (description);

CREATE UNIQUE INDEX uk_university_name ON university (universityName);

CREATE UNIQUE INDEX uk_person_idCard ON person (idCard);

CREATE UNIQUE INDEX uk_person_username ON person (username);

CREATE UNIQUE INDEX uk_digitalNewspaper_name_committee_university ON digitalNewspaper (newspaperName, committeeId, universityId);

CREATE UNIQUE INDEX uk_store_description_newspaper ON store (description, newspaperId);

CREATE UNIQUE INDEX uk_product_name_store ON product (name, storeId);

ALTER TABLE country COMMENT = 'Representes a country';

ALTER TABLE province COMMENT = 'Represents a province or state in a country';

ALTER TABLE canton COMMENT = 'Represents a canton or city inside a province/state';

ALTER TABLE district COMMENT = 'Represents a district in a canton/city';

ALTER TABLE parameter COMMENT = 'Parameters for internal use in the system';

ALTER TABLE idType COMMENT = 'The types of identifications';

ALTER TABLE gender COMMENT = 'The gender a person can have';

ALTER TABLE authorType COMMENT = 'Represents a type of author';

ALTER TABLE newsArticleType COMMENT = 'Represents a type of article';

ALTER TABLE committee COMMENT = 'Represents a committee';

ALTER TABLE university COMMENT = 'represents an university';

ALTER TABLE person COMMENT = 'Represents a person inside the system';

ALTER TABLE telephone COMMENT = 'Represents a phone';

ALTER TABLE email COMMENT = 'Represents a person\'s email';

ALTER TABLE admin COMMENT = 'Represents a system administrator';

ALTER TABLE author COMMENT = 'Represents an author';

ALTER TABLE digitalNewspaper COMMENT = 'Represents a digital newspaper';

ALTER TABLE store COMMENT = 'Represents a store';

ALTER TABLE product COMMENT = 'Represents a product';

ALTER TABLE purchase COMMENT = 'Represents a purchase';

ALTER TABLE purchaseXproduct COMMENT = 'Represents the products in a purchase';

ALTER TABLE personXnewspaper COMMENT = 'Represents a person subscribed to a news paper';

ALTER TABLE personXcommittee COMMENT = 'Represents a person that joined a committee';

ALTER TABLE newsArticle COMMENT = 'Represents an article';

ALTER TABLE pXcommentXArticle COMMENT = 'Represents a comment';

ALTER TABLE pXfavoriteXArticle COMMENT = 'Represents a favorite';

ALTER TABLE pXreviewsXarticle COMMENT = 'Represents a review';

ALTER TABLE log COMMENT = 'Represents the changelog of an article';

ALTER TABLE province ADD CONSTRAINT fk_province_country FOREIGN KEY (countryId) REFERENCES country (countryId);

ALTER TABLE canton ADD CONSTRAINT fk_canton_province FOREIGN KEY (provinceId) REFERENCES province (provinceId);

ALTER TABLE district ADD CONSTRAINT fk_district_canton FOREIGN KEY (cantonId) REFERENCES canton (cantonId);

ALTER TABLE university ADD CONSTRAINT fk_university_district FOREIGN KEY (districtId) REFERENCES district (districtId);

ALTER TABLE person ADD CONSTRAINT fk_person_gender FOREIGN KEY (genderId) REFERENCES gender (genderId);

ALTER TABLE person ADD CONSTRAINT fk_person_typeId FOREIGN KEY (typeId) REFERENCES idType (typeId);

ALTER TABLE person ADD CONSTRAINT fk_person_district FOREIGN KEY (districtId) REFERENCES district (districtId);

ALTER TABLE telephone ADD CONSTRAINT fk_telephone_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE email ADD CONSTRAINT fk_email_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE admin ADD CONSTRAINT fk_admin_person FOREIGN KEY (adminId) REFERENCES person (personId);

ALTER TABLE author ADD CONSTRAINT fk_author_person FOREIGN KEY (authorId) REFERENCES person (personId);

ALTER TABLE digitalNewspaper ADD CONSTRAINT fk_newspaper_committee FOREIGN KEY (committeeId) REFERENCES committee (committeeId);

ALTER TABLE digitalNewspaper ADD CONSTRAINT fk_newspaper_university FOREIGN KEY (universityId) REFERENCES university (universityId);

ALTER TABLE store ADD CONSTRAINT fk_store_newspaper FOREIGN KEY (newspaperId) REFERENCES digitalNewspaper (newspaperId);

ALTER TABLE product ADD CONSTRAINT fk_product_store FOREIGN KEY (storeId) REFERENCES store (storeId);

ALTER TABLE purchase ADD CONSTRAINT fk_purchase_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE purchaseXproduct ADD CONSTRAINT fk_pxp_purchase FOREIGN KEY (purchaseId) REFERENCES purchase (purchaseId);

ALTER TABLE purchaseXproduct ADD CONSTRAINT fk_pxp_product FOREIGN KEY (productId) REFERENCES product (productId);

ALTER TABLE personXnewspaper ADD CONSTRAINT fk_pxn_newspaper FOREIGN KEY (newspaperId) REFERENCES digitalNewspaper (newspaperId);

ALTER TABLE personXnewspaper ADD CONSTRAINT fk_pxn_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE newsArticle ADD CONSTRAINT fk_article_author FOREIGN KEY (authorId) REFERENCES author (authorId);

ALTER TABLE newsArticle ADD CONSTRAINT fk_article_type FOREIGN KEY (articleTypeId) REFERENCES newsArticleType (typeId);

ALTER TABLE newsArticle ADD CONSTRAINT fk_article_newspaper FOREIGN KEY (newspaperId) REFERENCES digitalNewspaper (newspaperId);

ALTER TABLE newsArticle ADD CONSTRAINT fk_article_committee FOREIGN KEY (committeeIdApprov) REFERENCES committee (committeeId);

ALTER TABLE pXcommentXArticle ADD CONSTRAINT fk_comment_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE pXcommentXArticle ADD CONSTRAINT fk_comment_article FOREIGN KEY (articleId) REFERENCES newsArticle (articleId);

ALTER TABLE pXfavoriteXArticle ADD CONSTRAINT p_favorite_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE pXfavoriteXArticle ADD CONSTRAINT p_favorite_article FOREIGN KEY (articleId) REFERENCES newsArticle (articleId);

ALTER TABLE pXreviewsXarticle ADD CONSTRAINT fk_review_person FOREIGN KEY (personId) REFERENCES person (personId);

ALTER TABLE pXreviewsXarticle ADD CONSTRAINT fk_review_article FOREIGN KEY (articleId) REFERENCES newsArticle (articleId);

ALTER TABLE log ADD CONSTRAINT fk_log_article FOREIGN KEY (articleId) REFERENCES newsArticle (articleId);
