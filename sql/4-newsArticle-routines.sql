DELIMITER $$

DROP FUNCTION IF EXISTS createNewsArticle$$
CREATE FUNCTION createNewsArticle(
    p_title VARCHAR(100),
    p_content TEXT,
    p_publicationDate DATETIME,
    p_photo MEDIUMBLOB,
    p_authorId BIGINT,
    p_articleTypeId BIGINT,
    p_newsPaperId BIGINT,
    p_committeeIdApprov BIGINT
)
RETURNS BIGINT
BEGIN
    DECLARE newsArticleId BIGINT DEFAULT 0;

    INSERT INTO newsArticle(
        title,
        content,
        publicationDate,
        photo,
        authorId,
        articleTypeId,
        newsPaperId,
        committeeIdApprov
    )
    VALUES(
        p_title,
        p_content,
        p_publicationDate,
        p_photo,
        p_authorId,
        p_articleTypeId,
        p_newsPaperId,
        p_committeeIdApprov
    );

    SET newsArticleId = LAST_INSERT_ID();

    RETURN newsArticleId;
END$$

DROP PROCEDURE IF EXISTS updateNewsArticle$$
CREATE PROCEDURE updateNewsArticle(
    p_articleId BIGINT,
    p_title VARCHAR(100),
    p_content TEXT,
    p_publicationDate DATETIME,
    p_photo MEDIUMBLOB,
    p_authorId BIGINT,
    p_articleTypeId BIGINT,
    p_newsPaperId BIGINT,
    p_committeeIdApprov BIGINT
)
BEGIN
    UPDATE newsArticle
    SET title = p_title,
        content = p_content,
        publicationDate = p_publicationDate,
        photo = p_photo,
        authorId = p_authorId,
        articleTypeId = p_articleTypeId,
        newsPaperId = p_newsPaperId,
        committeeIdApprov = p_committeeIdApprov
    WHERE articleId = p_articleId;
END$$

DROP PROCEDURE IF EXISTS deleteNewsArticle$$
CREATE PROCEDURE deleteNewsArticle(
    p_articleId BIGINT
)
BEGIN
    DELETE FROM newsArticle
    WHERE articleId = p_articleId;
END$$

DROP PROCEDURE IF EXISTS getNewsArticle$$
CREATE PROCEDURE getNewsArticle(
    p_articleId BIGINT
)
BEGIN
    SELECT articleId,
           title,
           content,
           publicationDate,
           photo,
           authorId,
           articleTypeId,
           newsPaperId,
           committeeIdApprov
    FROM newsArticle
    WHERE articleId = COALESCE(p_articleId, articleId);
END$$

DROP PROCEDURE IF EXISTS getLatestNewsPaginated$$
CREATE PROCEDURE getLatestNewsPaginated(pageNumber INT, p_newsPaperId BIGINT)
BEGIN

    DECLARE pageOffset INT;

    DECLARE newsQuantity INT DEFAULT 3;

    SELECT CAST(value AS SIGNED)
    INTO newsQuantity
    FROM parameter
    WHERE name = 'newsPaginationQuantity';

    SET pageOffset = COALESCE(pageNumber, 0) * newsQuantity;
    
    SELECT articleId,
           title,
           content,
           publicationDate,
           photo,
           authorId,
           articleTypeId,
           newsPaperId,
           committeeIdApprov
    FROM newsArticle
    WHERE newsPaperId = p_newsPaperId AND committeeIdApprov IS NOT NULL
    ORDER BY PUBLICATIONDATE DESC
    LIMIT pageOffset, newsQuantity;
END$$

DROP FUNCTION IF EXISTS getLatestNewsPages$$
CREATE FUNCTION getLatestNewsPages(p_newsPaperId BIGINT)
RETURNS INT
BEGIN
    DECLARE pages INT DEFAULT 0;
    DECLARE newsCount INT DEFAULT 0;
    DECLARE newsQuantity INT DEFAULT 3;

    SELECT CAST(value AS SIGNED)
    INTO newsQuantity
    FROM parameter
    WHERE name = 'newsPaginationQuantity';

    SELECT count(1)
    INTO newsCount
    FROM newsArticle
    WHERE newsPaperId = p_newsPaperId AND committeeIdApprov IS NOT NULL;

    SET pages = newsCount / newsQuantity;

    RETURN pages;

END$$

DROP PROCEDURE IF EXISTS approveNewsarticle$$
CREATE PROCEDURE approveNewsarticle(
    p_articleId BIGINT,
    p_committeeId BIGINT
)
BEGIN
    UPDATE newsArticle
    SET committeeIdApprov = p_committeeId,
        publicationDate = NOW()
    WHERE articleId = p_articleId;
END$$

DROP PROCEDURE IF EXISTS getNewsArticlePending$$
CREATE PROCEDURE getNewsArticlePending(
    pageNumber INTEGER,
    p_newsPaperId BIGINT
)
BEGIN
    DECLARE pageOffset INT;

    DECLARE newsQuantity INT DEFAULT 3;

    SELECT CAST(value AS SIGNED)
    INTO newsQuantity
    FROM parameter
    WHERE name = 'newsPaginationQuantity';

    SET pageOffset = COALESCE(pageNumber, 0) * newsQuantity;

    SELECT articleId,
           title,
           content,
           publicationDate,
           photo,
           authorId,
           articleTypeId,
           newsPaperId,
           committeeIdApprov
    FROM newsArticle
    WHERE newsPaperId = COALESCE(p_newsPaperId, newsPaperId)
        AND committeeIdApprov IS NULL
        LIMIT pageOffset, newsQuantity;
END$$

DROP FUNCTION IF EXISTS getNewsArticlePendingPages$$
CREATE FUNCTION getNewsArticlePendingPages(p_newsPaperId BIGINT)
RETURNS INT
BEGIN
    DECLARE pages INT DEFAULT 0;
    DECLARE newsCount INT DEFAULT 0;
    DECLARE newsQuantity INT DEFAULT 3;

    SELECT CAST(value AS SIGNED)
    INTO newsQuantity
    FROM parameter
    WHERE name = 'newsPaginationQuantity';

    SELECT count(1)
    INTO newsCount
    FROM newsArticle
    WHERE newsPaperId = COALESCE(p_newsPaperId, newsPaperId)
        AND committeeIdApprov IS NULL;

    SET pages = newsCount / newsQuantity;

    RETURN pages;

END$$

DELIMITER ;
