DELIMITER $$

DROP FUNCTION IF EXISTS createNewsarticletype$$
CREATE FUNCTION createNewsarticletype(
    p_description VARCHAR(100)
)
RETURNS BIGINT
BEGIN
    DECLARE newsArticleTypeId BIGINT DEFAULT 0;

    INSERT INTO newsArticleType (description)
    VALUES (p_description);

    SET newsArticleTypeId = LAST_INSERT_ID();

    RETURN newsArticleTypeId;
END$$

DROP PROCEDURE IF EXISTS updateNewsarticletype$$
CREATE PROCEDURE updateNewsarticletype(p_typeId BIGINT,p_description VARCHAR(100))
BEGIN
    UPDATE newsArticleType
    SET description = p_description
    WHERE typeId = p_typeId;
END$$

DROP PROCEDURE IF EXISTS deleteNewsarticletype$$
CREATE PROCEDURE deleteNewsarticletype(p_typeId BIGINT)
BEGIN
    DELETE FROM newsArticleType
    WHERE typeId = p_typeId;
END$$

DROP PROCEDURE IF EXISTS getNewsarticletype$$
CREATE PROCEDURE getNewsarticletype(p_typeId BIGINT)
BEGIN
    SELECT typeId,
           description
    FROM newsArticleType
    WHERE typeId = COALESCE(p_typeId, typeId);
END$$

DELIMITER ;
