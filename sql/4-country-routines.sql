
DELIMITER $$
DROP FUNCTION IF EXISTS createCountry$$
CREATE FUNCTION createCountry(p_CountryName  VARCHAR(100)) RETURNS BIGINT 
BEGIN
    INSERT INTO Country(CountryName)
    VALUES (p_CountryName);
    return last_insert_id();
END
$$ DELIMITER ;



DELIMITER $$
DROP PROCEDURE IF EXISTS updateCountry$$
CREATE PROCEDURE updateCountry(p_CountryId  BIGINT, p_CountryName  VARCHAR(100)) 
BEGIN
    UPDATE Country
    SET CountryName = p_CountryName
    WHERE CountryId = p_CountryId;
    COMMIT;
END
$$ DELIMITER ;



DELIMITER $$
DROP PROCEDURE IF EXISTS deleteCountry$$
CREATE PROCEDURE deleteCountry(p_CountryId  BIGINT) 
BEGIN
    DELETE FROM Country WHERE CountryId = p_CountryId;
    COMMIT;
END
$$ DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS getCountries$$
CREATE PROCEDURE getCountries(p_CountryId  BIGINT) 
BEGIN
        SELECT CountryId,
               CountryName
        FROM Country
        WHERE CountryId = coalesce(p_CountryId, CountryId);
END
$$ DELIMITER ;
