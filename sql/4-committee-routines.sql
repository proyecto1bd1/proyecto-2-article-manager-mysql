DELIMITER $$

DROP FUNCTION IF EXISTS createCommittee$$
CREATE FUNCTION createCommittee(
    p_description VARCHAR(100)
)
RETURNS BIGINT
BEGIN
    DECLARE committeeId BIGINT DEFAULT 0;

    INSERT INTO committee (description)
    VALUES (p_description);

    SET committeeId = LAST_INSERT_ID();

    RETURN committeeId;
END$$

DROP PROCEDURE IF EXISTS updateCommittee$$
CREATE PROCEDURE updateCommittee(
    p_committeeId BIGINT,
    p_description VARCHAR(100)
)
BEGIN
    UPDATE committee
    SET description = p_description
    WHERE committeeId = p_committeeId;
END$$

DROP PROCEDURE IF EXISTS deleteCommittee$$
CREATE PROCEDURE deleteCommittee(p_committeeId BIGINT)
BEGIN
    DELETE FROM committee
    WHERE committeeId = p_committeeId;
END$$

DROP PROCEDURE IF EXISTS getCommittee$$
CREATE PROCEDURE getCommittee(p_committeeId BIGINT)
BEGIN
    SELECT committeeId, description
    FROM committee
    WHERE committeeId = COALESCE(p_committeeId, committeeId);
END$$

DELIMITER ;
