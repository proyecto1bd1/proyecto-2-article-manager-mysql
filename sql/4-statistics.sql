DELIMITER $$

DROP PROCEDURE IF EXISTS statArticlesXuniversity$$
CREATE PROCEDURE statArticlesXuniversity()
	BEGIN
	SELECT universityName University, count(*) TotalArticles FROM UNIVERSITY a
	INNER JOIN DIGITALNEWSPAPER b
    ON a.universityId = b.universityId
    INNER JOIN NEWSARTICLE c
    ON c.newspaperId = b.newspaperId
    GROUP BY universityName;
    END$$
    
DROP PROCEDURE IF EXISTS statAuthorsXuniversity$$
CREATE PROCEDURE statAuthorsXuniversity()
	BEGIN 
    SELECT universityName University, count(*) TotalAuthors FROM UNIVERSITY a
    INNER JOIN DIGITALNEWSPAPER b
    ON a.universityId = b.universityId
    INNER JOIN PERSONXNEWSPAPER c
    ON c.newspaperId = b.newspaperId
    INNER JOIN PERSON d
    ON d.personId = c.personId
    INNER JOIN AUTHOR e 
    ON e.authorId = d.personId
    GROUP BY universityName;
    END$$
    
DROP PROCEDURE IF EXISTS statArticlesXauthor$$
	CREATE PROCEDURE statArticlesXauthor()
    BEGIN
    SELECT concat(name, " ", firstLastName, " ", secondLastName) AuthorName, count(*) TotalArticles FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN NEWSARTICLE c
    ON c.authorId = b.authorId
    GROUP BY AuthorName ORDER BY TotalArticles DESC;
    END$$

DROP PROCEDURE IF EXISTS statTopNpublishers$$
	CREATE PROCEDURE  statTopNpublishers(p_universityId BIGINT, p_genderId BIGINT, p_newsArticleTypeId BIGINT, p_topN BIGINT)
    BEGIN 
    SELECT concat(name, " ", firstLastName, " ", secondLastName) AuthorName, count(*) TotalArticles FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId =  c.genderId
    INNER JOIN NEWSARTICLE d
    ON d.authorId = b.authorId
    INNER JOIN NEWSARTICLETYPE e
    ON e.typeId = d.articleTypeId
    INNER JOIN DIGITALNEWSPAPER f
    ON d.newspaperId = f.newspaperId
    INNER JOIN UNIVERSITY g
    ON f.universityId = g.universityId
    WHERE a.genderId = coalesce(p_genderId, a.genderId) AND
    g.universityId = coalesce(p_universityId, g.universityId) AND
    e.typeId = coalesce(p_newsArticleTypeId, e.typeId)
    GROUP BY AuthorName
    ORDER BY TotalArticles DESC
    LIMIT p_topN;
    END$$
   
    DROP PROCEDURE IF EXISTS statAvgReviewXauthor$$
		CREATE PROCEDURE  statAvgReviewXauthor()
		BEGIN
		SELECT concat(name, " ", firstLastName, " ", secondLastName) AuthorName, avg(nStars) AverageReview FROM PERSON a
		INNER JOIN AUTHOR b
		ON a.personId = b.authorId
		INNER JOIN NEWSARTICLE c
		ON b.authorId = c.authorId
		INNER JOIN PXREVIEWSXARTICLE d
		ON d.articleId = c.articleId
		GROUP BY AuthorName ORDER BY AverageReview DESC;
		END$$

DROP PROCEDURE IF EXISTS statAuthorsXageRange$$
	CREATE PROCEDURE statAuthorsXageRange(p_universityId BIGINT, p_genderId BIGINT, p_newsArticleTypeId BIGINT)
    BEGIN
    SELECT '0-18' AgeRange, c.genderName Gender, count(distinct b.authorid) TotalAuthors
    FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId = c.genderId
    INNER JOIN PERSONXNEWSPAPER d
    ON a.personId = d.personId
    INNER JOIN DIGITALNEWSPAPER e
    ON d.newspaperId = e.newspaperId
    INNER JOIN UNIVERSITY f
    ON f.universityId = e.universityId
    INNER JOIN NEWSARTICLE g
    ON g.newspaperId = e.newspaperId
    INNER JOIN NEWSARTICLETYPE h
    ON h.typeid = g.articletypeid
    WHERE (TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 0 AND 18) AND
    f.universityId = coalesce(p_universityId, f.universityId) AND
    c.genderId = coalesce(p_genderId, c.genderId) AND
    h.typeId = coalesce(p_newsArticleTypeId, h.typeId)
    GROUP BY AgeRange, Gender
    
    UNION
    
    SELECT '19-30' AgeRange, c.genderName Gender, count(distinct b.authorid) TotalAuthors
    FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId = c.genderId
    INNER JOIN PERSONXNEWSPAPER d
    ON a.personId = d.personId
    INNER JOIN DIGITALNEWSPAPER e
    ON d.newspaperId = e.newspaperId
    INNER JOIN UNIVERSITY f
    ON f.universityId = e.universityId
    INNER JOIN NEWSARTICLE g
    ON g.newspaperId = e.newspaperId
    INNER JOIN NEWSARTICLETYPE h
    ON h.typeid = g.articletypeid
    WHERE (TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 19 AND 30) AND
    f.universityId = coalesce(p_universityId, f.universityId) AND
    c.genderId = coalesce(p_genderId, c.genderId) AND
    h.typeId = coalesce(p_newsArticleTypeId, h.typeId)
    GROUP BY AgeRange, Gender
    
    UNION
    
    SELECT '31-45' AgeRange, c.genderName Gender, count(distinct b.authorid) TotalAuthors
    FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId = c.genderId
    INNER JOIN PERSONXNEWSPAPER d
    ON a.personId = d.personId
    INNER JOIN DIGITALNEWSPAPER e
    ON d.newspaperId = e.newspaperId
    INNER JOIN UNIVERSITY f
    ON f.universityId = e.universityId
    INNER JOIN NEWSARTICLE g
    ON g.newspaperId = e.newspaperId
    INNER JOIN NEWSARTICLETYPE h
    ON h.typeid = g.articletypeid
    WHERE (TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 31 AND 45) AND
    f.universityId = coalesce(p_universityId, f.universityId) AND
    c.genderId = coalesce(p_genderId, c.genderId) AND
    h.typeId = coalesce(p_newsArticleTypeId, h.typeId)
    GROUP BY AgeRange, Gender
    
    UNION
    
    SELECT '46-60' AgeRange, c.genderName Gender, count(distinct b.authorid) TotalAuthors
    FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId = c.genderId
    INNER JOIN PERSONXNEWSPAPER d
    ON a.personId = d.personId
    INNER JOIN DIGITALNEWSPAPER e
    ON d.newspaperId = e.newspaperId
    INNER JOIN UNIVERSITY f
    ON f.universityId = e.universityId
    INNER JOIN NEWSARTICLE g
    ON g.newspaperId = e.newspaperId
    INNER JOIN NEWSARTICLETYPE h
    ON h.typeid = g.articletypeid
    WHERE (TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 46 AND 60) AND
    f.universityId = coalesce(p_universityId, f.universityId) AND
    c.genderId = coalesce(p_genderId, c.genderId) AND
    h.typeId = coalesce(p_newsArticleTypeId, h.typeId)
    GROUP BY AgeRange, Gender
    
	UNION
    
    SELECT '61-75' AgeRange, c.genderName Gender, count(distinct b.authorid) TotalAuthors
    FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId = c.genderId
    INNER JOIN PERSONXNEWSPAPER d
    ON a.personId = d.personId
    INNER JOIN DIGITALNEWSPAPER e
    ON d.newspaperId = e.newspaperId
    INNER JOIN UNIVERSITY f
    ON f.universityId = e.universityId
    INNER JOIN NEWSARTICLE g
    ON g.newspaperId = e.newspaperId
    INNER JOIN NEWSARTICLETYPE h
    ON h.typeid = g.articletypeid
    WHERE (TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) BETWEEN 61 AND 75) AND
    f.universityId = coalesce(p_universityId, f.universityId) AND
    c.genderId = coalesce(p_genderId, c.genderId) AND
    h.typeId = coalesce(p_newsArticleTypeId, h.typeId)
    GROUP BY AgeRange, Gender
    
    UNION
    
    SELECT 'Over 75' AgeRange, c.genderName Gender, count(distinct b.authorid) TotalAuthors
    FROM PERSON a
    INNER JOIN AUTHOR b
    ON a.personId = b.authorId
    INNER JOIN GENDER c
    ON a.genderId = c.genderId
    INNER JOIN PERSONXNEWSPAPER d
    ON a.personId = d.personId
    INNER JOIN DIGITALNEWSPAPER e
    ON d.newspaperId = e.newspaperId
    INNER JOIN UNIVERSITY f
    ON f.universityId = e.universityId
    INNER JOIN NEWSARTICLE g
    ON g.newspaperId = e.newspaperId
    INNER JOIN NEWSARTICLETYPE h
    ON h.typeid = g.articletypeid
    WHERE (TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) > 75) AND
    f.universityId = coalesce(p_universityId, f.universityId) AND
    c.genderId = coalesce(p_genderId, c.genderId) AND
    h.typeId = coalesce(p_newsArticleTypeId, h.typeId)
    GROUP BY AgeRange, Gender;
    END$$
    
DROP PROCEDURE IF EXISTS statMostPurchasedProducts$$
	CREATE PROCEDURE statMostPurchasedProducts(topNpurchasedProducts BIGINT)
    BEGIN
    SELECT a.name ProductName, count(*) Purchases
    FROM PRODUCT a
    INNER JOIN PURCHASEXPRODUCT b
    ON a.productId = b.productId
    GROUP BY ProductName ORDER BY Purchases DESC LIMIT topNpurchasedProducts;
    END$$
DELIMITER ;  
    

	

    
    

    
    
    
    
  

    
    
    


    

