DELIMITER $$

DROP FUNCTION IF EXISTS createAuthorType$$
CREATE FUNCTION createAuthorType(
    p_description VARCHAR(100)
)
RETURNS BIGINT
BEGIN
    DECLARE authorId BIGINT DEFAULT 0;

    INSERT INTO authorType (description)
    VALUES (p_description);
    SET authorId = LAST_INSERT_ID();

    RETURN authorId;
END$$

DROP PROCEDURE IF EXISTS updateAuthorType$$
CREATE PROCEDURE updateAuthorType(
    p_authorTypeId BIGINT,
    p_description VARCHAR(100)
)
BEGIN
    UPDATE authorType
    SET description = p_description
    WHERE authorTypeId = p_authorTypeId;
END$$


DROP PROCEDURE IF EXISTS deleteAuthorType$$
CREATE PROCEDURE deleteAuthorType(
    p_authorTypeId BIGINT
)
BEGIN
    DELETE FROM authorType
    WHERE authorTypeId = p_authorTypeId;
END$$


DROP PROCEDURE IF EXISTS getAuthorType$$
CREATE PROCEDURE getAuthorType(
    p_authorTypeId BIGINT
)
BEGIN
    SELECT authorTypeId, description
    FROM authorType
    WHERE authorTypeId = COALESCE(p_authorTypeId, authorTypeId);
END$$

DELIMITER ;
