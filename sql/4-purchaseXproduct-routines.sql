DELIMITER $$

DROP PROCEDURE IF EXISTS createPurchasexproduct$$
CREATE PROCEDURE createPurchasexproduct(p_PurchaseId  BIGINT, p_ProductId  BIGINT) 
BEGIN
INSERT INTO PURCHASEXPRODUCT (PurchaseId,ProductId)
VALUES (p_PurchaseId, p_ProductId);
END$$

DROP PROCEDURE IF EXISTS deletePurchasexproduct$$
CREATE PROCEDURE deletePurchasexproduct(p_PurchaseId  BIGINT, p_ProductId  BIGINT) 
BEGIN
DELETE FROM PURCHASEXPRODUCT
WHERE PurchaseId = p_PurchaseId AND ProductId = p_ProductId ;
COMMIT;
END$$


DROP PROCEDURE IF EXISTS getPurchasexproduct$$
CREATE PROCEDURE getPurchasexproduct(p_PurchaseId  BIGINT, p_ProductId  BIGINT) 
BEGIN
SELECT PurchaseId,ProductId
FROM PURCHASEXPRODUCT
WHERE PurchaseId = coalesce(p_PurchaseId, PurchaseId) AND ProductId = coalesce (p_ProductId, ProductId) ;
END$$

DELIMITER ;
