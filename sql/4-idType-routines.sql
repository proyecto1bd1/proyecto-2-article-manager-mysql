DELIMITER $$

DROP FUNCTION IF EXISTS createIdtype$$
CREATE FUNCTION createIdtype(
    p_description VARCHAR(100),
    p_mask VARCHAR(100)
)
RETURNS BIGINT
BEGIN
    DECLARE typeId BIGINT DEFAULT 0;

    INSERT INTO idType (description, mask)
    VALUES (p_description, p_mask);

    SET typeId = LAST_INSERT_ID();

    RETURN typeId;
END$$

DROP PROCEDURE IF EXISTS updateIdtype$$
CREATE PROCEDURE updateIdtype(
    p_typeId BIGINT,
    p_description VARCHAR(100),
    p_mask VARCHAR(100)
)
BEGIN
    UPDATE idType
    SET description = p_description,
        mask = p_mask
    WHERE typeId = p_typeId;
END$$

DROP PROCEDURE IF EXISTS deleteIdtype$$
CREATE PROCEDURE deleteIdtype(
    p_typeId BIGINT
)
BEGIN
    DELETE FROM idType
    WHERE typeId = p_typeId;
END$$

DROP PROCEDURE IF EXISTS getIdtype$$
CREATE PROCEDURE getIdtype(
    p_typeId BIGINT
)
BEGIN
    SELECT typeId,
           description,
           mask
    FROM idType
    WHERE typeId = COALESCE(p_typeId, typeId);
END$$

DELIMITER ;
