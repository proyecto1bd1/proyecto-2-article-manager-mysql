DELIMITER $$

DROP FUNCTION IF EXISTS createCanton$$
CREATE FUNCTION createCanton(
    p_cantonName VARCHAR(100),
    p_provinceId BIGINT
)
RETURNS BIGINT
BEGIN
    DECLARE cantonId BIGINT DEFAULT 0;

    INSERT INTO canton (cantonName, provinceId)
    VALUES (p_cantonName, p_provinceId);
    
    SET cantonId = LAST_INSERT_ID();

    RETURN cantonId;
END$$

DROP PROCEDURE IF EXISTS updateCanton$$
CREATE PROCEDURE updateCanton(
    p_cantonId BIGINT,
    p_cantonName VARCHAR(100),
    p_provinceId BIGINT
)
BEGIN
    UPDATE canton
    SET cantonName = p_cantonName,
        provinceId = p_provinceId
    WHERE cantonId = p_cantonId;
END$$

DROP PROCEDURE IF EXISTS deleteCanton$$
CREATE PROCEDURE deleteCanton(p_cantonId BIGINT)
BEGIN
    DELETE FROM canton WHERE cantonId = p_cantonId;
END$$

DROP PROCEDURE IF EXISTS getCantons$$
CREATE PROCEDURE getCantons(p_cantonId BIGINT, p_provinceId BIGINT)
BEGIN
    SELECT cantonId,
           cantonName,
           provinceId
    FROM canton
    WHERE cantonId = COALESCE(p_cantonId, cantonId)
    AND provinceId = COALESCE(p_provinceId, provinceId);
END$$

DELIMITER ;