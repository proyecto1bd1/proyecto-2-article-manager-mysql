DELIMITER $$
DROP FUNCTION IF EXISTS createPersonxcommittee$$
CREATE PROCEDURE createPersonxcommittee(p_PersonId  BIGINT, p_CommitteeId  BIGINT) 
BEGIN
INSERT INTO PERSONXCOMMITTEE (PersonId,CommitteeId)
VALUES (p_PersonId, p_CommitteeId);
COMMIT;
END
$$



DROP PROCEDURE IF EXISTS deletePersonxcommittee$$
CREATE PROCEDURE deletePersonxcommittee(p_PersonId  BIGINT, p_CommitteeId  BIGINT) 
BEGIN
DELETE FROM PERSONXCOMMITTEE
WHERE PersonId = p_PersonId AND CommitteeId = p_CommitteeId;
COMMIT;
END
$$

DROP PROCEDURE IF EXISTS getPersonxcommittee$$
CREATE PROCEDURE getPersonxcommittee(p_PersonId  BIGINT, p_CommitteeId  BIGINT) 
BEGIN
SELECT PersonId,CommitteeId
FROM PERSONXCOMMITTEE
WHERE PersonId = coalesce(p_PersonId, PersonId) AND CommitteeId = coalesce(p_CommitteeId,CommitteeId);
END$$

DROP PROCEDURE IF EXISTS getCommitteesOfPerson$$
CREATE PROCEDURE getCommitteesOfPerson(p_PersonId  BIGINT) 
BEGIN
SELECT PersonId, CommitteeId
FROM PERSONXCOMMITTEE
WHERE PersonId = coalesce(p_PersonId, PersonId);
END$$

DELIMITER ;












