DELIMITER $$

DROP FUNCTION IF EXISTS createProvince$$
CREATE FUNCTION createProvince(p_ProvinceName  VARCHAR(100), p_CountryId  BIGINT) RETURNS BIGINT 
BEGIN
    INSERT INTO Province(ProvinceName, CountryId)
    VALUES (p_ProvinceName, p_CountryId);
    RETURN last_insert_id();
END$$


DROP PROCEDURE IF EXISTS updateProvince$$
CREATE PROCEDURE updateProvince(p_ProvinceId  BIGINT, p_ProvinceName  VARCHAR(100), p_CountryId  BIGINT) 
BEGIN
    UPDATE Province
    SET ProvinceName = p_ProvinceName,
        CountryId = p_CountryId
    WHERE ProvinceId = p_ProvinceId;
    COMMIT;
END$$

DROP PROCEDURE IF EXISTS deleteProvince$$
CREATE PROCEDURE deleteProvince(p_ProvinceId  BIGINT) 
BEGIN
    DELETE FROM Province WHERE ProvinceId = p_ProvinceId;
    COMMIT;
END$$

DROP PROCEDURE IF EXISTS getProvinces$$
CREATE PROCEDURE getProvinces(p_ProvinceId  BIGINT, p_CountryId  BIGINT) 
BEGIN
        SELECT ProvinceId,
               ProvinceName,
               CountryId
        FROM Province
        WHERE ProvinceId = coalesce(p_ProvinceId, ProvinceId)
        AND CountryId = coalesce(p_CountryId, CountryId);
END$$

DELIMITER ;
