DELIMITER $$
DROP FUNCTION IF EXISTS updateParameter$$
CREATE PROCEDURE updateParameter(p_ParameterId  BIGINT,p_Description  VARCHAR(100),p_Value  VARCHAR(100)) 
BEGIN
    UPDATE PARAMETER SET
    Description = p_Description,
    Value = p_Value
    WHERE ParameterId = p_ParameterId;
    COMMIT;
END
$$

DROP PROCEDURE IF EXISTS getParameter$$
CREATE PROCEDURE getParameter(p_ParameterId  BIGINT) 
BEGIN
    SELECT ParameterId,Name,Description,Value
    FROM PARAMETER
    WHERE ParameterId = coalesce(p_ParameterId, ParameterId);
END
$$ DELIMITER ;




