CREATE TABLE TopArticlesReport(
	reportId BIGINT PRIMARY KEY AUTO_INCREMENT,
    articleId BIGINT,
	articleTitle VARCHAR(100),
    publicationDate DATETIME,
    favoriteCount BIGINT,
    reportDate DATETIME
    );

delimiter $$

CREATE EVENT IF NOT EXISTS saveMostFavoriteArticles
ON SCHEDULE EVERY '1' DAY
STARTS '2022-11-14 01:00:00'
DO
BEGIN
Declare p_id BIGINT;
Declare p_title VARCHAR(100);
Declare p_date DATETIME;
Declare p_favorite BIGINT;
Declare topArticles
	CURSOR FOR 
		SELECT a.articleId, title, publicationDate, count(*) totalFavorite 
		FROM newsarticle a
		INNER JOIN pxfavoritexarticle b
		ON a.articleid = b.articleid
		GROUP BY title, publicationDate ORDER BY totalFavorite DESC
		LIMIT 3;
OPEN topArticles;
    LOOP
    FETCH topArticles INTO p_id, p_title, p_date, p_favorite;
		INSERT INTO TopArticlesReport(articleId,articleTitle, publicationDate, favoriteCount, reportDate)
        VALUES (p_id, p_title, p_date, p_favorite, NOW());
	END LOOP;
    CLOSE topArticles;
    END$$

delimiter ;


