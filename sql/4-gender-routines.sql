DELIMITER $$

DROP FUNCTION IF EXISTS createGender$$
CREATE FUNCTION createGender(
    p_genderName VARCHAR(100)
)
RETURNS BIGINT
BEGIN
    DECLARE genderId BIGINT DEFAULT 0;

    INSERT INTO gender(genderName)
    VALUES (p_genderName);

    SET genderId = LAST_INSERT_ID();

    RETURN genderId;
END$$

DROP PROCEDURE IF EXISTS updateGender$$
CREATE PROCEDURE updateGender(p_genderId BIGINT, p_genderName VARCHAR(100))
BEGIN
    UPDATE gender
    SET genderName = p_genderName
    WHERE genderId = p_genderId;
END$$

DROP PROCEDURE IF EXISTS deleteGender$$
CREATE PROCEDURE deleteGender(p_genderId BIGINT)
BEGIN
    DELETE FROM gender
    WHERE genderId = p_genderId;
END$$

DROP PROCEDURE IF EXISTS getGenders$$
CREATE PROCEDURE getGenders(p_genderId BIGINT)
BEGIN
    SELECT genderId,
           genderName
    FROM gender
    WHERE genderId = COALESCE(p_genderId, genderId);
END$$

DELIMITER ;
