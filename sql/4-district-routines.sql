DELIMITER $$

DROP FUNCTION IF EXISTS createDistrict$$
CREATE FUNCTION createDistrict(
    p_districtName VARCHAR(100),
    p_cantonId BIGINT
)
RETURNS BIGINT
BEGIN
    DECLARE districtId BIGINT DEFAULT 0;

    INSERT INTO district(districtName, cantonId)
    VALUES (p_districtName, p_cantonId);

    SET districtId = LAST_INSERT_ID();

    RETURN districtId;
END$$

DROP PROCEDURE IF EXISTS updateDistrict$$
CREATE PROCEDURE updateDistrict(
    p_districtId BIGINT,
    p_districtName VARCHAR(100),
    p_cantonId BIGINT
)
BEGIN
    UPDATE district
    SET districtName = p_districtName,
        cantonId = p_cantonId
    WHERE districtId = p_districtId;
END$$

DROP PROCEDURE IF EXISTS deleteDistrict$$
CREATE PROCEDURE deleteDistrict(
    p_districtId BIGINT
)
BEGIN
    DELETE FROM district
    WHERE districtId = p_districtId;
END$$

DROP PROCEDURE IF EXISTS getDistricts$$
CREATE PROCEDURE getDistricts(
    p_districtId BIGINT,
    p_cantonId BIGINT
)
BEGIN
    SELECT districtId,
           districtName,
           cantonId
    FROM district
    WHERE districtId = COALESCE(p_districtId, districtId)
    AND cantonId = COALESCE(p_cantonId, cantonId);
END$$

DELIMITER ;
