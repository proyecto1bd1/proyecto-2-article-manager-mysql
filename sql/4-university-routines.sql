DELIMITER $$
DROP FUNCTION IF EXISTS createUniversity$$
CREATE FUNCTION createUniversity(
    p_UniversityName varchar(150), 
    p_LocalAddress varchar(150), 
    p_DistrictId bigint) RETURNS bigint
BEGIN
    INSERT INTO University(UniversityName, LocalAddress, DistrictId)
    VALUES (p_UniversityName, p_LocalAddress, p_DistrictId);
    return last_insert_id();
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updateUniversity$$
CREATE PROCEDURE updateUniversity(
    p_UniversityId bigint,
    p_UniversityName varchar (150),
    p_LocalAddress varchar (150),
    p_DistrictId bigint)
BEGIN
    UPDATE University
    SET UniversityName = p_UniversityName,
        LocalAddress = p_LocalAddress,
        DistrictId = p_DistrictId
    WHERE UniversityId = p_UniversityId;
    commit;
END$$
DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS deleteUniversity$$
CREATE PROCEDURE deleteUniversity(p_UniversityId bigint)
BEGIN
    DELETE FROM University WHERE UniversityId = p_UniversityId;
    commit;
END$$
DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS getUniversities$$
CREATE PROCEDURE getUniversities(p_UniversityId bigint)
BEGIN
	SELECT UniversityId,
			UniversityName,
			LocalAddress,
			DistrictId
	FROM University
	WHERE UniversityId = coalesce(p_UniversityId, UniversityId);
END$$

DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS getUniNoNewspaper$$
CREATE PROCEDURE getUniNoNewspaper()
BEGIN
        SELECT a.universityId, universityName, districtId, localAddress
        FROM UNIVERSITY a
        LEFT OUTER JOIN DIGITALNEWSPAPER b 
        ON (a.universityId = b.universityId)
        WHERE b.universityId IS NULL;
END$$
DELIMITER ;