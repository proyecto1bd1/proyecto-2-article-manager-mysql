DELIMITER $$

DROP FUNCTION IF EXISTS createProduct$$
CREATE FUNCTION createProduct(p_Name  VARCHAR(100),p_pointsPrice  BIGINT, p_NumberInStock  BIGINT,p_StoreId  BIGINT) RETURNS BIGINT 
BEGIN
INSERT INTO PRODUCT (Name,Pointsprice, NumberInStock,StoreId)
VALUES (p_Name,p_pointsPrice, p_NumberInStock,p_StoreId);
RETURN last_insert_id();
END$$



DROP PROCEDURE IF EXISTS updateProduct$$
CREATE PROCEDURE updateProduct(p_ProductId  BIGINT,p_Name  VARCHAR(100), p_pointsPrice  BIGINT, p_NumberInStock  BIGINT,p_StoreId  BIGINT) 
BEGIN
UPDATE PRODUCT SET
Name = p_Name,
PointsPrice = p_pointsPrice,
NumberInStock = p_NumberInStock,
StoreId = p_StoreId
WHERE ProductId = p_ProductId;
COMMIT;
END$$


DROP PROCEDURE IF EXISTS deleteProduct$$
CREATE PROCEDURE deleteProduct(p_ProductId  BIGINT) 
BEGIN
DELETE FROM PRODUCT
WHERE ProductId = p_ProductId;
COMMIT;
END$$


DROP PROCEDURE IF EXISTS getProduct$$
CREATE PROCEDURE getProduct(p_ProductId  BIGINT) 
BEGIN
SELECT ProductId,Name,PointsPrice,NumberInStock,StoreId
FROM PRODUCT
WHERE ProductId = coalesce(p_ProductId, ProductId);
END$$ 

DELIMITER ;
