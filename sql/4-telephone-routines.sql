DELIMITER $$
DROP FUNCTION IF EXISTS createTelephone$$
CREATE FUNCTION createTelephone(p_phoneNumber VARCHAR(100), p_personId BIGINT) RETURNS BIGINT
BEGIN
INSERT INTO TELEPHONE (phoneNumber,personId)
VALUES (p_phoneNumber, p_personId);
RETURN last_insert_id();
END$$
DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS updateTelephone$$
CREATE PROCEDURE updateTelephone(p_telephoneid  BIGINT,p_phoneNumber  VARCHAR(100),p_personId  BIGINT)
BEGIN
	UPDATE TELEPHONE 
    SET
	phoneNumber = p_phoneNumber,
	personId = p_personId
	WHERE telephoneid = p_telephoneid;
	commit;
END$$
DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS deleteTelephone$$
CREATE PROCEDURE deleteTelephone(p_telephoneid  BIGINT)
BEGIN
DELETE FROM TELEPHONE
WHERE telephoneid = p_telephoneid;
COMMIT;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS getTelephone$$
CREATE PROCEDURE getTelephone(p_telephoneid  BIGINT)
BEGIN
SELECT telephoneid,phoneNumber,personId
FROM TELEPHONE
WHERE telephoneid = coalesce(p_telephoneid, telephoneid);
END$$

DELIMITER ;
