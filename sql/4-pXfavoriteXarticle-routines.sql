DELIMITER $$

DROP PROCEDURE IF EXISTS createPxfavoritexarticle$$
CREATE PROCEDURE createPxfavoritexarticle(p_PersonId  BIGINT, p_ArticleId  BIGINT) 
BEGIN
INSERT INTO PXFAVORITEXARTICLE (PersonId,ArticleId,deleted)
VALUES (p_PersonId, p_ArticleId,0);
END$$

DROP PROCEDURE IF EXISTS deletePxfavoritexarticle$$
CREATE PROCEDURE deletePxfavoritexarticle(p_PersonId  BIGINT, p_ArticleId  BIGINT) 
BEGIN
UPDATE PXFAVORITEXARTICLE
SET deleted = 1
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
COMMIT;
END$$

DROP PROCEDURE IF EXISTS updatePxfavoritexarticle$$
CREATE PROCEDURE updatePxfavoritexarticle(p_PersonId  BIGINT, p_ArticleId  BIGINT) 
BEGIN
UPDATE PXFAVORITEXARTICLE
SET deleted = 0
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
END$$

DROP PROCEDURE IF EXISTS getPxfavoritexarticle$$
CREATE PROCEDURE getPxfavoritexarticle(p_PersonId  BIGINT, p_ArticleId  BIGINT) 
BEGIN
SELECT PersonId,ArticleId,deleted
FROM PXFAVORITEXARTICLE
WHERE PersonId = coalesce(p_PersonId, PersonId) AND ArticleId = coalesce(p_ArticleId, ArticleId);
END
$$

DELIMITER ;
