DELIMITER $$

DROP PROCEDURE IF EXISTS createAdmin$$
CREATE PROCEDURE createAdmin(
    p_adminId BIGINT
)
BEGIN
    INSERT INTO admin (adminId)
    VALUES (p_adminId);
END$$

DROP PROCEDURE IF EXISTS deleteAdmin$$
CREATE PROCEDURE deleteAdmin(
    p_adminId BIGINT
)
BEGIN
    DELETE FROM admin
    WHERE adminId= p_adminId;
END$$

DROP PROCEDURE IF EXISTS getAdmin$$
CREATE PROCEDURE getAdmin(
    p_adminId BIGINT
)
BEGIN
    SELECT adminId
    FROM admin
    WHERE adminId = COALESCE(p_adminId, adminId);
END$$

DELIMITER ;
