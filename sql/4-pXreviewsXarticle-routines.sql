DELIMITER $$

DROP PROCEDURE IF EXISTS createPxreviewsxarticle$$
CREATE PROCEDURE createPxreviewsxarticle(p_PersonId BIGINT,p_ArticleId BIGINT,p_nStars BIGINT) 
BEGIN
INSERT INTO PXREVIEWSXARTICLE (PersonId,ArticleId,nStars)
VALUES (p_PersonId, p_ArticleId,p_nStars);
END$$


DROP PROCEDURE IF EXISTS updatePxreviewsxarticle$$
CREATE PROCEDURE updatePxreviewsxarticle(p_PersonId BIGINT,p_ArticleId BIGINT,p_nStars BIGINT) 
BEGIN
UPDATE PXREVIEWSXARTICLE SET
nStars = p_nStars
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
COMMIT;
END$$


DROP PROCEDURE IF EXISTS deletePxreviewsxarticle$$
CREATE PROCEDURE deletePxreviewsxarticle(p_PersonId BIGINT,p_ArticleId BIGINT) 
BEGIN
DELETE FROM PXREVIEWSXARTICLE
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
COMMIT;
END$$



DROP PROCEDURE IF EXISTS getPxreviewsxarticle$$
CREATE PROCEDURE getPxreviewsxarticle(p_PersonId BIGINT,p_ArticleId BIGINT) 
BEGIN
SELECT PersonId,ArticleId,nStars
FROM PXREVIEWSXARTICLE
WHERE PersonId = coalesce(p_PersonId, PersonId) AND ArticleId = coalesce(p_ArticleId, ArticleId);
END $$

DELIMITER ;
