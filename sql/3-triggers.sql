DELIMITER $$

DROP TRIGGER IF EXISTS beforeCountryInsert$$
CREATE TRIGGER beforeCountryInsert
BEFORE INSERT ON country
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeAdminInsert$$
CREATE TRIGGER beforeAdminInsert
BEFORE INSERT
ON admin
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateAdmin$$
CREATE TRIGGER beforeUpdateAdmin
BEFORE UPDATE
ON admin
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertTelephone$$
CREATE TRIGGER beforeInsertTelephone
BEFORE INSERT
ON telephone
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateTelephone$$
CREATE TRIGGER beforeUpdateTelephone
BEFORE UPDATE
ON telephone
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertEmail$$
CREATE TRIGGER beforeInsertEmail
BEFORE INSERT
ON email
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateEmail$$
CREATE TRIGGER beforeUpdateEmail
BEFORE UPDATE
ON email
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertParameter$$
CREATE TRIGGER beforeInsertParameter
BEFORE INSERT
ON parameter
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateParameter$$
CREATE TRIGGER beforeUpdateParameter
BEFORE UPDATE
ON parameter
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertDigitalNewspaper$$
CREATE TRIGGER beforeInsertDigitalNewspaper
BEFORE INSERT
ON digitalNewspaper
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateDigitalNewspaper$$
CREATE TRIGGER beforeUpdateDigitalNewspaper
BEFORE UPDATE
ON digitalNewspaper
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertStore$$
CREATE TRIGGER beforeInsertStore
BEFORE INSERT
ON store
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateStore$$
CREATE TRIGGER beforeUpdateStore
BEFORE UPDATE
ON store
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertProduct$$
CREATE TRIGGER beforeInsertProduct
BEFORE INSERT
ON product
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateProduct$$
CREATE TRIGGER beforeUpdateProduct
BEFORE UPDATE
ON product
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertCommittee$$
CREATE TRIGGER beforeInsertCommittee
BEFORE INSERT
ON committee
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateCommittee$$
CREATE TRIGGER beforeUpdateCommittee
BEFORE UPDATE
ON committee
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPXcommentXarticle$$
CREATE TRIGGER beforeInsertPXcommentXarticle
BEFORE INSERT
ON pXcommentXarticle
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePXcommentXarticle$$
CREATE TRIGGER beforeUpdatePXcommentXarticle
BEFORE UPDATE
ON pXcommentXarticle
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPurchase$$
CREATE TRIGGER beforeInsertPurchase
BEFORE INSERT
ON purchase
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePurchase$$
CREATE TRIGGER beforeUpdatePurchase
BEFORE UPDATE
ON purchase
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPurchaseXproduct$$
CREATE TRIGGER beforeInsertPurchaseXproduct
BEFORE INSERT
ON purchaseXproduct
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePurchaseXproduct$$
CREATE TRIGGER beforeUpdatePurchaseXproduct
BEFORE UPDATE
ON purchaseXproduct
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPersonXnewspaper$$
CREATE TRIGGER beforeInsertPersonXnewspaper
BEFORE INSERT
ON personXnewspaper
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePersonXnewspaper$$
CREATE TRIGGER beforeUpdatePersonXnewspaper
BEFORE UPDATE
ON personXnewspaper
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPersonXcommittee$$
CREATE TRIGGER beforeInsertPersonXcommittee
BEFORE INSERT
ON personXcommittee
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePersonXcommittee$$
CREATE TRIGGER beforeUpdatePersonXcommittee
BEFORE UPDATE
ON personXcommittee
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertAuthorType$$
CREATE TRIGGER beforeInsertAuthorType
BEFORE INSERT
ON authorType
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateAuthorType$$
CREATE TRIGGER beforeUpdateAuthorType
BEFORE UPDATE
ON authorType
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertNewsArticleType$$
CREATE TRIGGER beforeInsertNewsArticleType
BEFORE INSERT
ON newsArticleType
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateNewsArticleType$$
CREATE TRIGGER beforeUpdateNewsArticleType
BEFORE UPDATE
ON newsArticleType
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPXreviewsXarticle$$
CREATE TRIGGER beforeInsertPXreviewsXarticle
BEFORE INSERT
ON pXreviewsXarticle
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePXreviewsXarticle$$
CREATE TRIGGER beforeUpdatePXreviewsXarticle
BEFORE UPDATE
ON pXreviewsXarticle
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPXfavoriteXarticle$$
CREATE TRIGGER beforeInsertPXfavoriteXarticle
BEFORE INSERT
ON pXfavoriteXarticle
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePXfavoriteXarticle$$
CREATE TRIGGER beforeUpdatePXfavoriteXarticle
BEFORE UPDATE
ON pXfavoriteXarticle 
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertCountry$$
CREATE TRIGGER beforeInsertCountry
BEFORE INSERT
ON country
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateCountry$$
CREATE TRIGGER beforeUpdateCountry
BEFORE UPDATE
ON country
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertProvince$$
CREATE TRIGGER beforeInsertProvince
BEFORE INSERT
ON province
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateProvince$$
CREATE TRIGGER beforeUpdateProvince
BEFORE UPDATE
ON province
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertCanton$$
CREATE TRIGGER beforeInsertCanton
BEFORE INSERT
ON canton
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateCanton$$
CREATE TRIGGER beforeUpdateCanton
BEFORE UPDATE
ON canton
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertDistrict$$
CREATE TRIGGER beforeInsertDistrict
BEFORE INSERT
ON district
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateDistrict$$
CREATE TRIGGER beforeUpdateDistrict
BEFORE UPDATE
ON district
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertUniversity$$
CREATE TRIGGER beforeInsertUniversity
BEFORE INSERT
ON university
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateUniversity$$
CREATE TRIGGER beforeUpdateUniversity
BEFORE UPDATE
ON university
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertAuthor$$
CREATE TRIGGER beforeInsertAuthor
BEFORE INSERT
ON author
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateAuthor$$
CREATE TRIGGER beforeUpdateAuthor
BEFORE UPDATE
ON author
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertGender$$
CREATE TRIGGER beforeInsertGender
BEFORE INSERT
ON gender
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateGender$$
CREATE TRIGGER beforeUpdateGender
BEFORE UPDATE
ON gender
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertPerson$$
CREATE TRIGGER beforeInsertPerson
BEFORE INSERT
ON person
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdatePerson$$
CREATE TRIGGER beforeUpdatePerson
BEFORE UPDATE
ON person
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertNewsArticle$$
CREATE TRIGGER beforeInsertNewsArticle
BEFORE INSERT
ON newsArticle
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateNewsArticle$$
CREATE TRIGGER beforeUpdateNewsArticle
BEFORE UPDATE
ON newsArticle
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertIdType$$
CREATE TRIGGER beforeInsertIdType
BEFORE INSERT
ON idType
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateIdType$$
CREATE TRIGGER beforeUpdateIdType
BEFORE UPDATE
ON idType
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeInsertLog$$
CREATE TRIGGER beforeInsertLog
BEFORE INSERT
ON log
FOR EACH ROW
BEGIN
    SET NEW.creationUser = USER();
    SET NEW.creationDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateLog$$
CREATE TRIGGER beforeUpdateLog
BEFORE UPDATE
ON log
FOR EACH ROW
BEGIN
    SET NEW.lastModifiedUser = USER();
    SET NEW.lastModifiedDate = NOW();
END$$

DROP TRIGGER IF EXISTS beforeUpdateArticleContent$$
CREATE TRIGGER beforeUpdateArticleContent
BEFORE UPDATE
ON newsArticle
FOR EACH ROW
BEGIN
    IF NEW.content <> OLD.content THEN
        INSERT INTO LOG (FieldName,TableName,CurrentValue,PreviousValue,ArticleId)
        VALUES ('content','NewsArticle', NEW.content, OLD.content, NEW.articleId);
    END IF;
END$$

-- before the end of the file, return default delimiter
DELIMITER ;
