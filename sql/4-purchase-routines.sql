DELIMITER $$

DROP FUNCTION IF EXISTS createPurchase$$
CREATE FUNCTION createPurchase(p_PersonId  BIGINT) RETURNS BIGINT 
BEGIN
INSERT INTO PURCHASE (PersonId)
VALUES (p_PersonId);
RETURN last_insert_id();
END$$

DROP PROCEDURE IF EXISTS updatePurchase$$
CREATE PROCEDURE updatePurchase(p_PurchaseId  BIGINT,p_PersonId  BIGINT) 
BEGIN
UPDATE PURCHASE SET
PersonId = p_PersonId
WHERE PurchaseId = p_PurchaseId;
COMMIT;
END$$



DROP PROCEDURE IF EXISTS deletePurchase$$
CREATE PROCEDURE deletePurchase(p_PurchaseId  BIGINT) 
BEGIN
DELETE FROM PURCHASE
WHERE PurchaseId = p_PurchaseId;
COMMIT;
END$$



DROP PROCEDURE IF EXISTS getPurchase$$
CREATE PROCEDURE getPurchase(p_PurchaseId  BIGINT)
BEGIN 
SELECT PurchaseId,PersonId
FROM PURCHASE
WHERE PurchaseId = coalesce(p_PurchaseId, PurchaseId);
END$$

DELIMITER ;
