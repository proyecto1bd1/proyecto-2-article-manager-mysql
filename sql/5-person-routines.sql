
DELIMITER $$
DROP FUNCTION IF EXISTS createPerson$$
CREATE FUNCTION createPerson(
    p_IdCard  VARCHAR(80),
    p_Name  VARCHAR(80),
    p_FirstLastName  VARCHAR(80),
    p_SecondLastName  VARCHAR(80),
    p_Birthdate  DATE,
    p_Photo  MEDIUMBLOB,
    p_LocalAddress  TEXT,
    p_Username  VARCHAR(100),
    p_Password  VARCHAR(100),
    p_GenderId  BIGINT,
    p_TypeId  BIGINT,
    p_DistrictId  BIGINT) RETURNS BIGINT 
BEGIN
    DECLARE personId BIGINT DEFAULT 0;
    DECLARE defaultNewsPaper BIGINT DEFAULT 0;

    SELECT CAST(value as SIGNED)
    INTO defaultNewsPaper
    FROM parameter
    WHERE name = 'defaultRegistryNewsPaper';

    INSERT INTO Person(
        IdCard,
        name,
        FirstLastName,
        SecondLastName,
        Birthdate,
        Photo,
        LocalAddress,
        Username,
        PASSWORD,
        GenderId,
        TypeId,
        DistrictId)
    VALUES(
        p_IdCard,
        p_Name,
        p_FirstLastName,
        p_SecondLastName,
        p_Birthdate,
        p_Photo,
        p_LocalAddress,
        p_Username,
        p_Password,
        p_GenderId,
        p_TypeId,
        p_DistrictId
    );
    SET personId = last_insert_id();

    CALL createPersonxnewspaper(defaultNewsPaper, personId);

    RETURN personId;
END$$

DROP PROCEDURE IF EXISTS updatePerson$$
CREATE PROCEDURE updatePerson(
    p_PersonId  BIGINT,
    p_IdCard  VARCHAR(100),
    p_Name  VARCHAR(100),
    p_FirstLastName  VARCHAR(100),
    p_SecondLastName  VARCHAR(100),
    p_Birthdate  DATE,
    p_Photo  MEDIUMBLOB,
    p_LocalAddress  VARCHAR(100),
    p_Password  VARCHAR(100),
    p_GenderId  BIGINT,
    p_TypeId  BIGINT,
    p_DistrictId  BIGINT) 
BEGIN
    UPDATE Person
    SET IdCard = p_IdCard,
        Name = p_Name,
        FirstLastName = p_FirstLastName,
        SecondLastName = p_SecondLastName,
        Birthdate = p_Birthdate,
        Photo = p_Photo,
        LocalAddress = p_LocalAddress,
        PASSWORD = p_Password,
        GenderId = p_GenderId,
        TypeId = p_TypeId,
        DistrictId = p_DistrictId
    WHERE PersonId = p_PersonId;
    COMMIT;
END
$$



DROP PROCEDURE IF EXISTS deletePerson$$
CREATE PROCEDURE deletePerson(p_PersonId  BIGINT) 
BEGIN
    DELETE FROM Person WHERE PersonId = p_PersonId;
    COMMIT;
END
$$

DROP PROCEDURE IF EXISTS getPersons$$
CREATE PROCEDURE getPersons(p_PersonId  BIGINT) 
BEGIN
        SELECT PersonId,
               IdCard,
               Name,
               FirstLastName,
               SecondLastName,
               Birthdate,
               Photo,
               LocalAddress,
               Username,
               PASSWORD,
               GenderId,
               TypeId,
               DistrictId
        FROM Person
        WHERE PersonId = coalesce(p_PersonId, PersonId);
END
$$

DROP PROCEDURE IF EXISTS getPersonByUsername$$
CREATE PROCEDURE getPersonByUsername(p_Username  VARCHAR(100)) 
BEGIN
        SELECT PersonId,
               IdCard,
               Name,
               FirstLastName,
               SecondLastName,
               Birthdate,
               Photo,
               LocalAddress,
               Username,
               PASSWORD,
               GenderId,
               TypeId,
               DistrictId
        FROM Person
        WHERE Username = p_Username;
END
$$


DROP PROCEDURE IF EXISTS getPersonNoNewspaper$$
CREATE PROCEDURE getPersonNoNewspaper()
BEGIN
        SELECT a.PersonId,
               IdCard,
               Name,
               FirstLastName,
               SecondLastName,
               Birthdate,
               Photo,
               LocalAddress,
               Username,
               PASSWORD,
               GenderId,
               TypeId,
               DistrictId
        FROM Person a
        LEFT OUTER JOIN personxnewspaper b 
        ON (a.personid = b.personid)
        WHERE b.personid IS NULL;
END$$


DROP PROCEDURE IF EXISTS getPersonNoCommittee$$
CREATE PROCEDURE getPersonNoCommittee()
BEGIN
        SELECT a.PersonId,
               IdCard,
               Name,
               FirstLastName,
               SecondLastName,
               Birthdate,
               Photo,
               LocalAddress,
               Username,
               PASSWORD,
               GenderId,
               TypeId,
               DistrictId
        FROM Person a
        LEFT OUTER JOIN personxcommittee b 
        ON (a.personid = b.personid)
        WHERE b.personid IS NULL;
END$$

DELIMITER ;
