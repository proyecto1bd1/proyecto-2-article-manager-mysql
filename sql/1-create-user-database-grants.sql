CREATE USER p2bd
IDENTIFIED BY 'Maroon-Array6-Dyslexic';

CREATE DATABASE project2;

GRANT 
	SELECT
ON project2.*
TO p2bd;

GRANT
    INSERT
ON project2.*
TO p2bd;

GRANT
    UPDATE
ON project2.*
TO p2bd;

GRANT
    DELETE
ON project2.*
TO p2bd;

GRANT
    CREATE
ON project2.*
TO p2bd;

GRANT
    DROP
ON project2.*
TO p2bd;

GRANT
    ALTER
ON project2.*
TO p2bd;

GRANT
    ALTER ROUTINE
ON project2.*
TO p2bd;

GRANT
    CREATE ROUTINE
ON project2.*
TO p2bd;

GRANT
    CREATE VIEW
ON project2.*
TO p2bd;

GRANT
    EVENT
ON project2.*
TO p2bd;

GRANT
    EXECUTE
ON project2.*
TO p2bd;

GRANT
    INDEX
ON project2.*
TO p2bd;

GRANT
    REFERENCES
ON project2.*
TO p2bd;

GRANT
    SHOW VIEW
ON project2.*
TO p2bd;

GRANT
    TRIGGER
ON project2.*
TO p2bd;

-- We allow for temporal permission of trigger and procedures creations to user
-- If the connection is lost, the DBA will have to allow it again 
SET GLOBAL log_bin_trust_function_creators = 1;
