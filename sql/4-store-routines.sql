DELIMITER $$

DROP FUNCTION IF EXISTS createStore$$
CREATE FUNCTION createStore(p_Description VARCHAR(100),p_NewspaperId BIGINT) RETURNS BIGINT 
BEGIN
INSERT INTO STORE (Description,NewspaperId)
VALUES (p_Description,p_NewspaperId);
RETURN last_insert_id();
END$$ 

DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS updateStore$$
CREATE PROCEDURE updateStore(p_StoreId BIGINT,p_Description VARCHAR(100),p_NewspaperId BIGINT) 
BEGIN
UPDATE STORE SET
Description = p_Description,
NewspaperId = p_NewspaperId
WHERE StoreId = p_StoreId;
COMMIT;
END$$

DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteStore$$
CREATE PROCEDURE deleteStore(p_StoreId BIGINT) 
BEGIN
DELETE FROM STORE
WHERE StoreId = p_StoreId;
COMMIT;
END$$

DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS getStore$$
CREATE PROCEDURE getStore(p_StoreId BIGINT) 
BEGIN
SELECT StoreId,Description,NewspaperId
FROM STORE
WHERE StoreId = coalesce(p_StoreId, StoreId);
END$$

DELIMITER ;
